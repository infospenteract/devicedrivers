
var TemperatureC = function(_adcvalue)
{
	if(_adcvalue > 1022)
	{
		return 100;
	}
	var ADC = _adcvalue;
	var V = (ADC * 3.3 / 1023);
	var R = (10000 * 3.3) / V - 10000;
	var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 10000.0))
	temperature = (temperature - 273.15)*10;
	temperature = Math.round(temperature);
	return temperature/10;
}

var adcValueOfTemperatureC = function(tempC)
{
	if(tempC > 99)
	{
		return 65535;
	}
	var R = 10000 * Math.exp(3950/(tempC + 273.15) - 3950/298.15);
	var V = 10000 * 3.3 / (R + 10000);
	var ADC = V * 1023 / 3.3;
	
	return Math.round(ADC);	
}
var CurrentInAmp = function(currentRead, speed)
{
	var valCur = currentRead;// - 521;//521 bit est la valeur lue a 0 amperes
	if(speed == 100)
	{
		valCur = 36.5/1000 * valCur -0.25;//calculs trouvé par tests
	}
	else
	{
		valCur = (0.000115)*valCur*valCur + 0.0149*valCur - 0.0167;//calculs trouvé par tests
	}
	if(valCur < 0)
		valCur = 0;
	return valCur;
}

var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

    this.PropertiesList = [
        {
            name: 'lastoverridetime',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['lastoverridetime']; }
        },{
            name: 'manualvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if(_json['manualvalue'] === undefined){
                    return 0;
                }
                return _json['manualvalue']; }
        },{
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
		{
            name: 'pcb_version',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[7]);
            }
        },
		{
            name: 'mode_operation',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
                var valMode = (_pa[35]);
				if(valMode == 0)
					return "SHUTDOWN";
				if(valMode == 1)
					return "NORMAL";
				if(valMode == 2)
					return "FLEXIBLE 1";
				if(valMode == 3)
					return "FLEXIBLE 2";
				if(valMode == 4)
					return "BYPASS";
				if(valMode == 5)
					return "DEGIVRAGE PREVENTIF";
				if(valMode == 6)
					return "DEGIVRAGE LONG";
				if(valMode == 7)
					return "DEGIVRAGE SECURITAIRE";
				if(valMode == 8)
					return "POSITION FAN 1";
				if(valMode == 9)
					return "POSITION FAN 2";
				if(valMode == 10)
					return "INITIALIZE";
				if(valMode == 11)
					return "FILTATION";
				
			    return "UNKNOWN";
				
            }
        },
		{
            name: 'fan1_speed',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[32]);
            }
        },
		{
			name: 'current_fan1',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[18] * 256 + _pa[17]
			if (adcVal > 1023) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }
			//var lCurrent = CurrentInAmp(adcVal,_pa[32]);//avec triac
            var lCurrent = 20 * adcVal / 500;
			
					
            return lCurrent;
        }
		},
		{
            name: 'fan1_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16])&3;
				if(valAct == 0)
					return "POSITION 1";
				if(valAct == 1)
					return "POSITION 4";
				if(valAct == 2)
					return "POSITION 2";
				if(valAct == 3)
					return "POSITION 3";
				
                
            }
        },
		{
            name: 'fan1_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>6)&3;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "CW";
				if(valAct == 2)
					return "CCW";
				
				return "ERROR";
                
            }
        },
		{
            name: 'fan2_speed',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[33]);
            }
        },
		{
			name: 'current_fan2',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[20] * 256 + _pa[19]
			if (adcVal > 1023) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }
			//var lCurrent = CurrentInAmp(adcVal,_pa[32]);//avec triac
            var lCurrent = 20 * adcVal / 500;
			
					
            return lCurrent;
        }
		},
		{
            name: 'fan2_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16]>>>2)&3;
				if(valAct == 0)
					return "POSITION 1";
				if(valAct == 1)
					return "POSITION 4";
				if(valAct == 2)
					return "POSITION 2";
				if(valAct == 3)
					return "POSITION 3";
				
                
            }
        },
		{
            name: 'fan2_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>4)&3;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "CW";
				if(valAct == 2)
					return "CCW";
				
				return "ERROR";
                
            }
        },
		{
            name: 'aspir_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>2)&3;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "CW";
				if(valAct == 2)
					return "CCW";
				
				return "ERROR";
                
            }
        },
		{
            name: 'arrosoir_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8])&3;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "CW";
				if(valAct == 2)
					return "CCW";
				
				return "ERROR";
                
            }
        },
		{
            name: 'arrosoire_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16]>>>4)&3;
				if(valAct == 0)
					return "ERROR";
				if(valAct == 1)
					return "POSITION DEBUT";
				if(valAct == 2)
					return "POSITION FIN";
				if(valAct == 3)
					return "POSITION MILIEU";
				
                
            }
        },
		{
            name: 'degivrage_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[14]>>>6)&3;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "CW";
				if(valAct == 2)
					return "CCW";
				
				return "ERROR";
                
            }
        },
		{
            name: 'degivrage_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16]>>>6)&3;
				if(valAct == 0)
					return "ERROR";
				if(valAct == 1)
					return "POSITION DEBUT";
				if(valAct == 2)
					return "POSITION FIN";
				if(valAct == 3)
					return "POSITION MILIEU";
				
                
            }
        },
		{
            name: 'volet_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[14]>>>4)&3;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "CW";
				if(valAct == 2)
					return "CCW";
				
				return "ERROR";
                
            }
        },
		{
            name: 'valve',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[14]>>>3)&1;
				if(valAct == 0)
					return "OFF";
				if(valAct == 1)
					return "ON";
				
                
            }
        },
		{
            name: 'relais',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[14]>>>2)&1;
				if(valAct == 0)
					return "OFF";
				if(valAct == 1)
					return "ON";
				
                
            }
        },
		{
            name: 'led1',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[15]>>>7)&1;
				if(valAct == 0)
					return "OFF";
				if(valAct == 1)
					return "ON";
				
                
            }
        },
		{
            name: 'led2',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[15]>>>6)&1;
				if(valAct == 0)
					return "OFF";
				if(valAct == 1)
					return "ON";
				
                
            }
        },
		{
            name: 'bluetooth',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[15]>>>5)&1;
				if(valAct == 0)
					return "CONNECTED";
				if(valAct == 1)
					return "NOT CONNECTED";
				
                
            }
        },
		
		{
            name: 'limites_switch',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[16]);
            }
        },
		
		
		{
            name: 'control',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                var valMode = (_pa[21]);
				if(valMode == 1)
					return "BLUETOOTH";
				if(valMode == 2)
					return "HUB";
				if(valMode == 3)
					return "0-10 A";
				if(valMode == 4)
					return "0-10 B";
				if(valMode == 5)
					return "0-10 C";
				if(valMode == 6)
					return "MODBUS";
				
				
			    return "UNKNOWN";
            }
        },
		{
			name: 'temperature1inc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [23] * 256 + _pa [22]);
			}
		},
		{
			name: 'temperature2inc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [25] * 256 + _pa [24]);
			}
		},
		{
			
			name: 'temperature_a_inc',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[27] *256 + _pa[26];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10;
			}
		},
		{
			name: 'temperature_b_inc',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[29] *256 + _pa[28];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10;
			
			}
		},
		{
            name: 'humidity_a_percentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30]); 
			}
        },
		{
            name: 'humidity_b_percentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[31]); 
			}
        },
		
		
        {
            name: 'control_0_10v',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[34]);
                         
            }
        }
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        
		
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        },
		{
        name: 'setESA3000',
		arguments: [{name:'stringToSend',type:'string'}],
		method: function(_arg) {
		var output = [];
		output.push (0x1f);
		output.push(0x41); 
		for (var i=0;i<_arg.length;i++)
		{
			output.push(_arg.charCodeAt(i));
			//var charCode = _arg.charCodeAt(i);
            //output.push((charCode & 0xFF00) >> 8);
            //output.push(charCode & 0xFF);
		}
		
		return btoa(output);

		
        
        }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};