
    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },{
            name: 'productionstage',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
              return _json['productionstage']; }
        },{
            name: 'message',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['message']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },{
          name: 'roomimage',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['roomimage']; }
        },{
            name: 'subspecies',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['subspecies']; }
          },
          {
              name: 'ageatarrivalindays',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['ageatarrivalindays'];
            }
          },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'cameraid',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['cameraid'];
            }
          },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'settempincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[4] *256+ _pa[3])/10; }
        },
        {
            name: 'seconds',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[6]); }
        },
        {
            name: 'minutes',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[7]); }
        },
        {
            name: 'hours',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8]); }
        },
        {
            name: 'days',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[9]); }
        },
        {
            name: 'totalseconds',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[9]*86400 + _pa[8] * 3600 + _pa[7] * 60 + _pa[6]); }
        },
        {
            name: 'chickenageseconds',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (45*24*3600)-(_pa[9]*86400 + _pa[8] * 3600 + _pa[7] * 60 + _pa[6]); }
        },
        {
            name: 'inprogress',
            type: 'boproperol',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[0]==1); }
        },
        {
            name: 'inpreparation',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[1]==1); }
        },
        {
            name: 'lumens',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[2]); }
        },
        {
            name: 'expectedtemperatureincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[4]*256+_pa[3])/10; }
        },
        {
            name: 'indoordevice',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['indoordevice']; }
        },
        {
            name: 'heaterlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['heaterlist'];
            }
          },
          {
              name: 'fanslist',
              type: 'string',
              parser: function (_pa, _apa, _json, _typecsv) {
                  return _json['fanslist'];
              }
            },
            {
                name: 'actuatorlist',
                type: 'string',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return _json['actuatorlist'];
                }
              },
              {
                  name: 'feederlist',
                  type: 'string',
                  parser: function (_pa, _apa, _json, _typecsv) {
                      return _json['feederlist'];
                  }
              },
              {
                  name: 'openweatherhubid',
                  type: 'string',
                  parser: function (_pa, _apa, _json, _typecsv) {
                      return _json['openweatherhubid'];
                  }
                },
                {
                    name: 'selectedmode',
                    type: 'string',
                    parser: function (_pa, _apa, _json, _typecsv) {
                        return _json['selectedmode'];
                    }
                          }
    ];

    this.FunctionList = [
            {
                name: 'Start',
                method: function(_arg) {
                    return "";
                }
            },
            { 
                name: 'Stop',
                method: function(_arg) {
                    return "";
                }
            },
            {
                name: 'SetDays',
                method: function(_arg) {
                //Argument most be de number of days to offset start sequence
                    return "";
                }
            },
            {
                name: 'SetPreparation',
                method: function(_arg) {
                //Argument most be de number of days to offset start sequence
                    return "";
                }
            }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};