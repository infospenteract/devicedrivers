  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  this.PropertiesList = [
      {
          name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['name']; }
        },{
            name: 'productionstage',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['productionstage']; }
        },{
            name: 'message',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['message']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },{
          name: 'roomimage',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['roomimage']; }
        },{
            name: 'subspecies',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['subspecies']; }
          },
          {
              name: 'ageatarrivalindays',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['ageatarrivalindays'];
            }
          },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority'];
          }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        }, {
          name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
      {
          name: 'location',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['location'];
          }
        },
        {
            name: 'cameraid',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['cameraid'];
            }
          },
      {
          name: 'latitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['latitude'];
          }
        },
      {
          name: 'longitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['longitude'];
          }
        },
      {
          name: 'done',
          type: 'bool',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[0];
          }
        },
      {
          name: 'inpreparation',
          type: 'bool',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[1];
          }
        },
      {
          name: 'inprogress',
          type: 'bool',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[2];
          }
        },
      {
          name: 'notyetstarted',
          type: 'bool',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[3];
          }
        },
      {
          name: 'paused',
          type: 'bool',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[4];
          }
        },
      {
          name: 'pausedb',
          type: 'bool',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[4] == 'True';
          }
        },
      {
          name: 'ageofchickeninminutes',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[5];
          }
        },
      {
          name: 'amoniappm',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[6];
          }
        },
      {
          name: 'animalmeanweightinkg',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[7];
          }
        },
      {
          name: 'animalmeanweighting',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[7] * 1000;
          }
        },
      {
          name: 'co2ppm',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[8];
          }
        },
      {
          name: 'currentlumens',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[9];
          }
        },
      {
          name: 'currenttemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[10];
          }
          },
      {
          name: 'expectedtemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[11];
          }
          },
      {
          name: 'currenttemperatureinfarenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var value = convert(_pa).split('|')[10];
              return (value * 9 / 5) + 32;
          }
          },
      {
          name: 'expectedtemperatureinfarenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var value = convert(_pa).split('|')[11];
              return (value * 9 / 5) + 32;
          }
          },
      {
          name: 'foodremainingweightinkg',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[12];
          }
        },
      {
          name: 'humiditypercentage',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[13];
          }
        },
      {
          name: 'blobnumberofanimals',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json["numberofanimals"];
          }
        },
      {
          name: 'numberofanimals',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[14];
          }
        },
      {
          name: 'staticpressureininh20',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[15];
          }
        },
      {
          name: 'temperaturelist',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['temperaturelist'];
          }
        },
      {
          name: 'daylength',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['daylength'];
          }
        },
        {
            name: 'lightlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['lightlist'];
            }
        },
        {
            name: 'humiditylist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['humiditylist'];
            }
        },
      {
          name: 'dimmerid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['dimmerid'];
          }
        },
      {
          name: 'hvacid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['hvacid'];
          }
        },
      {
          name: 'debutdate',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['debutdate'];
          }
        },
      {
          name: 'cycletimeindays',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['cycletimeindays'];
          }
        },
      {
          name: 'seelowsid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['seelowsid'];
          }
        },
        {
            name: 'heaterlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['heaterlist'];
            }
          },
          {
              name: 'fanslist',
              type: 'string',
              parser: function (_pa, _apa, _json, _typecsv) {
                  return _json['fanslist'];
              }
            },
            {
                name: 'actuatorlist',
                type: 'string',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return _json['actuatorlist'];
                }
              },
              {
                  name: 'feederlist',
                  type: 'string',
                  parser: function (_pa, _apa, _json, _typecsv) {
                      return _json['feederlist'];
                  }
              },
              {
                  name: 'openweatherhubid',
                  type: 'string',
                  parser: function (_pa, _apa, _json, _typecsv) {
                      return _json['openweatherhubid'];
                  }
                },
                {
                    name: 'selectedmode',
                    type: 'string',
                    parser: function (_pa, _apa, _json, _typecsv) {
                        return _json['selectedmode'];
                    }
                          }

    ];

  this.FunctionList = [
      {
          name: 'Refresh',
          method: function (_arg) {
              return "";
          }
            },
      {
          name: 'Pause',
          method: function (_arg) {
              return "";
          }
            },
      {
          name: 'Restart',
          method: function (_arg) {
              return "";
          }
            },
      {
          name: 'SetTemperatureInCelcius',
          method: function (_arg) {
              //Argument most be de number of days to offset start sequence
              return "";
          }
            },
      {
          name: 'SetLumens',
          method: function (_arg) {
              //Argument most be de number of days to offset start sequence
              return "";
          }
            }
    ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
