  this.PropertiesList = [
      {
          name: 'driverinfos',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _typecsv;
          }
        },
      {
          name: 'error',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['error'] === undefined) {
                  return "";
              }
              return _json['error'];
          }
        },
      {
          name: 'location',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['location'] === undefined) {
                  return "undefined";
              }
              return _json['location'];
          }
      },
      {
          name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['name'] === undefined) {
                  return "undefined";
              }
              return _json['name'];
          }
        },
      {
          name: 'message',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['message'];
          }
        },
      {
          name: 'animaltype',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['animaltype'];
          }
        },
      {
          name: 'productiontype',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['productiontype'];
          }
        },
      {
          name: 'numberofanimals',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['numberofanimals'] === undefined) {
                  _json['numberofanimals'] = 0;
              }
              return _json['numberofanimals'];
          }
        },
      {
          name: 'numberofanimaldeath',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['numberofanimaldeath'] === undefined) {
                  _json['numberofanimaldeath'] = 0;
              }
              return _json['numberofanimaldeath'];
          }
        },{
          name: 'numberofanimalculls',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['numberofanimalculls'] === undefined) {
                  _json['numberofanimalculls'] = 0;
              }
              return _json['numberofanimalculls'];
          }
        },
      {
          name: 'ageatarrival',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['ageatarrival'];
          }
        },
      {
          name: 'debutdate',
          type: 'string',
          parser: function (_pa, _debutapa, _json, _typecsv) {
              return _json['debutdate'];
          }
        },
      {
          name: 'numberofdays',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['numberofdays'];
          }
        },
      {
          name: 'automationlist',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['automationlist'];
          }
        },
      {
          name: 'manualtemp',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['manualtemp'] === undefined) {
                  _json['manualtemp'] = 18;
              }
              return _json['manualtemp'];
          }
        },
      {
          name: 'expectedtemp',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['manualtemp'] === undefined) {
                  _json['manualtemp'] = 18;
              }
              return Math.abs(_json['manualtemp']);
          }
        },
      {
          name: 'manuallight',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['manuallight'] === undefined) {
                  _json['manuallight'] = 100;
              }
              return _json['manuallight'];
          }
        },
      {
          name: 'minimalventillation',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['minimalventillation'] === undefined) {
                  _json['minimalventillation'] = -0.0005;
              }
              return _json['minimalventillation'];
          }
        },
            {
          name: 'minimalheat',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['minimalheat'] === undefined) {
                  _json['minimalheat'] = -0.0005;
              }
              return _json['minimalheat'];
          }
        },
      {
          name: 'msg_temp',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['msg_temp'];
          }
        },
      {
          name: 'msg_light',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['msg_light'];
          }
        },
      {
          name: 'msg_minvent',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['msg_minvent'];
          }
        },
            {
          name: 'msg_minvheat',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['msg_minheat'];
          }
        },
      {
          name: 'msg_animals',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['msg_animals'];
          }
        },
      {
          name: 'weighting',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['weighting'] === undefined) {
                  _json['weighting'] = "0";
              }
              return _json['weighting'];
          }
        },
      {
          name: 'dailygaining',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['dailygaining'] === undefined) {
                  _json['dailygaining'] = "0";
              }
              return _json['dailygaining'];
          }
        },
            ,
            {
          name: 'humidity',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['humidity'] === undefined) {
                  _json['humidity'] = "0";
              }
              return _json['humidity'];
          }
        },
            {
          name: 'outdoorhumidity',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['outdoorhumidity'] === undefined) {
                  _json['outdoorhumidity'] = "0";
              }
              return _json['outdoorhumidity'];
          }
        },
            {
          name: 'outdoortemperatureincelcius',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['outdoortemperatureincelcius'] === undefined) {
                  _json['outdoortemperatureincelcius'] = "0";
              }
              return _json['outdoortemperatureincelcius'];
          }
        },
            {
          name: 'currenttemperatureincelcius',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['currenttemperatureincelcius'] === undefined) {
                  _json['currenttemperatureincelcius'] = "0";
              }
              return _json['currenttemperatureincelcius'];
          }
        },
            {
          name: 'feellikeincelcius',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['feellikeincelcius'] === undefined) {
                  _json['feellikeincelcius'] = "0";
              }
              return _json['feellikeincelcius'];
          }
        },
            {
          name: 'foodconsumptionperanimaling',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['foodconsumptionperanimaling'] === undefined) {
                  _json['foodconsumptionperanimaling'] = "0";
              }
              return _json['foodconsumptionperanimaling'];
          }
        },
             {
          name: 'waterconsumptionperanimalinl',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['waterconsumptionperanimalinl'] === undefined) {
                  _json['waterconsumptionperanimalinl'] = "0";
              }
              return _json['waterconsumptionperanimalinl'];
          }
        },
        {
          name: 'energyconsumption',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['energyconsumption'] === undefined) {
                  _json['energyconsumption'] = "0";
              }
              return _json['energyconsumption'];
          }
        },
        {
          name: 'cs',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['cs'] === undefined) {
                  _json['cs'] = "0";
              }
              return _json['cs'];
          }
        },
        {
          name: 'hs',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['hs'] === undefined) {
                  _json['hs'] = "0";
              }
              return _json['hs'];
          }
        },
        {
          name: 'co2',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['co2'] === undefined) {
                  _json['co2'] = "0";
              }
              return _json['co2'];
          }
        },
        {
          name: 'nh3',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              if (_json['nh3'] === undefined) {
                  _json['nh3'] = "0";
              }
              return _json['nh3'];
          }
        }
    ];

  this.FunctionList = [
      {
          name: 'Refresh',
          method: function (_arg) {
              return "";
          }
          }
          ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
