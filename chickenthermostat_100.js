
    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'settempincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[1] *256+ _pa[0])/10; }
        },
        {
            name: 'seconds',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[3]); }
        },
        {
            name: 'minutes',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[4]); }
        },
        {
            name: 'hours',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[5]); }
        },
        {
            name: 'days',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[6]); }
        },
        {
            name: 'totalseconds',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[6]*86400 + _pa[5] * 3600 + _pa[4] * 60 + _pa[3]); }
        },
        {
            name: 'chickenageseconds',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (45*24*3600)-(_pa[6]*86400 + _pa[5] * 3600 + _pa[4] * 60 + _pa[3]); }
        },
        {
            name: 'started',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[2]==1); }
        }
    ];

    this.FunctionList = [
            {
                name: 'Start',
                method: function(_arg) {
                    return "";
                }
            },
            { 
                name: 'Stop',
                method: function(_arg) {
                    return "";
                }
            },
            {
                name: 'SetDays',
                method: function(_arg) {
                //Argument most be de number of days to offset start sequence
                    return "";
                }
            }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};