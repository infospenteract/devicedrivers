  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  this.PropertiesList = [
      {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
      {
          name: 'location',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['location'];
          }
        },
      {
          name: 'managerid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['ManagerID'];
          }
        },
      {
          name: 'hvacid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['HVACID'];
          }
        },
      {
          name: 'latitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['latitude'];
          }
        },
      {
          name: 'longitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['longitude'];
          }
        },
      {
          name: 'expectedtemp',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              var result = JSON.parse(convert(_pa))["expectedtemp"];
              return result;
          }
        },
      {
          name: 'expectedlightlux',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var result = JSON.parse(convert(_pa))["expectedlightlux"];
              return result;
          }
        },
      {
          name: 'expectedminvent',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var result = JSON.parse(convert(_pa))["expectedminvent"];
              return result;
          }
        }
    ];

  this.FunctionList = [
      {
          name: 'SendExpectedTemperature',
          method: function (_arg) {
              //Argument must be degree in celcius Must be call using Extended structure test
              return "";
          }
    },
      {
          name: 'SendExpectedLux',
          method: function (_arg) {
              //Argument must be degree in celcius Must be call using Extended structure test
              return "";
          }
    },
      {
          name: 'SendExpectedMinVent',
          method: function (_arg) {
              //Argument must be degree in celcius Must be call using Extended structure test
              return "";
          }
            }
            ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
