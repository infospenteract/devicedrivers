this.PropertiesList = [
    {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },{
            name: 'lastoverridetime',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['lastoverridetime']; }
        },{
            name: 'manualvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if(_json['manualvalue'] === undefined){
                    return 0;
                }
                return _json['manualvalue']; }
        },
        {
            name: 'fadeenable',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['fadeenable'];
        }
        },
        {
            name: 'openinghour',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['openinghour'];
        }
        },
        {
            name: 'closinghour',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['closinghour'];
        }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'percentage',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[0];
        }
        },
    {
        name: 'luxtolumen',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['luxtolumen'];
        }
        },
    {
        name: 'lightids',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['lightids'];
        }
        },
    {
        name: 'lumens',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'expectedlumens',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[2];
        }
        },
    {
        name: 'lightids',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['lightids'];
        }
        },
        {
            name: 'nighttime',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['nighttime'];
            }
        }
    ];

this.FunctionList = [
    {
        name: 'SetLumens',
        method: function (_arg) {
            //Argument must be degree in celcius Must be call using Extended structure test
            return "";
        }
    },
    {
        name: 'SetLumensFade',
        method: function (_arg) {
            //Argument must be degree in celcius Must be call using Extended structure test
            return "";
        }
            }
            ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
