  this.PropertiesList = [
      {
            name: 'statlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return "sourcehumidity,sourcetemperatureincelcius,humidity,currenttemperatureincelcius,feelliketemperatureincelcius,co2,electricalpower,prod1nv,prod2nv,numberofanimaldeathnv,currentcs,currenths,manualventnv,manualcsnv,manualhsnv,water,feed,weight";
            }
        }
   
    ];

  this.FunctionList = [
      {
          name: 'Refresh',
          method: function (_arg) {
              return "";
          }
          }
          ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
