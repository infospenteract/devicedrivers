
    /* var PressurePa = function(ADCvalue,vr)
	{
		var zero = vr - 2047;
		
		var output = ((ADCvalue-zero / 4095)-0.5) * Math.pow(((ADCvalue-zero / (4095 * 0.4)) - 1.25),2) * 525;
		return output;
		
	} */
	
	this.PropertiesList = [
        {
            name: 'statlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return "pressurerelativeinchofwater,battvoltage,rxrssi";
            }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[2]); }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return 1; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1] * 256 + _pa[0]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[1] * 256 + _pa[0]) / 100; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[1] * 256 + _pa[0]) / 100; 
            
			if (BattAD >= 1.52)
				return 100;
			if (BattAD > 1.42)
				return ((BattAD - 1.42) * 200 + 80);
			if (BattAD > 1.31)
				return ((BattAD - 1.31) * 181.81 + 60);
			if (BattAD > 1.21)
				return ((BattAD - 1.21) * 400 + 20);
			if (BattAD > 1)
				return ((BattAD - 1) * 71.43 + 5);

			return 0;
            
            }
        },
		{
            name: 'pressureadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[4] * 256 + _pa[3]); }
        },
        {
            name: 'pressurekpa',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){
				var pressure = (_pa[4] * 256 + _pa[3])*5/4095;
                var f = (pressure/5-0.5);
                if (f>0)
                {
                    f = 1;
                }
                else
                {
                    f = -1;
                }
                var t = (f*Math.pow((pressure/(5*0.4)-1.25),2)*525)/1000;
                return t;
			}
        },
        {
            name: 'pressurerelativeinchofwater',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
				var pressure = (_pa[4] * 256 + _pa[3])*5/4095;
                var f = (pressure/5-0.5);
                if (f>0)
                {
                    f = 1;
                }
                else
                {
                    f = -1;
                }
                var t = (f*Math.pow((pressure/(5*0.4)-1.25),2)*525)*0.00401865;
                return t;
            }
        }
    ];

    this.FunctionList = [
    ];
