    this.PropertiesList = [
        {
            name: 'statlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return "statenum"
            }
        }, 
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		{
            name: 'forcerelayfactor',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[34]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; 
            
            if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
                return ((BattAD - 3.3) * 57.14 + 5);
                

			return 0;
            
            }
        },
        {
            name: 'DiginADC',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[31] * 256 + _pa[30]; }
        },
		{
			name: 'DigInState',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
				return _pa[27];
			}
		},
		{
            name: 'Open',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
		    if((_pa[27] == 0)||(_pa[27] == 1))
			{
				return false;
			}
			else if((_pa[27] == 2)||(_pa[27] == 3))
			{
				return true;
			}
			}
            
        },
		{
            name: 'Close',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
		    if((_pa[27] == 0)||(_pa[27] == 2))
			{
				return false;
			}
			else if((_pa[27] == 1)||(_pa[27] == 3))
			{
				return true;
			}
			}
            
        },
        {
            name: 'statenum',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
		    if((_pa[27] == 0)||(_pa[27] == 1))
			{
				return 0;
			}
			else if((_pa[27] == 2)||(_pa[27] == 3))
			{
				return 1;
			}
			}
            
        },
        {
            name: 'state',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
		    if((_pa[27] == 0)||(_pa[27] == 1))
			{
				return false;
			}
			else if((_pa[27] == 2)||(_pa[27] == 3))
			{
				return true;
			}
			}
            
        },
		{
			name: 'DigInStateEnum',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
				if(_pa[27] == 0)
				{
					return "NONE detected";
				}
				else if(_pa[27] == 1)
				{
					return "CLOSE detected"; 
				}
				else if(_pa[27] == 2)
				{
					return "OPEN detected"; 
				}
				else if(_pa[27] == 3)
				{
					return "CLOSE and OPEN detected"; 
				}
				else
				{
					return "Undefined";
				}
			 }
		},
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[37];
            }
        }  
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor", "defaultValue":2};