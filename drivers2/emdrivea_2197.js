
    this.PropertiesList = [
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>1; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; 
            
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
				return ((BattAD - 3.3) * 57.14 + 5);

			return 0;
            
            }
        },
		{
			name: 'value',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
			
			var adcVal = _pa[28] * 256 + _pa[27]
			if(adcVal > 4095)
            {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

			return adcVal; }
		},
		{
			name: 'expectedValue',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30] * 256 + _pa[29]); }
		},
		{
			name: 'defaultValue',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[32] * 256 + _pa[31]); }
		},
        {
            name: 'ampermeter',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return _typecsv.split(',')[4] > 0;
            }
        },
		{
			name: 'currentadc',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa[34] * 256 + _pa[33]);
			}
		},
		{
			name: 'current',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				var adcVal = _pa[34] * 256 + _pa[33]
				if(adcVal > 4095)
				{
					//means negative value. If the there is noise one the ground of the ADC we might read negative value
					adcVal = 0;
				}
				
				var lCurrent = adcVal * 3.3 / 4095.0 * 1012 / 120.0 * 1.14 + 0.463;  
				if(lCurrent < 0.6)
				{
					return 0;
				}
				return lCurrent;
			}
        },
        {
            name: 'rcon',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[35];
            }
        },
        {
            name: 'brownout',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
				if((_pa[35] & 0x02) == 0x02)
				{
					return (_pa[35] & 0x01) < 1;
				}
				else 
				{
					return false;
				}
            }
        },
        {
            name: 'poweronreset',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[35] & 0x02) < 1;
            }
        },
        {
            name: 'wdt',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[35] & 0x08)  < 1;
            }
        },
        {
            name: 'rcon',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[35];
            }
        },
        {
            name: 'brownout',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
				if((_pa[35] & 0x02) == 0x02)
				{
					return (_pa[35] & 0x01) < 1;
				}
				else 
				{
					return false;
				}
            }
        },
        {
            name: 'poweronreset',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[35] & 0x02) < 1;
            }
        },
        {
            name: 'wdt',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[35] & 0x08)  < 1;
            }
        }
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetValue',
            method: function(_arg) {
				if(_arg > 4095)
				{
					_arg = 4095;
				}
                var valueMsb = Number(_arg) / 256;//value from 0 to 4095
				var valueLsb = Number(_arg) % 256;//value from 0 to 4095
                return btoa([0x1f,0x43,valueLsb,valueMsb]);
            }
        },
		{
            name: 'SetDefaultValue',
            method: function(_arg) {
				if(_arg >= 4095)
				{
					_arg = 65535;
				}
                var valueMsb = Number(_arg) / 256;//value from 0 to 4095
				var valueLsb = Number(_arg) % 256;//value from 0 to 4095
                return btoa([0x1f,0x44,valueLsb,valueMsb]);
            }
        },
		{
            name: 'SetRelay',
            arguments: [{name:'relay0',type:'byte'},{name:'relay1',type:'byte'},{name:'relay2',type:'byte'},{name:'relay3',type:'byte'}],
            method: function(_arg)
            {
                var split = _arg.split(',');
                var i=0;
                var output = [0x1f, 0x42, 2, 2, 2, 2 ];
                for (i=0;i<split.length;i++)
                {
                    output[2+i] = Number(split[i]);
                }
                return btoa(output);
            }
        },
		{
            name: 'SetDebugRelay',
            method: function(_arg) {
                return btoa([0x1f,0x45]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];
