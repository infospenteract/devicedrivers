this.PropertiesList = [
        {
            name: 'statlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return "value"
            }
        }, 
        {
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
		{
			name: 'temperatureinc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
                _adcvalue = _pa [21] * 256 + _pa [20]
                if(_adcvalue > 4094)
                {
                    return 100;
                }
                var ADC = _adcvalue;
                var V = (ADC * 3.3 / 4095);
                var R = (10000 * 3.3) / V - 10000;
                var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 9991.6))
                return temperature - 273.15;
			}
		},
		{
			name: 'defaulttemperatureinc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
                _adcvalue = _pa [32] * 256 + _pa [31]
                if(_adcvalue > 4094)
                {
                    return 100;
                }
                var ADC = _adcvalue;
                var V = (ADC * 3.3 / 4095);
                var R = (10000 * 3.3) / V - 10000;
                var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 9991.6))
                return temperature - 273.15;
			}
		},
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>1; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
        name: 'potentiometer_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[8] * 256 + _pa[7]
			if(adcVal > 4095)
            {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

			return adcVal;
        }
        },
    {
        name: 'potentiometer_voltage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[8] * 256 + _pa[7]) / 4095 * 5;
        }
        },
		{
			name: 'value',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
			
			var adcVal = _pa[28] * 256 + _pa[27]
			if(adcVal > 4095)
            {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

			return adcVal; }
		},
		{
			name: 'expectedValue',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30] * 256 + _pa[29]); }
		},
		{
			name: 'defaultValue',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[32] * 256 + _pa[31]); }
		},
        {
            name: 'ampermeter',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return _typecsv.split(',')[4] > 0;
            }
        },
		{
        name: 'current_r1_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[15] * 256 + _pa[14]);
        }
		},
		{
        name: 'current_r1',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[15] * 256 + _pa[14]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }
			var lCurrent = 8.84 * adcVal / 1000 - 0.127;
			if(lCurrent<0)lCurrent=0;
					
            return lCurrent;
        }
		},
		{
        name: 'current_r2_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[17] * 256 + _pa[16]);
        }
		},
		{
        name: 'current_r2',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[17] * 256 + _pa[16]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }
			var lCurrent = 8.84 * adcVal / 1000 - 0.127;
			if(lCurrent<0)lCurrent=0;
					
            return lCurrent;
        }
		},
		{
        name: 'current_r3_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[19] * 256 + _pa[18]);
        }
		},
		{
        name: 'current_r3',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[19] * 256 + _pa[18]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

            var lCurrent = 8.84 * adcVal / 1000 - 0.127;
			if(lCurrent<0)lCurrent=0;
					
            return lCurrent;
        }
		},
		{
			name: 'temperatureadc',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [21] * 256 + _pa [20]);
			}
		},
		{
			name: 'defaultvaluebt',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [25] * 256 + _pa [24]);
			}
		},
		{
			name: 'defaultvalueot',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [23] * 256 + _pa [22]);
			}
		},
        {
            name: 'rcon',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[35];
            }
        },
        {
            name: 'brownout',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
				if((_pa[35] & 0x02) == 0x02)
				{
					return (_pa[35] & 0x01) < 1;
				}
				else 
				{
					return false;
				}
            }
        },
        {
            name: 'poweronreset',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[35] & 0x02) < 1;
            }
        },
        {
            name: 'wdt',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[35] & 0x08)  < 1;
            }
        },
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[35];
            }
        }
        
    ];

     this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetValue',
            method: function(_arg) {
				if (_arg.split(',').length==3)
                {
                    var value = Number(_arg.split(',')[0]);
                    var min = Number(_arg.split(',')[1]);
                    var max = Number(_arg.split(',')[2]);
					//var rev = Number(_arg.split(',')[3]);

                    if((value != min)&&(value != max))
					{
						
						value = GetCorrectedPercent(value,min,max);
                    
					}
					
                    
                    var valueMsb = Number(value) / 256;//value from 0 to 4095
                    var valueLsb = Number(value) % 256;//value from 0 to 4095
                    return btoa([0x1f,0x43,valueLsb,valueMsb]);
                }
                else
                {
					if(_arg > 4095)
					{
						_arg = 4095;
					}
					var valueMsb = Number(_arg) / 256;//value from 0 to 4095
					var valueLsb = Number(_arg) % 256;//value from 0 to 4095
					
					return btoa([0x1f,0x43,valueLsb,valueMsb]);
				}
            }
        },
		{
            name: 'SetDefaultValue',
            arguments: [{name:'defaultActuatorTempValue',type:'byte'},{name:'defaultActuatorValueBt',type:'byte'},{name:'defaultActuatorValueOt',type:'byte'}],
            method: function(_arg)
            {
                var split = _arg.split(',');
                var i=0;
                var output = [0x1f, 0x44, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF ];
				
				var adcVal = adcValueOfTemperatureC(Number(split[i]));
                output[2] = adcVal >> 8;
				output[2+1] = adcVal & 0xFF;
                for (i=1;i<split.length;i++)
                {
					adcVal = Number(split[i]);
                    output[2+i*2] = adcVal >> 8;
					output[2+i*2+1] = adcVal & 0xFF;
                }
                return btoa(output);
            }
        },
		{
            name: 'SetRelay',
            arguments: [{name:'relay0',type:'byte'},{name:'relay1',type:'byte'},{name:'relay2',type:'byte'},{name:'relay3',type:'byte'}],
            method: function(_arg)
            {
                var split = _arg.split(',');
                var i=0;
                var output = [0x1f, 0x42, 2, 2, 2, 2 ];
                for (i=0;i<split.length;i++)
                {
                    output[2+i] = Number(split[i]);
                }
                return btoa(output);
            }
        },
		{
            name: 'SetDebugRelay',
            method: function(_arg) {
                return btoa([0x1f,0x45]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];


    this.DefaultBlob = {"name":""};