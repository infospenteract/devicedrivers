 var TemperatureC = function(_adcvalue)
{
	if(_adcvalue > 4094)
	{
		return 100;
	}
	var ADC = _adcvalue;
	var V = (ADC * 3.3 / 4095);
	var R = (10000 * 3.3) / V - 10000;
	var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 9991.6))
	return temperature - 273.15;
}

var adcValueOfTemperatureC = function(tempC)
{
	if(tempC > 99)
	{
		return 65535;
	}
	var R = 9991.6 * Math.exp(3950/(tempC + 273.15) - 3950/298.15);
	var V = 10000 * 3.3 / (R + 10000);
	var ADC = V * 4095 / 3.3;
	
	return Math.round(ADC);	
}

var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

this.PropertiesList = [
    {
        name: 'lastoverridetime',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['lastoverridetime']; }
    },{
            name: 'manualvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if(_json['manualvalue'] === undefined){
                    return 0;
                }
                return _json['manualvalue']; }
        },{
        name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
    },{
        name: 'driverinfos',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
    },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },{
            name: 'relayimage0',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['relayimage0']; }
        },
        {
            name: 'maxpower',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['maxpower']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
        },
        {
            name: 'connectedobject',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['connectedobject']; }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'encrypted',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return false;
        }
        },
    {
        name: 'relaisid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var out = "0x";
            var i = 0;
            for (i = 0; i < 4; i++) {
                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
            }
            return out;
        }
        },
    
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'supplyad',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[8] * 256 + _pa[7];
        }
        },
    {
        name: 'supplyvoltage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2;
        }
        },
    
		{
        name: 'washAsked',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var valtmp = (_pa[28])&8;
			if(valtmp == 8)
				return 1;
			else
				return 0;	
				
        }
        },
		{
        name: 'washing',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var valtmp = (_pa[28])&4;
			if(valtmp == 4)
				return 1;
			else
				return 0;
			
				
        }
        },
		{
        name: 'deIcingAsked',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var valtmp = (_pa[28])&2;
			if(valtmp == 2)
				return 1;
			else
				return 0;
				
        }
        },
		{
        name: 'deIcing',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var valtmp = (_pa[28])&1;
			if(valtmp == 1)
				return 1;
			else
				return 0;
			
				
        }
        },
		{
        name: 'deIcingOrder',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[15]);
			
				
        }
        },
    {
        name: 'fanEvacAsked',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            
			return (_pa[17] * 256 + _pa[16]);
        }
		},
    
	{
        name: 'fanEvacSpeed',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[19] * 256 + _pa[18]);
        }
		},
	{
        name: 'fanEvacPercent',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			var perc = (_pa[19] * 256 + _pa[18])*100/4095;
			if(perc > 100)
				perc = 100;
			return Math.round(perc);
			
        }
		},
	{
        name: 'fanInputAsked',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			return (_pa[23] * 256 + _pa[22]);
        }
		},	
	{
        name: 'fanInputSpeed',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[27] * 256 + _pa[26]);
        }
		},
	{
        name: 'fanInputPercent',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var perc = (_pa[27] * 256 + _pa[26])*100/4095;
			if(perc > 100)
				perc = 100;
			return Math.round(perc);
        }
		},	
    
        {
            name: 'softwarename',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
                return _typecsv.split(',')[0];
            }
        },
		{
			name: 'currentadc',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa[30] * 256 + _pa[29]);
			}
		},
		{
			name: 'current',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				var adcVal = _pa[30] * 256 + _pa[29]
				if(adcVal > 4095)
				{
					//means negative value. If the there is noise one the ground of the ADC we might read negative value
					adcVal = 0;
				}
				var lCurrent = adcVal * 3.3 / 4095.0 * 1012 / 120.0 * 1.14 + 0.463; 
				if(lCurrent < 0.5)
				{
					return 0;
				}
				return lCurrent;
			}
		},
		{
			name: 'temperature1adc',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [32] * 256 + _pa [31]);
			}
		},
		{
			name: 'temperature1inc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [32] * 256 + _pa [31]);
			}
		},
		{
			name: 'temperature1inf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureF(TemperatureC(_pa [32] * 256 + _pa [31]));
			}
		},
		{
			name: 'temperature2adc',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [21] * 256 + _pa [20]);
			}
		},
		{
			name: 'temperature2inc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [21] * 256 + _pa [20]);
			}
		},
		{
			name: 'temperature2inf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureF(TemperatureC(_pa [21] * 256 + _pa [20]));
			}
		},
		{
			name: 'defaulttemperatureinc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [25] * 256 + _pa [24]);
			}
		},
		{
			name: 'defaulttemperatureinf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureF(TemperatureC(_pa [25] * 256 + _pa [24]));
			}
		},
		{
			name: 'dacdefaultvaluebt',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [34] * 256 + _pa [33]);
			}
		},
		{
			name: 'dacdefaultvalueot',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [36] * 256 + _pa [35]);
			}
		},
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[37];
            }
        },
        {
            name: 'cfm/btu',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['cfm/btu']; }
        },{
            name: 'minpow',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['minpow']; }
        },{
            name: 'recirculation',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['recirculation']; }
        },{
            name: 'minimalventpriority',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['minimalventpriority']; }
        }
		
    ];

this.FunctionList = [
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
    {
        name: 'SetValue',
		arguments: [{
            name: 'dac0val',
            type: 'int'
        }, {
            name: 'dac1val',
            type: 'int'
        }, {
            name: 'relay1val',
            type: 'byte'
        }, {
            name: 'relay2val',
            type: 'byte'
        }],
        method: function (_arg) {
			var split = _arg.split(',');
			var dac0Value = Number(split[0]);
			var dac1Value = Number(split[1]);
			var rel1value = Number(split[2]);
			var rel2value = Number(split[3]);
			
			var output = [0x1f, 0x44, 0, 0, 0, 0, 0, 0];
			
			if (dac0Value > 4095) {
                dac0Value = 4095;
            }
			if (dac1Value > 4095) {
                dac1Value = 4095;
            }
			
			if(dac0Value > 0)
			{
				output[3] = Number(dac0Value) / 256;
				output[2] = Number(dac0Value) % 256;
			}
            if(dac1Value > 0)
			{
				output[5] = Number(dac1Value) / 256;
				output[4] = Number(dac1Value) % 256;
			}
            if(rel1value > 0){
				output[6] = 1;
			}
			if(rel2value > 0){
				output[7] = 1;
			}
				
           return btoa(output);
        }
        },
	{
        name: 'SetFans',
		arguments: [{
            name: 'fan1dac',
            type: 'int'
        }, {
            name: 'fan2dac',
            type: 'int'
        }],
        method: function (_arg) {
            var split = _arg.split(',');
			var dac0Value = Number(split[0]);
			var dac1Value = Number(split[1]);
			var output = [0x1f, 0x4A, 0, 0, 0, 0];
			
			if (dac0Value > 4095) {
                dac0Value = 4095;
            }
			if (dac1Value > 4095) {
                dac1Value = 4095;
            }
			
			if(dac0Value > 0)
			{
				output[3] = Number(dac0Value) / 256;
				output[2] = Number(dac0Value) % 256;
			}
            if(dac1Value > 0)
			{
				output[5] = Number(dac1Value) / 256;
				output[4] = Number(dac1Value) % 256;
			}
			
            
            return btoa(output);
        }
        },	
	{
        name: 'SetEvac',
        method: function (_arg) {
            if (_arg > 4095) {
                _arg = 4095;
            }
            var valueEvac = Number(_arg); //value from 0 to 4095
			var output = [0x1f, 0x46, 0, 0];
            if(valueEvac > 0)
			{
				output[3] = Number(valueEvac) / 256;
				output[2] = Number(valueEvac) % 256;
			}
            return btoa(output);
        }
        },
	{
        name: 'SetInput',
        method: function (_arg) {
			if (_arg > 4095) {
                _arg = 4095;
            }
            var valueEntree = Number(_arg); //value from 0 to 4095
			var output = [0x1f, 0x49, 0, 0];
            if(valueEntree > 0)
			{
				output[3] = Number(valueEntree) / 256;
				output[2] = Number(valueEntree) % 256;
			}
            return btoa(output);
			
            
        }
        },	
	{
        name: 'SetWash',
		arguments: [{
            name: 'washonoff',
            type: 'int'
        }, {
            name: 'washtime',
            type: 'int'
        }],
        method: function (_arg) {
            
            var split = _arg.split(',');
			var washonoff = Number(split[0]);
			var washtime = Number(split[1]); //temps en secondes
            if(washtime > 255)
				washtime = 255;
            return btoa([0x1f, 0x47, washonoff, washtime]);
        }
        },
	{
        name: 'SetDeicing',
		arguments: [{
            name: 'deicingonoff',
            type: 'int'
        }, {
            name: 'deicingtime',
            type: 'int'
        }],
        method: function (_arg) {
            var split = _arg.split(',');
			var deicingonoff = Number(split[0]);
			var deicingtime = Number(split[1]);//temps en minutes
            if(deicingtime > 255)
				deicingtime = 255;
            
            return btoa([0x1f, 0x48, deicingonoff,deicingtime]);
        }
        },
	{
        name: 'SetDeIcingOrder',
        method: function (_arg) {
            
            var valueOrder = Number(_arg); //
			var output = [0x1f, 0x4B, 0];
            
			output[2] = valueOrder;
			
            return btoa(output);
        }
        },	
	{
            name: 'SetDefaultValue',
            arguments: [{name:'temp',type:'byte'},{name:'dacValueBt',type:'byte'},{name:'dacValueOt',type:'byte'}],
            method: function(_arg)
            {
                var split = _arg.split(',');
                var i=0;
                var output = [0x1f, 0x45, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF ];
				
				var adcVal = adcValueOfTemperatureC(Number(split[i]));
                output[2] = adcVal >> 8;
				output[2+1] = adcVal & 0xFF;
                for (i=1;i<split.length;i++)
                {
					adcVal = Number(split[i]);
                    output[2+i*2] = adcVal >> 8;
					output[2+i*2+1] = adcVal & 0xFF;
                }
                return btoa(output);
            }
        },
    
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        }
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
