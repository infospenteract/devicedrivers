//*
// * voici ce qui doit être dans le driver et le blob pour les version 209 et +
// * le capteur est alimenté à 3.3V et on a un gain de 128 sur l'ampli et l'ampli est sur 24 bits donc 16 777 215 bits
// * si par exemple on a un capteur de 2mV/V sur un maximum de 100Kg et qu'il y a 1 capteur
// * pour avoir la valeur de la calibrationValue on doit faire 
// * 128 x 16 777 215 x (2x10-3)
// * --------------------------- = 42 950 bit/Kg
// * 100kg x 1 capteurs
//* 
// * donc:
// * 2 147 483 x A(en mv)
// * -------------------
// *  B (en kg) x C(en nombre de capteurs)
// * 
// * on doit diviser par 100 la valeur à envoyer par le emmesh pour que ca entre dans 65 535
// * donc la valeur du calcul ne doit jamais dépasser 65 535.
// * on ne doit pas avoir plus de 3mV/V pour 1 capteur si on veut jamais depasser le 65 535
// * par contre si on utilise 2 capteurs, on peut monter jusqua 6mV/V
// bref on doit prendre la valeur envoyée par le emmesh, la multiplier par 100 et la passer dans la fonction WeightInG()
// * 
// */


var WeightGorKG = function (_json, ADCvalue) {
    try {
        var A = Number(_json["options"].split(',')[0]); //mv/V
        var B = Number(_json["options"].split(',')[1]); //valeur max en Kg
        var C = Number(_json["options"].split(',')[2]); //nombre de capteurs

    } catch (err) {
        var A = 2;
        var B = 100; //100Kg
        var C = 1; //1 capteur
    }


    var pente = 2147483 * A / (B * C); //retoune une valeur en bit/kg
    if (C > 1) {

        return Math.round(ADCvalue / pente); //affiche en Kg
    }
    if (C === 1) {
        var yy = Math.round((ADCvalue / pente) * 1000); //affiche en G
        if (yy > (B * 1000)) //si la scale a un max de 250Kg, impossible d'afficher plus de 250 000g
        {
            yy = 0;
        }
        return yy;
    }

}
var WeightinBits = function (_json, WeightinGorKg) {
    try {
        var A = Number(_json["options"].split(',')[0]); //mv/V
        var B = Number(_json["options"].split(',')[1]); //valeur max en Kg
        var C = Number(_json["options"].split(',')[2]); //nombre de capteurs

    } catch (err) {
        var A = 2;
        var B = 100; //100Kg
        var C = 1; //1 capteur
    }

    var pente = 2147483 * A / (B * C); //retoune une valeur en bit/kg

    if (C > 1) //le poids entre est en kg
    {
        var val = pente * WeightinGorKg;
    }
    if (C === 1) //le poids entré est en g
    {
        var val = pente * WeightinGorKg / 1000;
    }
    return val;

}

this.PropertiesList = [
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'calib_done',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var caldone = _pa[7];
            if (caldone == 0) {
                return "default calibration";
            } else if (caldone == 1) {
                return "only tar done";
            } else if (caldone == 2) {
                return "only span done";
            } else if (caldone == 3) {
                return "fully calibrated";
            } else {
                return "unknown";
            }
        }
        },
    {
        name: 'weight0adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[15] * 256 + _pa[14]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);
        }
        },
    {
        name: 'weight1adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[17] * 256 + _pa[16]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight2adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[19] * 256 + _pa[18]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight3adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[21] * 256 + _pa[20]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight4adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[23] * 256 + _pa[22]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight5adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[25] * 256 + _pa[24]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight6adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[27] * 256 + _pa[26]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight7adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[29] * 256 + _pa[28]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'weight8adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[31] * 256 + _pa[30]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
    {
        name: 'sampleWeightadc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var WeightInBit = (_pa[33] * 256 + _pa[32]) * 100; //on a divisé par 100 pour pas depasser 65535

            return (WeightInBit);

        }
        },
	{
        name: 'thresholdWeight',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[37] * 256 + _pa[36];
        }
        },	

    {
        name: 'actualIndex',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {

            return (_pa[34]);

        }
        },

    {
        name: 'resetCounter',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[35];
        }
        }

    ];

this.FunctionList = [
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
            message.push(0);
            message.push(0);
            if (sendNode) {
                message[2] |= 4;
                message[3] |= 4;
                message[4] |= 4;
            }

            if (sendGroup) {
                message[2] |= 2;
                message[3] |= 2;
                message[4] |= 2;
            }

            if (sendForceRelayTime) {
                message[2] |= 8;
                message[3] |= 8;
                message[4] |= 8;
            }

            if (sendAmpSettings && txPower != 255 && useAmp != 2) {
                message[2] |= 1;
                message[3] |= 1;
                message[4] |= 1;
            }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        },
    {
        name: 'SetTar',
        arguments: [{
            name: 'setTarValueToZero',
            type: 'bool'
        }, {
            name: 'resetTar',
            type: 'bool'
        }],
        method: function (_arg) {

            var split = _arg.split(',');
            var output = [0x1f, 0x41, 0x54, 0x41, 0x52, 0x00, 0x00];
            output[5] = Number(split[0]);
            output[6] = Number(split[1]);

            return btoa(output);
        }
        },
    {
        name: 'SetCalibration',
        arguments: [{
            name: 'calibrationWeight',
            type: 'int',
            default: 0
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var calibrationValue = Number(split[0]);
            var valueMsb = 0;
            var valueLsb = 1;

            if (calibrationValue > 0) {
                valueMsb = Number(calibrationValue) / 256;
                valueLsb = Number(calibrationValue) % 256;
            }

            var message = []
            message.push(0x1F);
            message.push(0x42);
            message.push(0x43);
            message.push(0x41);
            message.push(0x4C);
            message.push(valueLsb);
            message.push(valueMsb);
            message.push(0x00);

            return btoa(message);
        }
        },
    {
        name: 'SetThreshold',
        arguments: [{
            name: 'thresholdWeight',
            type: 'int',
            default: 0
        }],
        method: function (_arg) {
            var split = _arg.split(',');


            var A = Number(split[0]); //mv/V
            var B = Number(split[1]); //valeur max en Kg
            var C = Number(split[2]); //nombre de capteurs
            var D = Number(split[3]); //threshold en G



            var pente = 2147483 * A / (B * C); //retoune une valeur en bit/kg

            var THValue = pente * D / 1000; //on rentre le threshold en G
            THValue = THValue / 100; //pour pas depasser 65535
            //if(C>1)
            //{
            //	var calibrationValue = pente * D ;
            //}
            //if(C===1)
            //{
            //	var calibrationValue = pente * D / 1000;
            //}

            var valueMsb = 0;
            var valueLsb = 1;

            if (THValue > 0) {
                valueMsb = Number(THValue) / 256;
                valueLsb = Number(THValue) % 256;
            }

            var message = []
            message.push(0x1F);
            message.push(0x43);
            message.push(0x54); //T
            message.push(0x48); //H
            message.push(0x52); //R
            message.push(valueLsb);
            message.push(valueMsb);
            message.push(0x00);

            return btoa(message);
        }
        }
		// {
        // name: 'SetThreshold',
        // arguments: [{
            // name: 'thresholdWeight',
            // type: 'int',
            // default: 0
        // }],
        // method: function (_arg) {
            // var split = _arg.split(',');
            // var calibrationValue = Number(split[0]);//la valeur dans le emscale est en G...un jour on uniformisera avec le emSiloScale
			// var valueMsb = 0;
			// var valueLsb = 1;

			// if(calibrationValue > 0)
			// {
                // valueMsb = Number(calibrationValue) / 256;
				// valueLsb = Number(calibrationValue) % 256;
			// }

            // var message = []
            // message.push(0x1F);
            // message.push(0x43);
            // message.push(0x54);
            // message.push(0x48);
            // message.push(0x52);
            // message.push(valueLsb);
            // message.push(valueMsb);
            // message.push(0x00);

            // return btoa(message);
        // }
        // }

    ];
