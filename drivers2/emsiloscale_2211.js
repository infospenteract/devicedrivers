//*
// * voici ce qui doit être dans le driver et le blob pour les version 210 et +
// * le capteur est alimenté à 3.3V et on a un gain de 128 sur l'ampli et l'ampli est sur 24 bits donc 16 777 215 bits
// * si par exemple on a un capteur de 2mV/V sur un maximum de 100Kg et qu'il y a 1 capteur
// * pour avoir la valeur de la calibrationValue on doit faire 
// * 128 x 16 777 215 x (2x10-3)
// * --------------------------- = 42 950 bit/Kg
// * 100kg x 1 capteurs
//* 
// * donc:
// * 2 147 483 x A(en mv)
// * -------------------
// *  B (en kg) x C(en nombre de capteurs)
// * 
// * 
// * 
// * 
// * 
// */


this.PropertiesList = [
    {
        name: 'encrypted',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return false;
        }
        },
    {
        name: 'relaisid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var out = "0x";
            var i = 0;
            for (i = 0; i < 4; i++) {
                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
            }
            return out;
        }
        },
    
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'calib_done',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
             var caldone = _pa[7];
			 if(caldone == 0)
			 {
				 return "default calibration";
			 }
			 else if(caldone == 1)
			 {
				 return "only tar done";
			 }
			 else if(caldone == 2)
			 {
				 return "only span done";
			 }
			 else if(caldone == 3)
			 {
				 return "fully calibrated";
			 }
			 else
			 {	 
				return "unknown";
			 }
        }
        },
	{
        name: 'weightADC',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			var WeightInBit = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
			
            return WeightInBit;
            
        }
        },	
	{
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[34];
            }
        }	
    
    ];

this.FunctionList = [
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        },
    {
        name: 'SetTar',
		arguments: [{name:'setTarValueToZero',type:'bool'},{name:'resetTar',type:'bool'}],
        method: function (_arg) 
		{
			
			var split = _arg.split(',');
			var output = [0x1f, 0x41, 0x54, 0x41, 0x52, 0x00, 0x00];
			output[5] = Number(split[0]);
			output[6] = Number(split[1]);
			
			return btoa(output);
        }
        },
    {
        name: 'SetCalibration',
        arguments: [{
            name: 'calibrationWeight',
            type: 'int',
            default: 0
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var calibrationValue = Number(split[0]);
			var valueMsb = 0;
			var valueLsb = 1;
				
			if(calibrationValue > 0)
			{
                valueMsb = Number(calibrationValue) / 256;
				valueLsb = Number(calibrationValue) % 256;
			}
				
            var message = []
            message.push(0x1F);
            message.push(0x42);
            message.push(0x43);
            message.push(0x41);
            message.push(0x4C);
            message.push(valueLsb);
            message.push(valueMsb);
            message.push(0x00);

            return btoa(message);
        }
        },
		{
        name: 'SetThreshold',
        arguments: [{
            name: 'thresholdWeight',
            type: 'int',
            default: 0
        }],
        method: function (_arg) {
            var split = _arg.split(',');
			
			
			var A = Number(split[0]);//mv/V
			var B = Number(split[1]);//valeur max en Kg
			var C = Number(split[2]);//nombre de capteurs
			var D = Number(split[3]);//threshold en KG
					
				
				
			var pente = 2147483 * A /(B * C);//retoune une valeur en bit/kg
				
				
			
			if(C>1)
			{
				var calibrationValue = pente * D ;
			}
			if(C===1)
			{
				var calibrationValue = pente * D / 1000;
			}
				
			var valueMsb = 0;
			var valueLsb = 1;
				
			if(calibrationValue > 0)
			{
                valueMsb = Number(calibrationValue) / 256;
				valueLsb = Number(calibrationValue) % 256;
			}
				
            var message = []
            message.push(0x1F);
            message.push(0x43);
            message.push(0x54);
            message.push(0x48);
            message.push(0x52);
            message.push(valueLsb);
            message.push(valueMsb);
            message.push(0x00);

            return btoa(message);
        }
        }
		// {
        // name: 'SetThreshold',
        // arguments: [{
            // name: 'thresholdWeight',
            // type: 'int',
            // default: 0
        // }],
        // method: function (_arg) {
            // var split = _arg.split(',');
            // var calibrationValue = Number(split[0]);//la valeur dans le emscale est en G...un jour on uniformisera avec le emSiloScale
			// var valueMsb = 0;
			// var valueLsb = 1;
				
			// if(calibrationValue > 0)
			// {
                // valueMsb = Number(calibrationValue) / 256;
				// valueLsb = Number(calibrationValue) % 256;
			// }
				
            // var message = []
            // message.push(0x1F);
            // message.push(0x43);
            // message.push(0x54);
            // message.push(0x48);
            // message.push(0x52);
            // message.push(valueLsb);
            // message.push(valueMsb);
            // message.push(0x00);

            // return btoa(message);
        // }
        // }
	
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
