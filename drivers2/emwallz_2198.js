
var TemperatureC = function(_adcvalue)
{
	if(_adcvalue > 4094)
	{
		return 100;
	}
	var ADC = _adcvalue;
	var V = (ADC * 3.3 / 4095);
	var R = (10000 * 3.3) / V - 10000;
	var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 10000.0))
	return temperature - 273.15;
}

var adcValueOfTemperatureC = function(tempC)
{
	if(tempC > 99)
	{
		return 65535;
	}
	var R = 10000 * Math.exp(3950/(tempC + 273.15) - 3950/298.15);
	var V = 10000 * 3.3 / (R + 10000);
	var ADC = V * 4095 / 3.3;
	
	return Math.round(ADC);	
}

var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

    this.PropertiesList = [
        {
            name: 'statlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return "wallstate"
            }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>0; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		{
            name: 'forcerelayfactor',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[20]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
        name: 'potentiometer_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[8] * 256 + _pa[7]
			if(adcVal > 4095)
            {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

			return adcVal;
        }
        },
		{
        name: 'potentiometer_voltage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[8] * 256 + _pa[7]) / 4095 * 5;
        }
        },
		{
			name: 'relaystate0',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[27] & 1) > 0; }
		},
		{
			name: 'relaystate1',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[27] & 2) > 0; }
		},
		{
			name: 'relaystate2',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[27] & 4) > 0; }
		},
        {
            name: 'ampermeter',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return _typecsv.split(',')[4] > 0;
            }
        },
		{
        name: 'current_r1_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[15] * 256 + _pa[14]);
        }
		},
		{
        name: 'current_r1',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[15] * 256 + _pa[14]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }
			var lCurrent = 8.84 * adcVal / 1000 - 0.127;
			if(lCurrent<0)lCurrent=0;
					
            return lCurrent;
        }
		},
		{
        name: 'current_r2_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[17] * 256 + _pa[16]);
        }
		},
		{
        name: 'current_r2',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[17] * 256 + _pa[16]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }
			var lCurrent = 8.84 * adcVal / 1000 - 0.127;
			if(lCurrent<0)lCurrent=0;
					
            return lCurrent;
        }
		},
		{
        name: 'current_r3_adc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[19] * 256 + _pa[18]);
        }
		},
		{
        name: 'current_r3',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[19] * 256 + _pa[18]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

            var lCurrent = 8.84 * adcVal / 1000 - 0.127;
			if(lCurrent<0)lCurrent=0;
					
            return lCurrent;
        }
		},
        {
            name: 'wallstate',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[30]; }
        },
        {
            name: 'expectedwallstate',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[31]; }
        },
        {
            name: 'inflatedelay',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[32] * 30; }
        },
        {
            name: 'deflatedelay',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[21] * 30; }
        },
		{
			name: 'temperatureinc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [34] * 256 + _pa [33]);
			}
		},
		{
			name: 'temperatureinf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureF(TemperatureC(_pa [34] * 256 + _pa [33]));
			}
		},
		{
			name: 'defaulttempinc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) 
			{
				return TemperatureC(_pa [36] * 256 + _pa [35]);
			}
		},
		{
			name: 'defaulttempinf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) 
			{
				return TemperatureF(TemperatureC(_pa [36] * 256 + _pa [35]));
			}
		},
		{
			name: 'defaultwallstatebt',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[37] & 0xF0) >> 4; }
		},
		{
			name: 'defaultwallstateot',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[37] & 0x0F); }
		},
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[25];
            }
        }
        
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetWallState',
            arguments: [{name:'wallstate',type:'byte'}],
            method: function(_arg)
            {
                var split = _arg.split(',');
                var i=0;
                var output = [0x1f, 0x42, 2];
                for (i=0;i<split.length;i++)
                {
                    output[2+i] = Number(split[i]);
                }
                return btoa(output);
            }
        },
		{
            name: 'SetDefaultValue',
            arguments: [{name:'temp1',type:'byte'},{name:'defwallstatebt',type:'byte'},{name:'defwallstateot',type:'byte'},{name:'inflateDelay',type:'byte'},{name:'deflateDelay',type:'byte'}],
            method: function(_arg)
            {
                var split = _arg.split(',');
                var i=0;
                var output = [0x1f, 0x43, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF ];

				var adcVal = adcValueOfTemperatureC(Number(split[0]));
                output[2] = adcVal >> 8;
				output[3] = adcVal & 0xFF;
				output[4] = Number(split[1]);
				output[5] = Number(split[2]);
				
				var delay = Number(split[3]);
				delay /= 30;
				
				if(delay > 62)
				{
					delay = 62;
				}
				else if(delay == 0)
				{
					delay = 6;
				}
				output[6] = delay;
				
				delay = Number(split[4]);
				delay /= 30;
				
				if(delay > 62)
				{
					delay = 62;
				}
				else if(delay == 0)
				{
					delay = 6;
				}
				output[7] = delay;

                return btoa(output);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];
