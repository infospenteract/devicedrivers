 var adcToDegree = function(_adcvalue)
{
	if(_adcvalue >= 0 && _adcvalue <= 128)
	{
		return 0;
	}
	else if(_adcvalue > 3967 && _adcvalue <= 4095)
	{
		return 0;
	}
	else if(_adcvalue > 128 && _adcvalue <= 384)
	{
		return 22.5;
	}
	else if(_adcvalue > 384 && _adcvalue <= 640)
	{
		return 45;
	}
	else if(_adcvalue > 640 && _adcvalue <= 896)
	{
		return 67.5;
	}
	else if(_adcvalue > 896 && _adcvalue <= 1152)
	{
		return 90;
	}
	else if(_adcvalue > 1152 && _adcvalue <= 1408)
	{
		return 112.5;
	}
	else if(_adcvalue > 1408 && _adcvalue <= 1664)
	{
		return 135;
	}
	else if(_adcvalue > 1664 && _adcvalue <= 1920)
	{
		return 157.5;
	}
	else if(_adcvalue > 1920 && _adcvalue <= 2176)
	{
		return 180;
	}
	else if(_adcvalue > 2176 && _adcvalue <= 2432)
	{
		return 202.5;
	}
	else if(_adcvalue > 2432 && _adcvalue <= 2688)
	{
		return 225;
	}
	else if(_adcvalue > 2688 && _adcvalue <= 2944)
	{
		return 247.5;
	}
	else if(_adcvalue > 2944 && _adcvalue <= 3200)
	{
		return 270;
	}
	else if(_adcvalue > 3200 && _adcvalue <= 3456)
	{
		return 292.5;
	}
	else if(_adcvalue > 3456 && _adcvalue <= 3712)
	{
		return 315;
	}
	else if(_adcvalue > 3712 && _adcvalue <= 3967)
	{
		return 337.5;
	}
	else
	{
		return 404;
	}
}
 
 
var adcToCardinal = function(_adcvalue)
{
	if(_adcvalue >= 0 && _adcvalue <= 128)
	{
		return "N";
	}
	else if(_adcvalue > 3967 && _adcvalue <= 4095)
	{
		return "N";
	}
	else if(_adcvalue > 128 && _adcvalue <= 384)
	{
		return "NEN";
	}
	else if(_adcvalue > 384 && _adcvalue <= 640)
	{
		return "NE";
	}
	else if(_adcvalue > 640 && _adcvalue <= 896)
	{
		return "ENE";
	}
	else if(_adcvalue > 896 && _adcvalue <= 1152)
	{
		return "E";
	}
	else if(_adcvalue > 1152 && _adcvalue <= 1408)
	{
		return "ESE";
	}
	else if(_adcvalue > 1408 && _adcvalue <= 1664)
	{
		return "SE";
	}
	else if(_adcvalue > 1664 && _adcvalue <= 1920)
	{
		return "SES";
	}
	else if(_adcvalue > 1920 && _adcvalue <= 2176)
	{
		return "S";
	}
	else if(_adcvalue > 2176 && _adcvalue <= 2432)
	{
		return "SWS";
	}
	else if(_adcvalue > 2432 && _adcvalue <= 2688)
	{
		return "SW";
	}
	else if(_adcvalue > 2688 && _adcvalue <= 2944)
	{
		return "WSW";
	}
	else if(_adcvalue > 2944 && _adcvalue <= 3200)
	{
		return "W";
	}
	else if(_adcvalue > 3200 && _adcvalue <= 3456)
	{
		return "WNW";
	}
	else if(_adcvalue > 3456 && _adcvalue <= 3712)
	{
		return "NW";
	}
	else if(_adcvalue > 3712 && _adcvalue <= 3967)
	{
		return "NWN";
	}
	else
	{
		return "error";
	}
}


var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

    this.PropertiesList = [
        {
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>0; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		{
            name: 'forcerelayfactor',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[20]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; 
            
			if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
				return ((BattAD - 3.3) * 57.14 + 5);

			return 0;
            
            }
        },
		{
			name: 'isTemperatureValid',
            type: 'string',
			parser: function(_pa, _apa, _json, _typecsv) { 
                var temp = _pa[28] *256 + _pa[27];
				var humidity = _pa[30] *256 + _pa[29];
                if (temp == 32767)
				{
					return "Error code 2";
				}
				else if (temp == 0 && humidity == 0)
				{
					return "Error code 1"
				}
				else
				{
					return "Valid";
				}
            }
		},
        {
            name: 'temperatureincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[28] *256 + _pa[27];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10; 
            }
        },
		{
			name: 'currentempinfahrenheit',
			type: 'double',
			parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[28] *256 + _pa[27];
                if (value>32767)
                    value = -(65536-value);
                
                return TemperatureF(value/10);
          }
        },
		{
			name: 'isHumidityValid',
            type: 'string',
			parser: function(_pa, _apa, _json, _typecsv) { 
				var temp = _pa[28] *256 + _pa[27];
                var humidity = _pa[30] *256 + _pa[29];
                if (humidity == 65535)
				{
					return "Error code 2";
				}
				else if (temp == 0 && humidity == 0)
				{
					return "Error code 1"
				}
				else
				{
					return "Valid";
				}
                return value/10; 
            }
		},
        {
            name: 'currenthumiditypercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30] *256+ _pa[29]); }
        },
		
        {
            name: 'windspeedadc',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
			return (_pa[34] *256+ _pa[33]); 
			}
        },
		{
            name: 'currentwindspeed',//kmh
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
			
			return ( _pa[34] *256+ _pa[33])*162/4095;
			}
        },
		{
            name: 'windspeedmph',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
			
			return (( _pa[34] *256+ _pa[33])*162/4095)*0.621371;
			
			}
        },
		{
            name: 'maxwindspeedkmh',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
				
				return ( _pa[23] *256+ _pa[22])*162/4095;
			}
        },
        {
            name: 'raincount',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[36] *256+ _pa[35]); }
        },
		
        {
            name: 'winddirectionadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[25] *256+ _pa[24]); }
        },
        {
            name: 'currentwinddirection',//degree
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return adcToDegree(_pa[25] *256+ _pa[24]); }
        },
        {
            name: 'winddirection',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return adcToCardinal(_pa[25] *256+ _pa[24]); }
        },
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[37];
            }
        }
        
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
				message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};