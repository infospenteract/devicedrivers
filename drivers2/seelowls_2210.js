this.PropertiesList = [
        {
            name: 'statlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return "v_weightinkg,v_percentage,v_height,v_dayweightinkg,battvoltage,chargeevaluator,v_sunpercentage"
            }
     },
    {
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
        name: 'encrypted',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return false;
        }
        },
    {
        name: 'relaisid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var out = "0x";
            var i = 0;
            for (i = 0; i < 4; i++) {
                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
            }
            return out;
        }
        },
    {
        name: 'chargingstate',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[26] > 0;
        }
        },
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'batteryad',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[8] * 256 + _pa[7];
        }
        },
    {
        name: 'battvoltage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2;
        }
        },
    {
        name: 'battpercentage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var BattAD;
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2;

            if (BattAD >= 4.2)
				return 100;
            if (BattAD > 3.95)
                return ((BattAD - 3.95) * 80 + 80);
            if (BattAD > 3.8)
                return ((BattAD - 3.80) * 133.3333 + 60);
            if (BattAD > 3.65)
                return ((BattAD - 3.65) * 266.3333 + 20);
            if (BattAD > 3.3)
                return ((BattAD - 3.3) * 57.14 + 5);

            return 0;

        }
        },
    {
        name: 'doorstate',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[33];
        }
        },
    {
        name: 'heightmaxinfos',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			var maxInfos = _pa[32] * 256 + _pa[31];
			if(maxInfos == 0)
			{
				return "Valid";
			}
			if(maxInfos == 65535)
			{
				return "Invalid: reset value"
			}
			
			var errorCode = _pa[24] * 256 + _pa[23];
			var errorHappend = maxInfos & 0x8000;
			
			if(errorCode == 15)
			{
				return "Invalid: Distance too short";
			}
			if(errorCode == 16)
			{
				return "Invalid: No echo";
			}
			if(errorHappend == 32768)
			{
				return "Invalid: an error happened";
			}
			
			return "Invalid: unknown reason"
        }
        },
	{
        name: 'heightmininfos',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			var minInfos = _pa[28] * 256 + _pa[27];
			if(minInfos == 0)
			{
				return "Valid";
			}
			if(minInfos == 65535)
			{
				return "Invalid: reset value"
			}
			
			var errorCode = _pa[22] * 256 + _pa[21];
			var errorHappend = minInfos & 0x8000;
			
			if(errorCode == 15)
			{
				return "Invalid: Distance too short";
			}
			if(errorCode == 16)
			{
				return "Invalid: No echo";
			}
			if(errorHappend == 32768)
			{
				return "Invalid: an error happened";
			}
			
			return "Invalid: unknown reason"
        }
        },
    {
        name: 'heightmaxmmraw',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[24] * 256 + _pa[23];
        } //TODO: not finished
        },
	{
        name: 'heightValidity',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			var validinfo = _pa[22] * 256 + _pa[21];
			if(validinfo == 5)
			{
				return "Valid";
			}
			if(validinfo == 4)
			{
				return "Distance too short";
			}
			if(validinfo == 3)
			{
				return "Restart Laser";
			}
			if(validinfo == 2)
			{
				return "min=max=act";
			}
			if(validinfo == 1)
			{
				return "max-min>300";
			}
			if(validinfo == 0)
			{
				return "! INVALID !";
			}
            if(validinfo == 6)
			{
				return "UART ERROR";
			}
			if(validinfo == 7)
			{
				return "no parsing";
			}
        } //TODO: not finished
        },
    {
        name: 'heightmax',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			var Max = _pa[37] * 256 + _pa[36];
			var Min = _pa[35] * 256 + _pa[34];
			if(Min == 65535 && Max == 0)
			{
				return NaN;
			}
            var tmp = (_pa[37] * 256 + _pa[36]) / 1000;
	    if (tmp > 10) tmp = 10;
            return tmp;

        } //TODO: not finished
        },
    {
        name: 'heightmin',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			var Max = _pa[37] * 256 + _pa[36];
			var Min = _pa[35] * 256 + _pa[34];
			if(Min == 65535 && Max == 0)
			{
				return NaN;
			}
            var tmp = (_pa[35] * 256 + _pa[34]) / 1000;
	    if (tmp > 10) tmp = 10;
            return tmp;
        } //TODO: not finished
        },
    {
        name: 'heightinmeter',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
			var Max = _pa[37] * 256 + _pa[36];
			var Min = _pa[35] * 256 + _pa[34];
			if(Min == 65535 && Max == 0)
			{
				return NaN;
			}
            var min = (_pa[24] * 256 + _pa[23]) / 1000;
            var max = (_pa[24] * 256 + _pa[23]) / 1000;
            if (min < 0)
                min = 0;


            if (max - min > 1)
                min = max - 1;

            var hg = max;
	    if (hg>10) hg = 10;
        
            return hg;

        } //TODO: not finished
        },
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[25];
            }
        }
        

    ];

this.FunctionList = [
    {
        name: 'RefreshWithReading',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x01]);
        }
        },
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
	{
        name: 'Open',
        method: function (_arg) {
            return btoa([0x1f, 0x41, 1, 1, 1]);
        }
        },
	{
        name: 'Close',
        method: function (_arg) {
            return btoa([0x1f, 0x41, 0, 0, 0]);
        }
        },	
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        }
    ];


