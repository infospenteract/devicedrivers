    var CalibratedInHG = function(pressurekpa, heightm, temperaturecelcius)
    {
        var p_inhg = pressurekpa * 0.295301;
        var t_fa = 1.8 * temperaturecelcius + 32;
        var h_ft = 3.2808 * heightm;
        
        //t_fa += 459.67;
        
        var result1 = p_inhg * ((t_fa - 28.630) / (1.1123 * t_fa + 10978));
        
        var result2 = 29.92126 * (1 - (1 / Math.pow(10, ((0.0081350 * h_ft) / (t_fa + (0.00178308 * h_ft))))));
        
        var result = (result1) + (result2 * -1);
        
        //return p_inhg + ":" + h_ft + ":" + t_fa;
        return (p_inhg + result)/0.295301;
    }



    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[2]); }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return 1; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1] * 256 + _pa[0]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[1] * 256 + _pa[0]) / 100; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[1] * 256 + _pa[0]) / 100; 
            
			if (BattAD >= 1.52)
				return 100;
			if (BattAD > 1.42)
				return ((BattAD - 1.42) * 200 + 80);
			if (BattAD > 1.31)
				return ((BattAD - 1.31) * 181.81 + 60);
			if (BattAD > 1.21)
				return ((BattAD - 1.21) * 400 + 20);
			if (BattAD > 1)
				return ((BattAD - 1) * 71.43 + 5);

			return 0;
            
            }
        },
        {
            name: 'temperatureincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[4] *256 + _pa[3];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10; 
            }
        },
        {
            name: 'humiditypercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[6] *256+ _pa[5]); }
        },
        {
            name: 'pressurekilopascalraw',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7])/100; }
        },
        {
            name: 'pressurecount',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7])/10*4096; }
        },
        {
            name: 'pressurerelativeinchofwater',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var pka = (_pa[8] *256+ _pa[7])/100;
                return Math.round(pka*4.02*100)/100;
               /// return Math.round(((count/4096*10)/100-rn)*4.01865);
                
                //return (((_pa[9] * 256 + _pa[8])/100)-rn)*4.01865; 
            }
        },
        {
            name: 'pressurekilopascal',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv)
			{ 
               var pka = (_pa[8] *256+ _pa[7])/100;
               return pka;
            }
        },
        {
            name: 'alertunder',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertunder'] }
        },
        {
            name: 'alertover',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertover'] }
        }
    ];

    this.FunctionList = [
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor", "relativepressure":"101.38"};