
    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[3]); }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return 1; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[2] * 256 + _pa[1]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[2] * 256 + _pa[1]) / 100; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[4] * 256 + _pa[2]) / 100; 
            
			if (BattAD >= 1.52)
				return 100;
			if (BattAD > 1.42)
				return ((BattAD - 1.42) * 200 + 80);
			if (BattAD > 1.31)
				return ((BattAD - 1.31) * 181.81 + 60);
			if (BattAD > 1.21)
				return ((BattAD - 1.21) * 400 + 20);
			if (BattAD > 1)
				return ((BattAD - 1) * 71.43 + 5);

			return 0;
            
            }
        },
		{
            name: 'pressureadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[5] * 256 + _pa[4]); }
        },
        {
            name: 'pressurekpa',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){
				var digitalOffset = 0.5 * Math.pow(2, 24);
				var pressure = ((_pa[6] + _pa[5] * 256 + _pa[4] * 65536) - digitalOffset) / Math.pow(2, 24);// 1.25 * 0.8 = 1 so not needed;
				/*if (pressure > 0.5)
					return Number.NaN;
				else if (pressure < -0.5)
					return Number.NaN;
				else*/
					return pressure;
			}
        },
		{
            name: 'temperature',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){
				var ltemperature = (_pa[9] + _pa[8] * 256 + _pa[7] * 65536) * 125 / Math.pow(2, 24) - 40;
				/*if (pressure > 0.5)
					return Number.NaN;
				else if (pressure < -0.5)
					return Number.NaN;
				else*/
					return ltemperature;
			}
        },
        {
            name: 'pressurerelativeinchofwater',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var pka = (_pa[5] *256+ _pa[4])/100;
                return Math.round(pka*4.02*100)/100;
               /// return Math.round(((count/4096*10)/100-rn)*4.01865);
                
                //return (((_pa[9] * 256 + _pa[8])/100)-rn)*4.01865; 
            }
        }
    ];

    this.FunctionList = [
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor", "relativepressure":"101.38"};