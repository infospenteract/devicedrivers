function Bytes2Float32(bytes) 
	{
        var temp = bytes;
        var g = [0,0,0,0];
        g[0] = Math.floor(bytes/256/256/256);
        bytes = bytes - g[0]*256*256*256;
        g[1] = Math.floor(bytes/256/256);
        bytes = bytes - g[1]*256*256;
        g[2] = Math.floor(bytes/256);
        bytes = bytes - g[2]*256;
        g[3] = bytes;
        
        bytes = temp;
        
		var sign = (g[0] & 0x80) ? -1 : 1;
		var exponent = (g[0] & 0x7F)*2 + (g[1] & 0x80)/0x80 - 0x7F;
		var significand = (g[1] & 0x7F) * 256*256 + g[2]*256 + g[3];

		if (exponent == 128) 
			return sign * ((significand) ? Number.NaN : Number.POSITIVE_INFINITY);

		if (exponent == -127) {
			if (significand == 0) 
            {
                return 0;
            }
			exponent = -126;
            
			significand =signifcand / 0x800000;
		} else 
        {
            if (significand>=0x800000)
            {
                significand = significand/0x800000;
            }
            else
            {
                significand = (significand+0x800000)/0x800000;
            }
        }

		return sign * significand * Math.pow(2, exponent);
	}

    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>1; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; 
            
			if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
				return ((BattAD - 3.3) * 57.14 + 5);

			return 0;
            
            }
        },
		{
			name: 'co2raw',
			type: 'int',
			parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30]*Math.pow(256,3) + _pa[29]*Math.pow(256,2) + _pa[28]*Math.pow(256,1) + _pa[27]); }
		},
		{
			name: 'co2ppm',
			type: 'int',
			parser: function(_pa, _apa, _json, _typecsv)
			{
				var myFloat32 = _pa[30]*Math.pow(256,3) + _pa[29]*Math.pow(256,2) + _pa[28]*Math.pow(256,1) + _pa[27];
				return Math.round(Bytes2Float32(myFloat32));
			}
		},
		{
			name: 'tempRaw',
			type: 'int',
			parser: function(_pa, _apa, _json, _typecsv){ return (_pa[34]*Math.pow(256,3) + _pa[33]*Math.pow(256,2) + _pa[32]*Math.pow(256,1) + _pa[31]); }
		},
		{
			name: 'temperatureincelcius',
			type: 'int',
			parser: function(_pa, _apa, _json, _typecsv)
			{
				//var data = new Uint8Array(4);
				//data[0] = _pa[34];
				//data[1] = _pa[33];
				//data[2] = _pa[32];
				//data[3] = _pa[31];
				//var f32 = new Float32Array(data.buffer);
				//var f32value = f32[0];
				//return f32value;
				
				var myFloat32 = _pa[34]*Math.pow(256,3) + _pa[33]*Math.pow(256,2) + _pa[32]*Math.pow(256,1) + _pa[31];
				return Math.round(Bytes2Float32(myFloat32)*10)/10;
			}
		},
		{
			name: 'humidityRaw',
			type: 'int',
			parser: function(_pa, _apa, _json, _typecsv){ return (_pa[25]*Math.pow(256,3) + _pa[24]*Math.pow(256,2) + _pa[23]*Math.pow(256,1) + _pa[22]); }
		},
		{
			name: 'humiditypercentage',
			type: 'int',
			parser: function(_pa, _apa, _json, _typecsv)
			{
				var myFloat32 = _pa[25]*Math.pow(256,3) + _pa[24]*Math.pow(256,2) + _pa[23]*Math.pow(256,1) + _pa[22];
				return Math.round(Bytes2Float32(myFloat32));
			}
		},
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[35];
            }
        }
        
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        },
		{
			name: 'SetCalibration',
			arguments: [{
				name: 'calibrationCO2',
				type: 'int',
				default: 0
			}],
			method: function (_arg) {
				var split = _arg.split(',');
				var calibrationValue = Number(split[0]);
				var valueMsb = 0;
				var valueLsb = 1;
					
				if(calibrationValue > 0)
				{
					valueMsb = Number(calibrationValue) / 256;
					valueLsb = Number(calibrationValue) % 256;
				}
					
				var message = []
				message.push(0x1F);
				message.push(0x42);
				message.push(0x43);
				message.push(0x41);
				message.push(0x4C);
				message.push(valueLsb);
				message.push(valueMsb);
				message.push(0x00);

				return btoa(message);
			}
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};