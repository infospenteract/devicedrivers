
var TemperatureC = function(_adcvalue)
{
	if(_adcvalue > 4094)
	{
		return 100;
	}
	var ADC = _adcvalue;
	var V = (ADC * 3.3 / 4095);
	var R = (10000 * 3.3) / V - 10000;
	var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 10000.0))
	return temperature - 273.15;
}

var adcValueOfTemperatureC = function(tempC)
{
	if(tempC > 99)
	{
		return 65535;
	}
	var R = 10000 * Math.exp(3950/(tempC + 273.15) - 3950/298.15);
	var V = 10000 * 3.3 / (R + 10000);
	var ADC = V * 4095 / 3.3;
	
	return Math.round(ADC);	
}

var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

    this.PropertiesList = [
        {
            name: 'lastoverridetime',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['lastoverridetime']; }
        },{
            name: 'manualvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if(_json['manualvalue'] === undefined){
                    return 0;
                }
                return _json['manualvalue']; }
        },
		{
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },
		{
            name: 'orientation',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['orientation']; }
        },
		{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'autoallowed',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['autoallowed']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>1; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		{
            name: 'forcerelayfactor',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[34]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; 
            
            if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
                return ((BattAD - 3.3) * 57.14 + 5);
                

			return 0;
            
            }
        },
        {
            name: 'doorStateADC',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[31] * 256 + _pa[30]; }
        },
		{
			name: 'doorState',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
				return _pa[27];
			}
		},
		{
			name: 'doorStateint',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
				return _pa[27];
			}
		},
		{
			name: 'doorStateEnum',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
				if(_pa[27] == 0)
				{
					return "Close";
				}
				else if(_pa[27] == 1)
				{
					return "Open"; 
				}
				else if(_pa[27] == 3)
				{
					return "Closing"; 
				}
				else if(_pa[27] == 4)
				{
					return "Opening"; 
				}
				else
				{
					return "Undefined";
				}
			 }
		},
		{
			name: 'width',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return _json['width'];
			}
		},
        {
			name: 'height',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return _json['height'];
			}
		},
		{
			name: 'expectedDoorState',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[28]; }
		},
		{
			name: 'expectedDoorStateEnum',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				if(_pa[28] == 0)
				{
					return "Close";
				}
				else if(_pa[28] == 1)
				{
					return "Open"; 
				}
				else
				{
					return "Undefined";
				}
			}
		},
		{
			name: 'defaultDoorStateBT',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[29] & 0x03; }
		},
		{
			name: 'defaultDoorStateOT',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[29] & 0x0C) >> 2; }
		},
		{
			name: 'defaulttemperatureinc',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return TemperatureC(_pa [35] * 256 + _pa[36]); }
		},
		{
			name: 'defaulttemperatureinf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureF(TemperatureC(_pa [35] * 256 + _pa [36]));
			}
		},
		{
            name: 'defaultDoorStateBlob',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['defaultValue']; }
        },
		{
			name: 'temperatureadc',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				return (_pa [33] * 256 + _pa [32]);
			}
		},
		{
			name: 'temperatureinc',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureC(_pa [33] * 256 + _pa [32]);
			}
		},
		{
			name: 'temperatureinf',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				return TemperatureF(TemperatureC(_pa [33] * 256 + _pa [32]));
			}
		}
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetDoorState',
            method: function(_arg) {
				if(_arg > 1)
				{
					_arg = 1;
				}
                return btoa([0x1f,0x41,_arg]);
            }
        },
		{
            name: 'SetDefaultValue',
			arguments: [{name:'stateBT',type:'byte'},{name:'stateOT',type:'byte'},{name:'temperature',type:'byte'}],
            method: function(_arg) {
				var split = _arg.split(',');
				var stateBt = Number(split[0]);
				var stateOt = Number(split[1]);
				var output = [0x1f, 0x42, 0xFF, 0xFF, 0xFF, 0xFF];
				
				if(stateBt > 2)
				{
					stateBt = 2;
				}
				if(stateOt > 2)
				{
					stateOt = 2;
				}
                output[2] = Number(stateBt);//0 closed or 1 open
				output[3] = Number(stateOt);//0 closed or 1 open
				var adcVal = adcValueOfTemperatureC(Number(split[2]));
                output[4] = adcVal >> 8;
				output[5] = adcVal & 0xFF;
					
                return btoa(output);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor", "defaultValue":2};