var TemperatureC = function (_adcvalue) {
    if (_adcvalue > 4094) {
        return 100;
    }
    var ADC = _adcvalue;
    var V = (ADC * 3.3 / 4095);
    var R = (10000 * 3.3) / V - 10000;
    var temperature = 1 / (1 / 298.15 + 1 / 3950 * Math.log(R / 10000.0))
    return temperature - 273.15;
}

var adcValueOfTemperatureC = function (tempC) {
    if (tempC > 99) {
        return 65535;
    }
    var R = 10000 * Math.exp(3950 / (tempC + 273.15) - 3950 / 298.15);
    var V = 10000 * 3.3 / (R + 10000);
    var ADC = V * 4095 / 3.3;

    return Math.round(ADC);
}

var TemperatureF = function (tempC) {
    var lTempF = tempC * 9 / 5.0 + 32;
    return lTempF;
}

this.PropertiesList = [
    {
        name: 'lastoverridetime',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['lastoverridetime'];
        }
        }, 
    {
        name: 'manualvalue',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            if (_json['manualvalue'] === undefined) {
                return 0;
            }
            return _json['manualvalue'];
        }
        }, 
    {
        name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },
    {
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },
    {
            name: 'name',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['name'];
        }
        },
    {
        name: 'nodeaddress',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['nodeaddress'];
        }
        },
    {
        name: 'priority',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['priority'];
        }
        }, 
    {
        name: 'namerelay0',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['namerelay0'];
        }
        }, 
    {
        name: 'namerelay1',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['namerelay1'];
        }
        }, 
    {
        name: 'namerelay2',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['namerelay2'];
        }
        },
    {
        name: 'driverinfos',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _typecsv;
        }
        },
    {
        name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },
    {
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },
    {
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        }, 
    {
        name: 'relayimage0',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['relayimage0'];
        }
        }, 
    {
        name: 'relayimage1',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['relayimage1'];
        }
        }, 
    {
        name: 'relayimage2',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['relayimage2'];
        }
        },
    {
        name: 'location0',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location0'];
        }
        },
    {
        name: 'location1',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location1'];
        }
        },
    {
        name: 'location2',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location2'];
        }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'encrypted',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return false;
        }
        },
    {
        name: 'relaisid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var out = "0x";
            var i = 0;
            for (i = 0; i < 4; i++) {
                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
            }
            return out;
        }
        },
    {
        name: 'chargingstate',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[26] > 0;
        }
        },
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'batteryad',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[8] * 256 + _pa[7];
        }
        },
    {
        name: 'battvoltage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2;
        }
        },
    {
        name: 'battpercentage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var BattAD;
            BattAD = (_pa[8] * 256 + _pa[7]) / 4095 * 3.3 * 2;

            if (BattAD >= 4.2)
                return 100;
            if (BattAD > 3.95)
                return ((BattAD - 3.95) * 80 + 80);
            if (BattAD > 3.8)
                return ((BattAD - 3.80) * 133.3333 + 60);
            if (BattAD > 3.65)
                return ((BattAD - 3.65) * 266.3333 + 20);
            if (BattAD > 3.3)
                return ((BattAD - 3.3) * 57.14 + 5);

            return 0;

        }
        },
    {
        name: 'relaystateisnotvalid',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[36] & 32) > 0;
        }
		},
        {
            name: 'relaystate0',
            type: 'bool',
            parser: function (_pa, _apa, _json, _typecsv) {
                return (_pa[36] & 4) > 0;
            }
            },
        {
            name: 'relaystate1',
            type: 'bool',
            parser: function (_pa, _apa, _json, _typecsv) {
                return (_pa[36] & 8) > 0;
            }
            },
        {
            name: 'relaystate2',
            type: 'bool',
            parser: function (_pa, _apa, _json, _typecsv) {
                return (_pa[36] & 16) > 0;
            }
            },
            {
                name: 'intrelay0',
                type: 'bool',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return (_pa[36] & 4) > 0?100:0;
                }
                },
            {
                name: 'intrelay1',
                type: 'bool',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return (_pa[36] & 8) > 0?100:0;
                }
                },
            {
                name: 'intrelay2',
                type: 'bool',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return (_pa[36] & 16) > 0?100:0;
                }
                },
    {
        name: 'ampermeter',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _typecsv.split(',')[4] > 0;
        }
        },
    {
        name: 'currentadc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[28] * 256 + _pa[27]);
        }
		},
    {
        name: 'current',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var adcVal = _pa[28] * 256 + _pa[27]
            if (adcVal > 4095) {
                //means negative value. If the there is noise one the ground of the ADC we might read negative value
                adcVal = 0;
            }

            var lCurrent = adcVal * 3.3 / 4095.0 * 1012 / 120.0 * 1.14 + 0.463;
            if (lCurrent < 0.6) {
                return 0;
            }
            return lCurrent;
        }
		},
    {
        name: 'temperatureadc',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[30] * 256 + _pa[29]);
        }
		},
    {
        name: 'temperatureinc',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureC(_pa[30] * 256 + _pa[29]);
        }
		},
    {
        name: 'temperatureinf',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureF(TemperatureC(_pa[30] * 256 + _pa[29]));
        }
		},
    {
        name: 'defaulttemprelay1inc',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureC((_pa[32] & 0xF0) * 16 + _pa[31]);
        }
		},
    {
        name: 'defaulttemprelay1inf',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureF(TemperatureC((_pa[32] & 0xF0) * 16 + _pa[31]));
        }
		},
    {
        name: 'defaultrelay1statebt',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[35] & 8) > 0;
        }
		},
    {
        name: 'defaultrelay1stateot',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[35] & 4) > 0;
        }
		},
    {
        name: 'defaulttemprelay2inc',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureC((_pa[32] & 0x0F) * 256 + _pa[33]);
        }
		},
    {
        name: 'defaulttemprelay2inf',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureF(TemperatureC((_pa[32] & 0x0F) * 256 + _pa[33]));
        }
		},
    {
        name: 'defaultrelay2statebt',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[35] & 2) > 0;
        }
		},
    {
        name: 'defaultrelay2stateot',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[35] & 1) > 0;
        }
		},
    {
        name: 'defaulttemprelay3inc',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureC((_pa[35] & 0xF0) * 16 + _pa[34]);
        }
		},
    {
        name: 'defaulttemprelay3inf',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return TemperatureF(TemperatureC((_pa[35] & 0xF0) * 16 + _pa[34]));
        }
		},
    {
        name: 'defaultrelay3statebt',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[36] & 128) > 0;
        }
		},
    {
        name: 'defaultrelay3stateot',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[36] & 64) > 0;
        }
		}, 
    {
        name: 'cfm/btu0',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['cfm/btu0'];
        }
        }, 
    {
        name: 'cfm/btu1',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['cfm/btu1'];
        }
        }, 
    {
        name: 'cfm/btu2',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['cfm/btu2'];
        }
		},
    {
        name: 'relay1TimeON',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[21] * 256 + _pa[20];
        }
        },
    {
        name: 'relay2TimeON',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[23] * 256 + _pa[22];
        }
        },
    {
        name: 'relay3TimeON',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[25] * 256 + _pa[24];
        }
        },
    {
        name: 'resetCounter',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[37];
        }
        }
    ];

this.FunctionList = [
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
    {
        name: 'SetRelay',
        arguments: [{
            name: 'relay0',
            type: 'byte'
        }, {
            name: 'relay1',
            type: 'byte'
        }, {
            name: 'relay2',
            type: 'byte'
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var i = 0;
            var output = [0x1f, 0x42, 2, 2, 2];
            for (i = 0; i < split.length; i++) {
                output[2 + i] = Number(split[i]);
            }
            return btoa(output);
        }
        },
    {
        name: 'SetDefaultValue',
        arguments: [{
            name: 'temp1',
            type: 'byte'
        }, {
            name: 'temp2',
            type: 'byte'
        }, {
            name: 'temp3',
            type: 'byte'
        }, {
            name: 'defRelayState',
            type: 'byte'
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var i = 0;
            var output = [0x1f, 0x43, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF];
            for (i = 0; i < split.length - 1; i++) {
                var adcVal = adcValueOfTemperatureC(Number(split[i]));
                output[2 + i * 2] = adcVal >> 8;
                output[2 + i * 2 + 1] = adcVal & 0xFF;
            }
            output[2 + (split.length - 1) * 2] = Number(split[split.length - 1]);
            return btoa(output);
        }
        },
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
            message.push(0);
            message.push(0);
            if (sendNode) {
                message[2] |= 4;
                message[3] |= 4;
                message[4] |= 4;
            }

            if (sendGroup) {
                message[2] |= 2;
                message[3] |= 2;
                message[4] |= 2;
            }

            if (sendForceRelayTime) {
                message[2] |= 8;
                message[3] |= 8;
                message[4] |= 8;
            }

            if (sendAmpSettings && txPower != 255 && useAmp != 2) {
                message[2] |= 1;
                message[3] |= 1;
                message[4] |= 1;
            }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        }
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
