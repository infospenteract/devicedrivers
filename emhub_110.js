  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'message',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['message']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },{
            name: 'username',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['username']; }
        },{
            name: 'numtel',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['numtel']; }
        },{
            name: 'email',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['email']; }
        },
      {
          name: 'noderedversion',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split(',')[0];
          }
        },
      {
          name: 'cpuusagetype',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split(',')[1];
          }
        },
      {
          name: 'load',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split(',')[2];
          }
        },
      {
          name: 'temperature',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split(',')[3];
          }
        },
      {
          name: 'cpuneed',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split(',')[4];
          }
        },{
            name: 'alertlanguage',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertlanguage']; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        }
    ];

  this.FunctionList = [
      {
          name: 'AskRefresh',
          method: function (_arg) {
              //Argument must be degree in celcius Must be call using Extended structure test
              return "";
          }
        }
    ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
