
    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'message',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['message']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },{
            name: 'username',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['username']; }
        },{
            name: 'numtel',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['numtel']; }
        },{
            name: 'email',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['email']; }
        },{
            name: 'alertlanguage',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertlanguage']; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        }
    ];
