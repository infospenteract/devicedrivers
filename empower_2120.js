
    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>1; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; 
            
			if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
				return ((BattAD - 3.3) * 57.14 + 5);

			return 0;
            
            }
        },
        {
            name: 'staterelay0',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[4] & 1)>0; }
        },
        {
            name: 'staterelay1',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[4] & 2)>0; }
        },
        {
            name: 'maxtensionadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[6] * 256 + _apa[5]; }
        },
        {
            name: 'avgtensionadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[8] * 256 + _apa[7]; }
        },
        {
            name: 'mintensionadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[10] * 256 + _apa[9]; }
        },
                {
            name: 'maxcurrentadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[14] * 256 + _apa[13]; }
        },
        {
            name: 'avgcurrentadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[16] * 256 + _apa[15]; }
        },
        {
            name: 'mincurrentadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[18] * 256 + _apa[17]; }
        },
        {
            name: 'maxtensionvolt',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[6] * 256 + _apa[5])* 119 / 1785; }
        },
        {
            name: 'avgtensionvolt',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[8] * 256 + _apa[7])* 119 / 1785; }
        },
        {
            name: 'mintensionvolt',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[10] * 256 + _apa[9])* 119 / 1785; }
        },
                {
            name: 'maxcurrentampere',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[14] * 256 + _apa[13])* 7.2 / 985; }
        },
        {
            name: 'avgcurrentampere',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[16] * 256 + _apa[15])* 7.2 / 985; }
        },
        {
            name: 'mincurrentampere',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[18] * 256 + _apa[17])* 7.2 / 985; }
        },
        {
            name: 'consumptioninwattshours',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[16] * 256 + _apa[15])* 7.2 / 985 *(_apa[8] * 256 + _apa[7])* 119 / 1785; }
        }
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'Start',
            method: function(_arg) {
                return btoa([0x02, 0x40, 0x40, 0x04, 0x01]);
            }
        },
        {
            name: 'Stop',
            method: function(_arg) {
                return btoa([0x02, 0x40, 0x40, 0x04, 0x00]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};