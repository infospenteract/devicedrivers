
    this.PropertiesList = [
        {
            name: 'lastoverridetime',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['lastoverridetime']; }
        },{
            name: 'manualvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if(_json['manualvalue'] === undefined){
                    return 0;
                }
                return _json['manualvalue']; }
        },{
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },{
            name: 'relayimage0',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['relayimage0']; }
        },
        {
            name: 'connectedobject',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['connectedobject']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>1; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2 * 1.0328; 
            
			if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
				return ((BattAD - 3.3) * 57.14 + 5);

			return 0;
            
            }
        },
        {
            name: 'tensionadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[12] * 256 + _apa[11]; }
        },
        {
            name: 'actualcurrentadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _apa[20] * 256 + _apa[19]; }
        },
        {
            name: 'tensionvolt',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[12] * 256 + _apa[11])/10; }
        },
        {
            name: 'outputvolt',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var peak = (_apa[12] * 256 + _apa[11])/10/(Math.sqrt(2)/2); 
                var delais = 65536-(_apa[31]+_apa[32]*256);
                var angle = delais/32768*3.141592;
                var t = Math.sqrt((2*3.141592-2*angle+Math.sin(2*angle))/(4*3.141592))*peak;
                
                if (isNaN(t))
                    {
                        return 0;
                    }
                
                return t;
            }
        },
        {
            name: 'currentampere',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var peak = ((_apa[20] * 256 + _apa[19])/4096*3.3*10)/(Math.sqrt(2)/2); 
                var delais = 65536-(_apa[31]+_apa[32]*256);
                var angle = delais/32768*3.141592;
                var t = Math.sqrt((2*3.141592-2*angle+Math.sin(2*angle))/(4*3.141592))*peak;
                if (isNaN(t))
                    {
                        return 0;
                    }
                
                return t;
            }
        },
        {
            name: 'consumptioninwattshours',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_apa[16] * 256 + _apa[15])* 7.2 / 985 *(_apa[8] * 256 + _apa[7])* 119 / 1785; }
        },
        {
            name: 'triacpercentage',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ //exa
               var t = 65535-(_apa[31]+_apa[32]*256);
               if (t<=0) { return 100;}
if (t<=1) { return 99;}
if (t<=4807) { return 98;}
if (t<=6099) { return 97;}
if (t<=7021) { return 96;}
if (t<=7767) { return 95;}
if (t<=8406) { return 94;}
if (t<=8972) { return 93;}
if (t<=9484) { return 92;}
if (t<=9954) { return 91;}
if (t<=10392) { return 90;}
if (t<=10802) { return 89;}
if (t<=11190) { return 88;}
if (t<=11559) { return 87;}
if (t<=11911) { return 86;}
if (t<=12249) { return 85;}
if (t<=12574) { return 84;}
if (t<=12888) { return 83;}
if (t<=13192) { return 82;}
if (t<=13486) { return 81;}
if (t<=13773) { return 80;}
if (t<=14052) { return 79;}
if (t<=14325) { return 78;}
if (t<=14591) { return 77;}
if (t<=14851) { return 76;}
if (t<=15107) { return 75;}
if (t<=15357) { return 74;}
if (t<=15603) { return 73;}
if (t<=15845) { return 72;}
if (t<=16083) { return 71;}
if (t<=16317) { return 70;}
if (t<=16548) { return 69;}
if (t<=16776) { return 68;}
if (t<=17001) { return 67;}
if (t<=17224) { return 66;}
if (t<=17443) { return 65;}
if (t<=17661) { return 64;}
if (t<=17876) { return 63;}
if (t<=18089) { return 62;}
if (t<=18300) { return 61;}
if (t<=18509) { return 60;}
if (t<=18717) { return 59;}
if (t<=18923) { return 58;}
if (t<=19127) { return 57;}
if (t<=19330) { return 56;}
if (t<=19532) { return 55;}
if (t<=19733) { return 54;}
if (t<=19933) { return 53;}
if (t<=20131) { return 52;}
if (t<=20329) { return 51;}
if (t<=20526) { return 50;}
if (t<=20722) { return 49;}
if (t<=20917) { return 48;}
if (t<=21112) { return 47;}
if (t<=21307) { return 46;}
if (t<=21501) { return 45;}
if (t<=21694) { return 44;}
if (t<=21887) { return 43;}
if (t<=22080) { return 42;}
if (t<=22273) { return 41;}
if (t<=22466) { return 40;}
if (t<=22659) { return 39;}
if (t<=22852) { return 38;}
if (t<=23045) { return 37;}
if (t<=23238) { return 36;}
if (t<=23432) { return 35;}
if (t<=23626) { return 34;}
if (t<=23820) { return 33;}
if (t<=24015) { return 32;}
if (t<=24211) { return 31;}
if (t<=24407) { return 30;}
if (t<=24604) { return 29;}
if (t<=24802) { return 28;}
if (t<=25002) { return 27;}
if (t<=25202) { return 26;}
if (t<=25404) { return 25;}
if (t<=25607) { return 24;}
if (t<=25811) { return 23;}
if (t<=26018) { return 22;}
if (t<=26226) { return 21;}
if (t<=26436) { return 20;}
if (t<=26649) { return 19;}
if (t<=26864) { return 18;}
if (t<=27082) { return 17;}
if (t<=27303) { return 16;}
if (t<=27527) { return 15;}
if (t<=27755) { return 14;}
if (t<=27987) { return 13;}
if (t<=28224) { return 12;}
if (t<=28466) { return 11;}
if (t<=28713) { return 10;}
if (t<=28968) { return 9;}
if (t<=29229) { return 8;}
if (t<=29500) { return 7;}
if (t<=29782) { return 6;}
if (t<=30076) { return 5;}
if (t<=30387) { return 4;}
if (t<=30718) { return 3;}
if (t<=31077) { return 2;}
if (t<=31479) { return 1;}
return 0;
            }
        },
        {
            name: 'triacdelay',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return 65536-(_apa[31]+_apa[32]*256); } //0.25 us
        },
        {
            name: 'phi',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if (_apa[20] * 256 + _apa[19]>100)
                    {
                        var val = _apa[21]+_apa[22]*256;
                        if (val>32768)
                            val = 65536-val;
                        return (val*0.000085/0.0166*360);
                    }
                return 0;
                
            } //83.25 us
        },
        {
            name: 'connectedto',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                return _json['connectedto']; 
            } 
        },{
            name: 'cfm/btu',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['cfm/btu']; }
        },{
            name: 'minpow',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['minpow']; }
        },{
            name: 'recirculation',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['recirculation']; }
        },{
            name: 'minimalventpriority',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['minimalventpriority']; }
        }
        
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetPercentage',
            method: function(_arg) {
                var t = Number(_arg);
                var value = 0;
                
                if (t>=100) { value=0;}
else if (t>=99) { value=1;}
else if (t>=98) { value=4807;}
else if (t>=97) { value=6099;}
else if (t>=96) { value=7021;}
else if (t>=95) { value=7767;}
else if (t>=94) { value=8406;}
else if (t>=93) { value=8972;}
else if (t>=92) { value=9484;}
else if (t>=91) { value=9954;}
else if (t>=90) { value=10392;}
else if (t>=89) { value=10802;}
else if (t>=88) { value=11190;}
else if (t>=87) { value=11559;}
else if (t>=86) { value=11911;}
else if (t>=85) { value=12249;}
else if (t>=84) { value=12574;}
else if (t>=83) { value=12888;}
else if (t>=82) { value=13192;}
else if (t>=81) { value=13486;}
else if (t>=80) { value=13773;}
else if (t>=79) { value=14052;}
else if (t>=78) { value=14325;}
else if (t>=77) { value=14591;}
else if (t>=76) { value=14851;}
else if (t>=75) { value=15107;}
else if (t>=74) { value=15357;}
else if (t>=73) { value=15603;}
else if (t>=72) { value=15845;}
else if (t>=71) { value=16083;}
else if (t>=70) { value=16317;}
else if (t>=69) { value=16548;}
else if (t>=68) { value=16776;}
else if (t>=67) { value=17001;}
else if (t>=66) { value=17224;}
else if (t>=65) { value=17443;}
else if (t>=64) { value=17661;}
else if (t>=63) { value=17876;}
else if (t>=62) { value=18089;}
else if (t>=61) { value=18300;}
else if (t>=60) { value=18509;}
else if (t>=59) { value=18717;}
else if (t>=58) { value=18923;}
else if (t>=57) { value=19127;}
else if (t>=56) { value=19330;}
else if (t>=55) { value=19532;}
else if (t>=54) { value=19733;}
else if (t>=53) { value=19933;}
else if (t>=52) { value=20131;}
else if (t>=51) { value=20329;}
else if (t>=50) { value=20526;}
else if (t>=49) { value=20722;}
else if (t>=48) { value=20917;}
else if (t>=47) { value=21112;}
else if (t>=46) { value=21307;}
else if (t>=45) { value=21501;}
else if (t>=44) { value=21694;}
else if (t>=43) { value=21887;}
else if (t>=42) { value=22080;}
else if (t>=41) { value=22273;}
else if (t>=40) { value=22466;}
else if (t>=39) { value=22659;}
else if (t>=38) { value=22852;}
else if (t>=37) { value=23045;}
else if (t>=36) { value=23238;}
else if (t>=35) { value=23432;}
else if (t>=34) { value=23626;}
else if (t>=33) { value=23820;}
else if (t>=32) { value=24015;}
else if (t>=31) { value=24211;}
else if (t>=30) { value=24407;}
else if (t>=29) { value=24604;}
else if (t>=28) { value=24802;}
else if (t>=27) { value=25002;}
else if (t>=26) { value=25202;}
else if (t>=25) { value=25404;}
else if (t>=24) { value=25607;}
else if (t>=23) { value=25811;}
else if (t>=22) { value=26018;}
else if (t>=21) { value=26226;}
else if (t>=20) { value=26436;}
else if (t>=19) { value=26649;}
else if (t>=18) { value=26864;}
else if (t>=17) { value=27082;}
else if (t>=16) { value=27303;}
else if (t>=15) { value=27527;}
else if (t>=14) { value=27755;}
else if (t>=13) { value=27987;}
else if (t>=12) { value=28224;}
else if (t>=11) { value=28466;}
else if (t>=10) { value=28713;}
else if (t>=9) { value=28968;}
else if (t>=8) { value=29229;}
else if (t>=7) { value=29500;}
else if (t>=6) { value=29782;}
else if (t>=5) { value=30076;}
else if (t>=4) { value=30387;}
else if (t>=3) { value=30718;}
else if (t>=2) { value=31077;}
else if (t>=1) { value=31479;}
else if (t>=0) { value=32767;}
                
                value = 65535-value;
                return btoa([0x02, 0x40, 0x40, 0x06, value%256, Math.floor(value/256)]);
            }
        },
        {
            name: 'SetTriacDelay',
            method: function(_arg) {
                var value = Number(_arg);
                value = 65535-value;
                return btoa([0x02, 0x40, 0x40, 0x06, value%256, Math.floor(value/256)]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor", "connectedto":"generic"};