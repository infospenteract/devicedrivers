//*
// * voici ce qui doit être dans le driver et le blob pour les version 210 et +
// * le capteur est alimenté à 3.3V et on a un gain de 128 sur l'ampli et l'ampli est sur 24 bits donc 16 777 215 bits
// * si par exemple on a un capteur de 2mV/V sur un maximum de 100Kg et qu'il y a 1 capteur
// * pour avoir la valeur de la calibrationValue on doit faire 
// * 128 x 16 777 215 x (2x10-3)
// * --------------------------- = 42 950 bit/Kg
// * 100kg x 1 capteurs
//* 
// * donc:
// * 2 147 483 x A(en mv)
// * -------------------
// *  B (en kg) x C(en nombre de capteurs)
// * 
// * 
// * 
// * 
// * 
// */


var ThresholdGorKG = function(_json,ADCvalue)
{
	try {
		var A =  Number(_json["options"].split(',')[0]);//mv/V
		var B = Number(_json["options"].split(',')[1]);//valeur max en Kg
		var C = Number(_json["options"].split(',')[2]);//nombre de capteurs
		var	Dtar = Number(_json["options"].split(',')[4]);//poid à vide du silo
		
	}
	catch(err){
		var A = 2;
		var B = 100;//100Kg
		var C = 1;//1 capteur
		var	Dtar = 0;
	}
	
	
	var pente = 2147483 * A /(B * C);//retoune une valeur en bit/kg
	if(C>1)
	{
		var tot = Math.round(ADCvalue/pente) ;//affiche en Kg
		if(tot < 0)
			tot = 0;
		return tot;
	}
	if(C===1)
	{
		var tot = Math.round((ADCvalue/pente)* 1000);//affiche en G
		if(tot < 0)
			tot = 0;
		return tot;
	}
	
}								 
var WeightGorKG = function(_json,ADCvalue)
{
	try {
		var A =  Number(_json["options"].split(',')[0]);//mv/V
		var B = Number(_json["options"].split(',')[1]);//valeur max en Kg
		var C = Number(_json["options"].split(',')[2]);//nombre de capteurs
		var	Dtar = Number(_json["options"].split(',')[4]);//poid à vide du silo
		
	}
	catch(err){
		var A = 2;
		var B = 100;//100Kg
		var C = 1;//1 capteur
		var	Dtar = 0;
	}
	
	
	var pente = 2147483 * A /(B * C);//retoune une valeur en bit/kg
	if(C>1)
	{
		var tot = Math.round(ADCvalue/pente) - Dtar;//affiche en Kg
		if(tot < 0)
			tot = 0;
		return tot;
	}
	if(C===1)
	{
		var tot = Math.round((ADCvalue/pente)* 1000);//affiche en G
		if(tot < 0)
			tot = 0;
		return tot;
	}
	
}
var WeightinBits = function(_json,WeightinGorKg)
{
	try {
		var A =  Number(_json["options"].split(',')[0]);//mv/V
		var B = Number(_json["options"].split(',')[1]);//valeur max en Kg
		var C = Number(_json["options"].split(',')[2]);//nombre de capteurs
		
		
	}
	catch(err){
		var A = 2;
		var B = 100;//100Kg
		var C = 1;//1 capteur
		
	}
	
	var pente = 2147483 * A /(B * C);//retoune une valeur en bit/kg
	
	if(C>1)//le poids entre est en kg
	{
		var val = pente * WeightinGorKg;
	}
	if(C===1)//le poids entré est en g
	{
		var val = pente * WeightinGorKg / 1000;
	}
	return val;
	
}

this.PropertiesList = [
    {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'managerid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['managerid']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'encrypted',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return false;
        }
        },
    {
        name: 'relaisid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var out = "0x";
            var i = 0;
            for (i = 0; i < 4; i++) {
                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
            }
            return out;
        }
        },
    
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'calib_done',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
             var caldone = _pa[7];
			 if(caldone == 0)
			 {
				 return "default calibration";
			 }
			 else if(caldone == 1)
			 {
				 return "only tar done";
			 }
			 else if(caldone == 2)
			 {
				 return "only span done";
			 }
			 else if(caldone == 3)
			 {
				 return "fully calibrated";
			 }
			 else
			 {	 
				return "unknown";
			 }
			}
			else
			{
				return "NA";
			}
        }
        },

    {
        name: 'units',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				try{
					var C = Number(_json["options"].split(',')[2]);//nombre de capteurs
				}
				catch(err){
					var C = 1;//1 capteur
					
				}
				if(C===1)
				{
					return "G";
				}
				if(C>1)
				{
					return "Kg";
				}
			}
			else
			{
				return "NA";
			}
        }
        },
	{
        name: 'mVbyV',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				try{
					var A = Number(_json["options"].split(',')[0]);//mv/v
				}
				catch(err){
					var A = 2;//2 mv/v
					
				}
				 
				return A;
			}
			else
			{
				return 0;
			}
        }
        },
	{
        name: 'Sensor_capacity_in_Kg',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				try{
					var B = Number(_json["options"].split(',')[1]);//capacité max du capteur en Kg
				}
				catch(err){
					var B = 100;//capacité en kg
					
				}
				 
				return B;
			}
			else
			{
				return 0;
			}
        }
        },
	{
        name: 'Sensors_used',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				try{
					var C = Number(_json["options"].split(',')[2]);//nombre de capteurs
				}
				catch(err){
					var C = 1;//1 capteur
					
				}
				 
				return C;
			}
			else
			{
				return 0;
			}
			
        }
        },
	{
        name: 'empty_weight',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				try{
					var Dtar = Number(_json["options"].split(',')[4]);//poid a vide
					}
				catch(err){
					var Dtar = 0;//
				
				}
             
				return Dtar;
			}
			else
			{
				return 0;
			}
        }
        },
	{
        name: 'bin_selected',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
             var binsel = _pa[35];
			 if(binsel == 0)
			 {
				 return "emSiloScale";
			 }
			 else
			 {
				 return String.fromCharCode(_pa[35]);
			 }
			 
        }
        },	
    {
        name: 'weight0',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[17] * 16777216 + _pa[16] * 65536 +_pa[15] * 256 + _pa[14]);
				return WeightGorKG(_json,WeightInBit);
			}
			else
			{
				var WeightDisplayed = (_pa[17] * 16777216 + _pa[16] * 65536 +_pa[15] * 256 + _pa[14]);
				if (WeightDisplayed>2147483647)
				WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs;
			}
            
        }
        },
	{
        name: 'weight1',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[21] * 16777216 + _pa[20] * 65536 +_pa[19] * 256 + _pa[18]);
			
				return WeightGorKG(_json,WeightInBit);
			}
			else
			{
				var WeightDisplayed = (_pa[21] * 16777216 + _pa[20] * 65536 +_pa[19] * 256 + _pa[18]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs;
			}
            
        }
        },
	{
        name: 'weight2',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[25] * 16777216 + _pa[24] * 65536 +_pa[23] * 256 + _pa[22]);
			
				return WeightGorKG(_json,WeightInBit);
			}
			else
			{
				var WeightDisplayed = (_pa[25] * 16777216 + _pa[24] * 65536 +_pa[23] * 256 + _pa[22]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs;
			}
            
        }
        },
	{
        name: 'weight3',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[29] * 16777216 + _pa[28] * 65536 +_pa[27] * 256 + _pa[26]);
			
				return WeightGorKG(_json,WeightInBit);
			}
			else
			{
				var WeightDisplayed = (_pa[29] * 16777216 + _pa[28] * 65536 +_pa[27] * 256 + _pa[26]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs;
			}
            
        }
        },
	
	{
        name: 'weight10',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				
				var WeightInBit = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
			
				return WeightGorKG(_json,WeightInBit);
			}
			else
			{
				var WeightDisplayed = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs
			}
            
        }
        },
	{
        name: 'weightADC',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
			
				return WeightInBit;
			}
			else
			{
				var WeightDisplayed = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs ... faut mettre les option pour que la pente = 1 donc 1,2147483,1,seelow,0
			}
            
        }
        },	
	{
        name: 'weightinkg',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
			
				return WeightGorKG(_json,WeightInBit);
			}
			else
			{
				var WeightDisplayed = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2;//le poid est tjrs recu en lbs
			}
            
        }
        },	
	{
        name: 'weightintons',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
			
				return WeightGorKG(_json,WeightInBit)/1000;
			}
			else
			{
				var WeightDisplayed = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2 / 1000;//le poid est tjrs recu en lbs
			}
            
        }
        },	
	{
        name: 'weightintonsh',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var WeightInBit = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
			
				return WeightGorKG(_json,WeightInBit)/1000 * 1.1;
			}
			else
			{
				var WeightDisplayed = (_pa[33] * 16777216 + _pa[32] * 65536 +_pa[31] * 256 + _pa[30]);
				if (WeightDisplayed>2147483647)
					WeightDisplayed = -(4294967296-WeightDisplayed);
				return WeightDisplayed / 2.2 / 1000 * 1.1;//le poid est tjrs recu en lbs
			}
            
        }
        },	
	{
        name: 'thresholdWeight',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
			if(_pa[35] == 0)
			{
				var w_inbit =(_pa[37] * 256 + _pa[36]);
				return ThresholdGorKG(_json,w_inbit);
			}
			else
			{
				return 0;
			}
			
        }
        },
	{
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[34];
            }
        }	
    
    ];

this.FunctionList = [
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        },
	{
        name: 'SetBin',
		arguments: [{name:'setBinSelected',type:'string'}],
        method: function (_arg) 
		{
			
			
			var output = [0x1f, 0x44, 0x42, 0x49, 0x4E, 0x00];
			output[5] =_arg.charCodeAt(0);
			
			
			return btoa(output);
        }
        },
    {
        name: 'SetTar',
		arguments: [{name:'setTarValueToZero',type:'bool'},{name:'resetTar',type:'bool'}],
        method: function (_arg) 
		{
			
			var split = _arg.split(',');
			var output = [0x1f, 0x41, 0x54, 0x41, 0x52, 0x00, 0x00];
			output[5] = Number(split[0]);
			output[6] = Number(split[1]);
			
			return btoa(output);
        }
        },
    {
        name: 'SetCalibration',
        arguments: [{
            name: 'calibrationWeight',
            type: 'int',
            default: 0
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var calibrationValue = Number(split[0]);
			var valueMsb = 0;
			var valueLsb = 1;
				
			if(calibrationValue > 0)
			{
                valueMsb = Number(calibrationValue) / 256;
				valueLsb = Number(calibrationValue) % 256;
			}
				
            var message = []
            message.push(0x1F);
            message.push(0x42);
            message.push(0x43);
            message.push(0x41);
            message.push(0x4C);
            message.push(valueLsb);
            message.push(valueMsb);
            message.push(0x00);

            return btoa(message);
        }
        },
		{
        name: 'SetThreshold',
        arguments: [{
            name: 'thresholdWeight',
            type: 'int',
            default: 0
        }],
        method: function (_arg) {
            var split = _arg.split(',');
			
			
			var A = Number(split[0]);//mv/V
			var B = Number(split[1]);//valeur max en Kg
			var C = Number(split[2]);//nombre de capteurs
			var D = Number(split[3]);//threshold en KG
					
				
				
			var pente = 2147483 * A /(B * C);//retoune une valeur en bit/kg
				
				
			
			if(C>1)
			{
				var calibrationValue = pente * D ;
			}
			if(C===1)
			{
				var calibrationValue = pente * D / 1000;
			}
				
			var valueMsb = 0;
			var valueLsb = 1;
				
			if(calibrationValue > 0)
			{
                valueMsb = Number(calibrationValue) / 256;
				valueLsb = Number(calibrationValue) % 256;
			}
				
            var message = []
            message.push(0x1F);
            message.push(0x43);
            message.push(0x54);
            message.push(0x48);
            message.push(0x52);
            message.push(valueLsb);
            message.push(valueMsb);
            message.push(0x00);

            return btoa(message);
        }
        }
		// {
        // name: 'SetThreshold',
        // arguments: [{
            // name: 'thresholdWeight',
            // type: 'int',
            // default: 0
        // }],
        // method: function (_arg) {
            // var split = _arg.split(',');
            // var calibrationValue = Number(split[0]);//la valeur dans le emscale est en G...un jour on uniformisera avec le emSiloScale
			// var valueMsb = 0;
			// var valueLsb = 1;
				
			// if(calibrationValue > 0)
			// {
                // valueMsb = Number(calibrationValue) / 256;
				// valueLsb = Number(calibrationValue) % 256;
			// }
				
            // var message = []
            // message.push(0x1F);
            // message.push(0x43);
            // message.push(0x54);
            // message.push(0x48);
            // message.push(0x52);
            // message.push(valueLsb);
            // message.push(valueMsb);
            // message.push(0x00);

            // return btoa(message);
        // }
        // }
	
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
