    



    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[2]); }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return 1; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1] * 256 + _pa[0]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[1] * 256 + _pa[0]) / 100; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[1] * 256 + _pa[0]) / 100; 
           
			if (BattAD >= 1.52)
				return 100;
			if (BattAD > 1.42)
				return ((BattAD - 1.42) * 200 + 80);
			if (BattAD > 1.31)
				return ((BattAD - 1.31) * 181.81 + 60);
			if (BattAD > 1.21)
				return ((BattAD - 1.21) * 400 + 20);
			if (BattAD > 1)
				return ((BattAD - 1) * 71.43 + 5);

			return 0;
            
            }
        }
		
		
        
		
    ];

    this.FunctionList = [
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor", "relativepressure":"101.38"};