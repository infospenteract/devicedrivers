 var adcToDegree = function(_adcvalue)
{
	if(_adcvalue >= 0 && _adcvalue <= 301)
	{
		return 112.5;
	}
	else if(_adcvalue > 301 && _adcvalue <= 356)
	{
		return 67.5;
	}
	else if(_adcvalue > 356 && _adcvalue <= 441)
	{
		return 90;
	}
	else if(_adcvalue > 441 && _adcvalue <= 624)
	{
		return 157.5;
	}
	else if(_adcvalue > 624 && _adcvalue <= 860)
	{
		return 135;
	}
	else if(_adcvalue > 860 && _adcvalue <= 1064)
	{
		return 202.5;
	}
	else if(_adcvalue > 1064 && _adcvalue <= 1388)
	{
		return 180;
	}
	else if(_adcvalue > 1388 && _adcvalue <= 1738)
	{
		return 22.5;
	}
	else if(_adcvalue > 1738 && _adcvalue <= 2125)
	{
		return 45;
	}
	else if(_adcvalue > 2125 && _adcvalue <= 2463)
	{
		return 247.5;
	}
	else if(_adcvalue > 2463 && _adcvalue <= 2668)
	{
		return 225;
	}
	else if(_adcvalue > 2668 && _adcvalue <= 2979)
	{
		return 337.5;
	}
	else if(_adcvalue > 2979 && _adcvalue <= 3227)
	{
		return 0;
	}
	else if(_adcvalue > 3227 && _adcvalue <= 3427)
	{
		return 292.5;
	}
	else if(_adcvalue > 3427 && _adcvalue <= 3661)
	{
		return 315;
	}
	else if(_adcvalue > 3661 && _adcvalue <= 4095)
	{
		return 270;
	}
	else
	{
		return 404;
	}
}

var adcToCardinal = function(_adcvalue)
{
	if(_adcvalue >= 0 && _adcvalue <= 301)
	{
		return "ESE";
	}
	else if(_adcvalue > 301 && _adcvalue <= 356)
	{
		return "ENE";
	}
	else if(_adcvalue > 356 && _adcvalue <= 441)
	{
		return "E";
	}
	else if(_adcvalue > 441 && _adcvalue <= 624)
	{
		return "SES";
	}
	else if(_adcvalue > 624 && _adcvalue <= 860)
	{
		return "SE";
	}
	else if(_adcvalue > 860 && _adcvalue <= 1064)
	{
		return "SWS";
	}
	else if(_adcvalue > 1064 && _adcvalue <= 1388)
	{
		return "S";
	}
	else if(_adcvalue > 1388 && _adcvalue <= 1738)
	{
		return "NNE";
	}
	else if(_adcvalue > 1738 && _adcvalue <= 2125)
	{
		return "NE";
	}
	else if(_adcvalue > 2125 && _adcvalue <= 2463)
	{
		return "WSW";
	}
	else if(_adcvalue > 2463 && _adcvalue <= 2668)
	{
		return "SW";
	}
	else if(_adcvalue > 2668 && _adcvalue <= 2979)
	{
		return "NWN";
	}
	else if(_adcvalue > 2979 && _adcvalue <= 3227)
	{
		return "N";
	}
	else if(_adcvalue > 3227 && _adcvalue <= 3427)
	{
		return "WNW";
	}
	else if(_adcvalue > 3427 && _adcvalue <= 3661)
	{
		return "NW";
	}
	else if(_adcvalue > 3661 && _adcvalue <= 4095)
	{
		return "W";
	}
	else
	{
		return "error";
	}
}

var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        {
            name: 'chargingstate',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[26]>0; }
        },
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		{
            name: 'forcerelayfactor',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[20]; }
        },
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
        {
            name: 'batteryad',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[8] * 256 + _pa[7]; }
        },
        {
            name: 'battvoltage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; }
        },
        {
            name: 'battpercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){  
            var BattAD; 
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2; 
            
			if (BattAD >= 4.2)
				return 100;
			if (BattAD > 3.95)
				return ((BattAD - 3.95) * 80 + 80);
			if (BattAD > 3.8)
				return ((BattAD - 3.80) * 133.3333 + 60);
			if (BattAD > 3.65)
				return ((BattAD - 3.65) * 266.3333 + 20);
			if (BattAD > 3.3)
				return ((BattAD - 3.3) * 57.14 + 5);

			return 0;
            
            }
        },
		{
			name: 'isTemperatureValid',
            type: 'string',
			parser: function(_pa, _apa, _json, _typecsv) { 
                var temp = _pa[28] *256 + _pa[27];
				var humidity = _pa[30] *256 + _pa[29];
                if (temp == 32767)
				{
					return "Error code 2";
				}
				else if (temp == 0 && humidity == 0)
				{
					return "Error code 1"
				}
				else
				{
					return "Valid";
				}
            }
		},
        {
            name: 'temperatureincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[28] *256 + _pa[27];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10; 
            }
        },
		{
			name: 'currentempinfahrenheit',
			type: 'double',
			parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[28] *256 + _pa[27];
                if (value>32767)
                    value = -(65536-value);
                
                return TemperatureF(value/10);
          }
        },
		{
			name: 'isHumidityValid',
            type: 'string',
			parser: function(_pa, _apa, _json, _typecsv) { 
				var temp = _pa[28] *256 + _pa[27];
                var humidity = _pa[30] *256 + _pa[29];
                if (humidity == 65535)
				{
					return "Error code 2";
				}
				else if (temp == 0 && humidity == 0)
				{
					return "Error code 1"
				}
				else
				{
					return "Valid";
				}
                return value/10; 
            }
		},
        {
            name: 'currenthumiditypercentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30] *256+ _pa[29]); }
        },
		
        {
            name: 'windspeedtick',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[34] *256+ _pa[33]); }
        },
		{
            name: 'currentwindspeed',//kmh
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[34] *256+ _pa[33])/60*1.9; }
        },
		{
            name: 'windspeedmph',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[34] *256+ _pa[33])/60*1.181; }
        },
		{
            name: 'maxwindspeedkmh',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
				var timerTick = _pa[23] *256+ _pa[22]
				if(timerTick == 65535)
				{
					timerTick = 0;
				}
				else
				{
					timerTick = 32768 / 8 / timerTick * 1.9
				}
				return timerTick;
			}
        },
        {
            name: 'raincount',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[36] *256+ _pa[35]); }
        },
		{
            name: 'rainmm',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[36] *256+ _pa[35])*1.75; }
        },
        {
            name: 'winddirectionadc',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[25] *256+ _pa[24]); }
        },
        {
            name: 'currentwinddirection',//degree
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return adcToDegree(_pa[25] *256+ _pa[24]); }
        },
        {
            name: 'winddirection',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return adcToCardinal(_pa[25] *256+ _pa[24]); }
        },
        {
            name: 'resetCounter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[37];
            }
        }
        
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
				message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};