
var TemperatureC = function(_adcvalue)
{
	if(_adcvalue > 1022)
	{
		return 100;
	}
	var ADC = _adcvalue;
	var V = (ADC * 3.3 / 1023);
	var R = (10000 * 3.3) / V - 10000;
	var temperature = 1/(1/298.15 + 1/3950 * Math.log(R / 10000.0));
	temperature = (temperature - 273.15)*10;
	temperature = Math.round(temperature);
	return temperature/10;
}

var adcValueOfTemperatureC = function(tempC)
{
	if(tempC > 99)
	{
		return 65535;
	}
	var R = 10000 * Math.exp(3950/(tempC + 273.15) - 3950/298.15);
	var V = 10000 * 3.3 / (R + 10000);
	var ADC = V * 1023 / 3.3;
	
	return Math.round(ADC);	
}
var CurrentInAmp = function(currentRead, speed)
{
	var valCur = currentRead;// - 521;//521 bit est la valeur lue a 0 amperes
	if(speed == 100)
	{
		valCur = 36.5/1000 * valCur -0.25;//calculs trouvé par tests
	}
	else
	{
		valCur = (0.000115)*valCur*valCur + 0.0149*valCur - 0.0167;//calculs trouvé par tests
	}
	if(valCur < 0)
		valCur = 0;
	return valCur;
}

var TemperatureF = function(tempC)
{
	var lTempF = tempC * 9 / 5.0 + 32; 
	return lTempF;
}

    this.PropertiesList = [
        {
            name: 'lastoverridetime',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['lastoverridetime']; }
        },{
            name: 'manualvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if(_json['manualvalue'] === undefined){
                    return 0;
                }
                return _json['manualvalue']; }
        },{
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'encrypted',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return false; }
        },
        {
            name: 'relaisid',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                var out = "0x";
                var i=0;
                for (i=0;i<4;i++)
                    {
                        out += ("0" + _pa[2+i].toString(16)).slice(-2);
                    }
                return out; 
            }
        },
        
        {
            name: 'txrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[10]); }
        },
        {
            name: 'rxrssi',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ return (-_pa[9]); }
        },
        {
            name: 'worstrssi',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[11]; }
        },
        {
            name: 'nodeadress',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[12]; }
        },
        {
            name: 'meshgroup',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[6]; }
        },
		
        {
            name: 'level',
            type: 'byte',
            parser: function(_pa, _apa, _json, _typecsv){ return _pa[1]; }
        },
		{
            name: 'soft_version',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[14]);
            }
        },
		{
            name: 'soft_subversion',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_apa[29]);
            }
        },
		{
            name: 'pcb_version',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[7]);
            }
        },
		{
            name: 'mode_operation',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
                var valMode = (_pa[35]);
				var valFunc = (_apa[30])&2;
				let funcText = "";
				const func = [" + DEFROST"," + OTHER"];
				if(valFunc == 2)
				{
					funcText = func[0];
				}
				const mode = ["00:SHUTDOWN","01:HRV","02:FLEXIBLE 1","03:FLEXIBLE 2","04:BYPASS","05:DEGIVRAGE PREVENTIF","06:DEGIVRAGE LONG","07:DEGIVRAGE SECURITAIRE","08:POSITION FAN 1","09:POSITION FAN 2","10:INITIALIZE","11:FILTRATION","12:NETTOYAGE","13:VITESSES MANUELLES"];
				let modeText = "";
				
				modeText += mode[valMode] + funcText;
				return modeText;
				
//				if(valMode == 0)
//					return "SHUTDOWN";
//				if(valMode == 1)
//					return "HRV";
//				if(valMode == 2)
//					return "FLEXIBLE 1";
//				if(valMode == 3)
//					return "FLEXIBLE 2";
//				if(valMode == 4)
//					return "BYPASS";
//				if(valMode == 5)
//					return "DEGIVRAGE PREVENTIF";
//				if(valMode == 6)
//					return "DEGIVRAGE LONG";
//				if(valMode == 7)
//					return "DEGIVRAGE SECURITAIRE";
//				if(valMode == 8)
//					return "POSITION FAN 1";
//				if(valMode == 9)
//					return "POSITION FAN 2";
//				if(valMode == 10)
//					return "INITIALIZE";
//				if(valMode == 11)
//					return "FILTRATION";
//				if(valMode == 12)
//					return "NETTOYAGE";
//				if(valMode == 13)
//					return "VITESSES MANUELLES";
//			    return "UNKNOWN";
				
            }
        },
		{
            name: 'fan1_speed',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[32]);
            }
        },
		{
			name: 'current_fan1',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {				
				
			var xa = _pa[17];
			var xb = _pa[18];
			var val1 = (xa>>>6)*256;
			var val2 = (xa<<2)&255;
			var val3 = xb>>>6;
			var adcVal= val1+val2+val3;
			if ((_pa[7] == 1)&&(_pa[14]<8))//version du pcb et du soft .. pcb 1 pas le meme chip de courant
			{
				adcVal -= 521;//on lit 521 quand ya pas de courant
				if(adcVal<0)
				{
					adcVal = 0;
				}
				var lCurrent = (20 * adcVal / 503)*100;
				lCurrent = Math.round(lCurrent);
				return lCurrent/100;
			}
			else if(_pa[14]<8)//version du soft .. pcb 2 nouveau chip de courant mais soft version 7 et moins, on envoi en bit et non en amp
			{
				adcVal -= 131;//on lit 131 quand ya pas de courant
				if(adcVal<0)
				{
					adcVal = 0;
				}
				var lCurrent = (5.78 * adcVal / 640)*100;//valeurs calculé selon les mesures
				lCurrent = Math.round(lCurrent);
				if(lCurrent < 20)//pour cacher l'interférence
					lCurrent = 0;
				return lCurrent/100;
			}
			else // version soft 8 peu importe la version du board
			{
				return adcVal/100;
			}
			
			
			
			
			
			
        }
		},
		{
            name: 'fan1_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16])&3;
				if(valAct == 0)
					return "POSITION 1";
				if(valAct == 1)
					return "POSITION 4";
				if(valAct == 2)
					return "POSITION 2";
				if(valAct == 3)
					return "POSITION 3";
				
                
            }
        },
		{
            name: 'fan1_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>7)&1;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "MOVING";
				
                
            }
        },
		{
            name: 'fan2_speed',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[33]);
            }
        },
		{
			name: 'current_fan2',
			type: 'int',
			parser: function (_pa, _apa, _json, _typecsv) {
				
		   var xa = _pa[18];
		   var xb = _pa[19];
		   var val1 = ((xa>>>4)&3)*256;
		   var val2 = (xa<<4)&255; 
		   var val3 = (xb>>>4);
		   var adcVal= val1+val2+val3;
		   if ((_pa[7] == 1)&&(_pa[14]<8))//version du pcb et du soft .. pcb 1 pas le meme chip de courant
			{
				adcVal -= 521;//on lit 521 quand ya pas de courant
				if(adcVal<0)
				{
					adcVal = 0;
				}
				var lCurrent = (20 * adcVal / 503)*100;
				lCurrent = Math.round(lCurrent);
				return lCurrent/100;
			}
			else if(_pa[14]<8)//version du soft .. pcb 2 nouveau chip de courant mais soft version 7 et moins, on envoi en bit et non en amp
			{
				adcVal -= 131;//on lit 131 quand ya pas de courant
				if(adcVal<0)
				{
					adcVal = 0;
				}
				var lCurrent = (5.78 * adcVal / 640)*100;//valeurs calculé selon les mesures
				lCurrent = Math.round(lCurrent);
				if(lCurrent < 20)//pour cacher l'interférence
					lCurrent = 0;
				return lCurrent/100;
			}
			else // version soft 8 peu importe la version du board
			{
				return adcVal/100;
			}
        }
		},
		{
            name: 'fan2_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16]>>>2)&3;
				if(valAct == 0)
					return "POSITION 1";
				if(valAct == 1)
					return "POSITION 4";
				if(valAct == 2)
					return "POSITION 2";
				if(valAct == 3)
					return "POSITION 3";
				
                
            }
        },
		{
            name: 'fan2_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>6)&1;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "MOVING";
				
                
            }
        },
		{
            name: 'aspir_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>5)&1;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "MOVING";
				
                
            }
        },
		{
            name: 'arrosoir_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>4)&1;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "MOVING";
                
            }
        },
		{
            name: 'arrosoir_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16]>>>4)&3;
				if(valAct == 0)
					return "ERROR";
				if(valAct == 1)
					return "POSITION DEBUT";
				if(valAct == 2)
					return "POSITION FIN";
				if(valAct == 3)
					return "POSITION MILIEU";
				
                
            }
        },
		{
            name: 'degivrage_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>3)&1;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "MOVING";
				
                
            }
        },
		{
            name: 'degivrage_position',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[16]>>>6)&3;
				if(valAct == 0)
					return "ERROR";
				if(valAct == 1)
					return "POSITION DEBUT";
				if(valAct == 2)
					return "POSITION FIN";
				if(valAct == 3)
					return "POSITION MILIEU";
				
                
            }
        },
		{
            name: 'volet_actuator',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>2)&1;
				if(valAct == 0)
					return "STOP";
				if(valAct == 1)
					return "MOVING";
				
                
            }
        },
		{
            name: 'valve',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8]>>>1)&1;
				if(valAct == 0)
					return "OFF";
				if(valAct == 1)
					return "ON";
				
                
            }
        },
		{
            name: 'relais',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var valAct = (_pa[8])&1;
				if(valAct == 0)
					return "OFF";
				if(valAct == 1)
					return "ON";
				
                
            }
        },
				
		{
            name: 'control',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                var valMode = (_pa[21]);
				if(valMode == 1)
					return "BLUETOOTH";
				if(valMode == 2)
					return "HUB";
				if(valMode == 3)
					return "0-10 A";
				if(valMode == 4)
					return "0-10 B";
				if(valMode == 5)
					return "0-10 C";
				if(valMode == 6)
					return "MODBUS";
				if(valMode == 7)
					return "0-10 MAN";
				
				
			    return "UNKNOWN";
            }
        },
		{
			name: 'temperature_air_evacue',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				if (_pa[14]<8)//version soft..en bas de 8 on etait en bit et on prenait dans PA
				{
					var xa = _pa[19];
					var xb = _pa[20];
					var val1 = ((xa>>>2)&3)*256;
					var val2 = (xa<<6)&255; 
					var val3 = (xb>>>2);
				
					var adcVal = val1 + val2 + val3;
					return TemperatureC(adcVal);
				}
				else
				{
					var adcVal = _apa[11] *256 + _apa[10];
					if (adcVal>32767)
						adcVal = -(65536-adcVal);
				
					return (adcVal/10);
				}
				
				
			}
		},
		{
			name: 'temperature_exterieure',
			type: 'double',
			parser: function (_pa, _apa, _json, _typecsv) {
				if (_pa[14]<8)//version soft..en bas de 8 on etait en bit et on prenait dans PA
				{
					var xa = _pa[20];
					var xb = _pa[22];
					var adcVal = (xa&3)*256 + xb;
				
					return TemperatureC(adcVal);
				}
				else
				{
					var adcVal = _apa[13] *256 + _apa[12];
					if (adcVal>32767)
						adcVal = -(65536-adcVal);
				
					return (adcVal/10);
				}
				
				
			}
		},
		{
			
			name: 'temperature_a_inc',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[27] *256 + _pa[26];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10;
			}
		},
		{
			name: 'temperature_b_inc',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv) { 
                var value = _pa[29] *256 + _pa[28];
                if (value>32767)
                    value = -(65536-value);
                
                return value/10;
			
			}
		},
		{
            name: 'humidity_a_percentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[30]); 
			}
        },
		{
            name: 'humidity_b_percentage',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return (_pa[31]); 
			}
        },
		{
            name: 'actuator1_current',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){
				if (_pa[7] == 1)//version du pcb
				{
					return NaN;
				}
					
				else
				{
					if (_pa[14]<8)//version soft
					{
						var acur = (_pa[23]/200);
						acur = acur * 100;
						acur = Math.round(acur);
						if(acur < 3)//pour cacher l'interférence
							acur = 0;
						return (acur/100);
					}
					else
					{
						var acur = (_pa[23]);
						if(acur < 3)//pour cacher l'interférence
							acur = 0;
						return (acur/100);
					}
					
				}
                
                         
            }
        },
		{
            name: 'actuator2_current',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){
				if (_pa[7] == 1)//version du pcb
				{
					return NaN;
				}
					
				else
				{
					var acur = (_apa[4]);
					if(acur < 3)//pour cacher l'interférence
						acur = 0;
					return (acur/100);
				}
                
                         
            }
        },
		{
            name: 'recirculation_index',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				  return (_apa[18]);     
            }
        },
		{
            name: 'recirculation_angle',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				  return (_apa[19]);     
            }
        },
		{
            name: 'next_filtration',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				if (_pa[14]<9)//version soft
					return (_pa[15]*5);//5 min par bit
		         else
					return (_pa[15]*10);//10 min par bit 
                         
            }
        },
		{
            name: 'filtration_current',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){
				if (_pa[7] == 1) 
				{
					return NaN;
				}
					
				else
				{
					if (_pa[14]<8)//version soft
					{
						var cur = ((_pa[24] * 4) * 0.009 - 1.17)*100; //on a multiplie par 4 dans le soft et la formule a ete trouvée par mesures
						if(cur < 20)
							cur = 0;
						cur = Math.round(cur);
						return cur/100;
					}
					else
					{
						var cur = ((_pa[25] * 256) + _pa[24]); //
					
						return cur/100;
					}
					
				}        
            }
        },
		{
            name: 'next_cleaning',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				if (_pa[14]<9)//version soft
				{
					if ((_pa[7] == 1)&& (_pa[14]>4)&& (_pa[14]<9))//version du pcb et soft
					{
						return (_pa[23]*5);//5 min par bit
					}
					
					else
					{
						return (_apa[14]*5);//5 min par bit
					}
				}
				else
				{
					return (_apa[14]*10);//10 min par bit
				}
				
                
                         
            }
        },
		{
            name: 'cleaning_time',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
				if ((_pa[7] == 1)&& (_pa[14]>4)&& (_pa[14]<9))//version du pcb et soft
				{
					return _pa[24];
				}
					
				else
				{
					return _apa[15];
				}        
            }
        },
		{
            name: 'next_defrost',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
					if (_pa[14]<9)//version soft
						return (_apa[16]*5);//5 min par bit
					else
						return (_apa[16]*10);//10 min par bit
                         
            }
        },
		{
            name: 'defrost_time',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return _apa[17];
				        
            }
        },
		{
            name: 'errors_active',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				var errAct = (_apa[30])&1;
				  if(errAct == 1)
				  {
					  return "ERRORS ACTIVE IN MODBUS";
				  }
				  else if(errAct == 0)
				  {
					  return "ERRORS NOT ACTIVE IN MODBUS";
				  }					  
            }
        },
		{
            name: 'error_codes',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){
				var codeerreur = (_apa[5] + _apa[6]*256 + _apa[7] * 65536 + _apa[8] * 16777216);
				const erreurs = ["00-FAN1POS/","01-FAN2POS/","02-FAN1CURRENT HIGH/","03-FAN2CURRENT HIGH/","04-FAN1CURRENT LOW/","05-FAN2CURRENT LOW/","06-I2C/","07-TEMPERATURE A/","08-TEMPERATURE B/","09-HUMIDITY A/","10-HUMIDITY B/","11-EFFICIENCY LOW/","12-EFFICIENCY HIGH/","13-ACTUATOR1/","14-ACTUATOR2/","14-ACTUATOR3/","16-VACCURRENT LOW/","17-VACCURRENT HIGH/","E18/","19-FLOTTE HAUT/","20-FLOTTE BAS/","21-VACCURRENT OVER/","22-ACTUATOR5/","E23/","E24/","E25/","E26/","E27/","E28/","E29/","E30/","E31/"];
				let erreurText = "";
				for (let i = 0; i < erreurs.length; i++)
				{
					if(codeerreur & (Math.pow(2,i)))
					{
						erreurText += erreurs[i];
					}
				}
				return erreurText;
				       
            }
        },
		{
            name: 'modbus_address',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				  return (_apa[9]);     
            }
        },
		
		{
            name: 'efficacite',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				if (_pa[14]<8)//version soft..en bas de 8 on etait en bit et on prenait dans PA
				{
					var xa = _pa[20];
					var xb = _pa[22];
					var adcVal = (xa&3)*256 + xb;
				
					var T3 = TemperatureC(adcVal);
					
					var value = _pa[27] *256 + _pa[26];//temperatureA
					if (value>32767)
						value = -(65536-value);
					var T1 = value/10;
					
					value = _pa[29] *256 + _pa[28];//temperatureB
					if (value>32767)
						value = -(65536-value);
					var T4 = value/10;
					
					return Math.round(((T1-T3)/(T4-T3))*100);
					
				}
				else
				{
					var adcVal = _apa[13] *256 + _apa[12];//exterieur
					if (adcVal>32767)
						adcVal = -(65536-adcVal);
					var T3 = (adcVal/10);
					
					var value = _pa[27] *256 + _pa[26];//temperatureA
					if (value>32767)
						value = -(65536-value);
					var T1 = value/10;
					
					value = _pa[29] *256 + _pa[28];//temperatureB
					if (value>32767)
						value = -(65536-value);
					var T4 = value/10;
					
					return Math.round(((T1-T3)/(T4-T3))*100);
				}
				
				
				       
            }
        },
		{
            name: 'debit',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (30 * _pa[33]);
				       
            }
        },
		{
            name: 'reset_counter',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[32]);
				       
            }
        },
        {
            name: 'control_0_10v',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[34]);
                         
            }
        },
		{
            name: 'reg_mb',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					var regVal = _apa[21] *256 + _apa[20];
					return regVal;
				       
            }
        },
		{
            name: 'data20',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[20]);
				       
            }
        },
		{
            name: 'data21',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[21]);
				       
            }
        },
		{
            name: 'data22',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[22]);
				       
            }
        },
		{
            name: 'data23',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[23]);
				       
            }
        },
		{
            name: 'data24',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[24]);
				       
            }
        },
		{
            name: 'data25',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[25]);
				       
            }
        },
		{
            name: 'data26',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[26]);
				       
            }
        },
		{
            name: 'data27',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[27]);
				       
            }
        },
		{
            name: 'data28',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
				
					return (_apa[28]);
				       
            }
        }
    ];

    this.FunctionList = [
        {
            name: 'Refresh',
            method: function(_arg) {
                return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
            }
        },
        {
            name: 'Reset',
            method: function(_arg) {
                return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
            }
        },
        
		
        {
            name: 'SetSettings',
            arguments: [{name: 'sendNode', type: 'bool'}, {name: 'sendGroup', type: 'bool'}, {name: 'sendAmpSettings', type: 'bool'}, {name: 'sendForceRelayTime', type: 'bool'}, {name: 'nodeAddress', type: 'byte'}, {name: 'groupAddress', type: 'byte'}, {name: 'forceRelayTime', type: 'byte'}, {name: 'useAmp', type: 'byte', default: 2}, {name: 'txPower', type: 'byte', default: 255}],
            method: function(_arg) {
                var split = _arg.split(',');
                var sendNode = split[0].toLowerCase() === 'true';
                var sendGroup = split[1].toLowerCase() === 'true';
                var sendAmpSettings = split[2].toLowerCase() === 'true';
                var sendForceRelayTime = split[3].toLowerCase() === 'true';
                var NodeAddress = Number(split[4]);
                var GroupAddress = Number(split[5]);
                var ForceRelayTime = Number(split[6]);
                var useAmp = Number(split[7]);
                var txPower = Number(split[8]);

                var message = []
                message.push(0x1F);
                message.push(0x23);
                message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

                message = message.concat([
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

                return btoa(message);  
            }
        },
		{
        name: 'setESA3000',
		arguments: [{name:'stringToSend',type:'string'}],
		method: function(_arg) {
		var output = [];
		output.push (0x1f);
		output.push(0x41); 
		for (var i=0;i<_arg.length;i++)
		{
			output.push(_arg.charCodeAt(i));
			//var charCode = _arg.charCodeAt(i);
            //output.push((charCode & 0xFF00) >> 8);
            //output.push(charCode & 0xFF);
		}
		
		return btoa(output);

		
        
        }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};