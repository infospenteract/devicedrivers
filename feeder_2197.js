this.PropertiesList = [
    {
        name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
    },
    {
        name: 'feederlist',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['feederlist']; }
    },
    {
        name: 'managerid',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['managerid']; }
    },
    {
        name: 'priority',
        type: 'double',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
    },{
        name: 'driverinfos',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
    },{
        name: 'enable',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
    },
    {
        name: 'location',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
    },{
        name: 'seelowlist',
        type: 'string',
        parser: function(_pa, _apa, _json, _typecsv){ return _json['seelowlist']; }
    },{
        name: 'closeatnight',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['closeatnight']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
    },
];