
    this.PropertiesList = [
        {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['location']; }
        },
        {
            name: 'latitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['latitude']; }
        },
        {
            name: 'longitude',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['longitude']; }
        },
        {
            name: 'action',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if (_pa[0]==0)
                    return "idle";
                
                if (_pa[0]==1)
                    return "heat";
                
                if (_pa[0]==2)
                    return "vent";
            }
        },
        {
            name: 'actionin',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ 
                if (_pa[0]==0)
                    return 0;
                
                if (_pa[0]==1)
                    return 20;
                
                if (_pa[0]==2)
                    return -20;
            }
        },
        {
            name: 'strength',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){ 
                return _pa[1];
            }
        },
        {
            name: 'expectedtemperatureincelcius',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ 
                return (_pa[2]*256+_pa[3])/10;
            }
        },
        {
            name: 'outdoordevice',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['outdoordevice']; }
        },
        {
            name: 'indoordevice',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['indoordevice']; }
        },
        {
            name: 'thermostatidvalue',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['thermostatidvalue']; }
        },
        {
            name: 'humiditylevel',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['humiditylevel']; }
        },
        {
            name: 'co2limitppm',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['co2limitppm']; }
        },
        {
            name: 'nh3limitppm',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nh3limitppm']; }
        },
        {
            name: 'alertovertempdelta',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertovertempdelta']; }
        },
        {
            name: 'alertundertempdelta',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertundertempdelta']; }
        }
    ];

    this.FunctionList = [
	{
            name: 'Calibrate',
            method: function(_arg) {
		      //Argument must be degree in celcius Must be call using Extended structure test
                return "";
            },
            name: 'Purge',
            method: function(_arg) {
		      //Argument must be degree in celcius Must be call using Extended structure test
                return "";
            }
        }
    ];

    this.DefaultBlob = {"name":"", "latitude":0, "longitude":0, "location":"outdoor"};