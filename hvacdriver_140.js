  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  this.PropertiesList = [
      {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
      {
          name: 'location',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['location'];
          }
        },
      {
          name: 'latitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['latitude'];
          }
        },
      {
          name: 'longitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['longitude'];
          }
        },
      {
          name: 'heatstrength',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[0];
          }
        },
      {
          name: 'coolstrength',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[1];
          }
        },
        {
            name: 'temperatureincelcius',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                return convert(_pa).split('|')[2];
            }
          },
        {
            name: 'sourcetemperatureincelcius',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                return convert(_pa).split('|')[3];
            }
          },
          {
            name: 'temperatureinfarenheit',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                var value = convert(_pa).split('|')[2];
                return ( value * 9 / 5 ) + 32;
            }
          },
        {
            name: 'sourcetemperatureinfarenheit',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                var value = convert(_pa).split('|')[3];
                return ( value * 9 / 5 ) + 32;
            }
          },
      {
          name: 'staticpressureininh2o',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[4];
          }
        },
      {
          name: 'humiditypercentage',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[5];
          }
        },
      {
          name: 'co2ppm',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[6];
          }
        }
    ,
      {
          name: 'amoniappm',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[7];
          }
        }
        ,
        {
            name: 'expectedtemperatureincelcius',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                return convert(_pa).split('|')[8];
            }
          }
          ,
          {
              name: 'expectedtemperatureinfarenheit',
              type: 'double',
              parser: function (_pa, _apa, _json, _typecsv) {
                var value = convert(_pa).split('|')[8];
                return ( value * 9 / 5 ) + 32;
              }
            }
    ,
      {
          name: 'co2meters',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['co2meters'];
          }
        }
    ,
      {
          name: 'indoorlist',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['indoorlist'];
          }
        }
        ,
      {
          name: 'amoniameters',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['amoniameters'];
          }
        }
    ,
      {
          name: 'embreeze',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['embreeze'];
          }
        }
    ,
      {
          name: 'minimalstrength',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['minimalstrength'];
          }
        }
        ,
      {
          name: 'sourceid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['sourceid'];
          }
        },
      {
          name: 'deltaminute',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[9];
          }
        },
        {
            name: 'deltaminutefarenheit',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                var value = convert(_pa).split('|')[9];
                return value * 9 / 5;
            }
          },
          {
              name: 'humiditylevel',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['humiditylevel']; }
          },
          {
              name: 'co2limitppm',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['co2limitppm']; }
          },
          {
              name: 'nh3limitppm',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['nh3limitppm']; }
          },
          {
              name: 'alertovertempdelta',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['alertovertempdelta']; }
          },
          {
              name: 'alertundertempdelta',
              type: 'double',
              parser: function(_pa, _apa, _json, _typecsv){ return _json['alertundertempdelta']; }
          }

    ];

  this.FunctionList = [
      {
          name: 'SetTemperatureInCelcius',
          method: function (_arg) {
              //Argument must be degree in celcius Must be call using Extended structure test
              return "";
          }
        },
      {
          name: 'SetMinimalVent',
          method: function (_arg) {
              //Argument must be degree in celcius Must be call using Extended structure test
              return "";
          }
            }
];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
