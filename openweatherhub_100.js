  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  this.PropertiesList = [
      {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
      {
          name: 'location',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['location'];
          }
        },
      {
          name: 'latitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['latitude'];
          }
        },
      {
          name: 'longitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['longitude'];
          }
        },
      {
          name: 'temperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[0] + _pa[1] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
        },
      {
          name: 'currenttemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[0] + _pa[1] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
        },
      {
          name: 'expectedmaxtemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[2] + _pa[3] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
        },
      {
          name: 'expectedmintemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[4] + _pa[5] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
        },
      {
          name: 'temperatureincelcius1hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[6] + _pa[7] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius2hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[8] + _pa[9] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius3hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[10] + _pa[11] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius4hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[12] + _pa[13] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius5hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[14] + _pa[15] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius6hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[16] + _pa[17] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius7hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[18] + _pa[19] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius8hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[20] + _pa[21] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius9hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[22] + _pa[23] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius10hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[24] + _pa[25] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius11hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[26] + _pa[27] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'temperatureincelcius12hr',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[28] + _pa[29] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'typeofweather',
          type: 'byte',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[30] + _pa[31] * 256;
              // if (val > 32768)
              //     val = -(65536 - val)
              return val;
          }
          },
      {
          name: 'cloudpercetage',
          type: 'byte',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[32];
              // if (val > 32768)
              //     val = -(65536 - val)
              return val;
          }
          },
      {
          name: 'humiditypercentage',
          type: 'byte',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[33];
              // if (val > 32768)
              //     val = -(65536 - val)
              return val;
          }
          },
      {
          name: 'windspeedkmh',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[34] + _pa[35] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          },
      {
          name: 'winddirectiondegree',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var val = _pa[36] + _pa[37] * 256;
              if (val > 32768)
                  val = -(65536 - val)
              return val / 10;
          }
          }



    ];

  this.FunctionList = [
];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
