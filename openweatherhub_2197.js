  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  var tofar = function tofare(data) {
      return (data * 9 / 5) + 32;
  }


  this.PropertiesList = [
      {
          name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority'];
          }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        }, {
          name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
        {
            name: 'location',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['location'];
            }
          },
          {
            name: 'latitude',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['latitude'];
            }
          },
          {
            name: 'longitude',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['longitude'];
            }
          },
	{
          name: 'temperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[0]).toFixed(0);
          }
      },
      {
          name: 'currentempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[0]).toFixed(0);
          }
      },
      {
          name: 'currentempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[0])).toFixed(0);
          }
      },
      {
          name: 'currentweathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[1];
          }
      },
      {
          name: 'currenthumiditypercentage',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[2];
          }
      },
      {
          name: 'currentcloudpercentage',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[3];
          }
      },
      {
          name: 'currentwindspeed',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[4];
          }
      },
      {
          name: 'currentwinddirection',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[5];
          }
      },
      {
          name: '2hrtempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[6]).toFixed(0);
          }
      },
      {
          name: '2hrtempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[6])).toFixed(0);
          }
      },
      {
          name: '2hrweathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[7];
          }
      },
      {
          name: '4hrtempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[8]).toFixed(0);
          }
      },
      {
          name: '4hrtempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[8])).toFixed(0);
          }
      },
      {
          name: '4hrweathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[9];
          }
      },
      {
          name: '6hrtempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[10]).toFixed(0);
          }
      },
      {
          name: '6hrtempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[10])).toFixed(0);
          }
      },
      {
          name: '6hrweathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[11];
          }
      },
      {
          name: '8hrtempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[12]).toFixed(0);
          }
      },
      {
          name: '8hrtempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[12])).toFixed(0);
          }
      },
      {
          name: '8hrweathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[13];
          }
      },
      {
          name: 'night1tempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[14]).toFixed(0);
          }
      },
      {
          name: 'night1tempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[14])).toFixed(0);
          }
      },
      {
          name: 'night1weathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[15];
          }
      },
      {
          name: 'day1tempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[16]).toFixed(0);
          }
      },
      {
          name: 'day1tempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[16])).toFixed(0);
          }
      },
      {
          name: 'day1weathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[17];
          }
      },
      {
          name: 'night2tempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[18]).toFixed(0);
          }
      },
      {
          name: 'night2tempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[18])).toFixed(0);
          }
      },
      {
          name: 'night2weathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[19];
          }
      },
      {
          name: 'day2tempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[20]).toFixed(0);
          }
      },
      {
          name: 'day2tempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[20])).toFixed(0);
          }
      },
      {
          name: 'day2weathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[21];
          }
      },
      {
          name: 'night3tempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[22]).toFixed(0);
          }
      },
      {
          name: 'night3tempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[22])).toFixed(0);
          }
      },
      {
          name: 'night3weathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[23];
          }
      },
      {
          name: 'day3tempincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return parseFloat(convert(_pa).split('|')[24]).toFixed(0);
          }
      },
      {
          name: 'day3tempinfahrenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return tofar(parseFloat(convert(_pa).split('|')[24])).toFixed(0);
          }
      },
      {
          name: 'day3weathertype',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[25];
          }
      }

    ];

  this.FunctionList = [
            ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
