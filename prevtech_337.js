function Utf8ArrayToStr(array) {
    var out, i, len, c;
    var char2, char3;

    out = "";
    len = array.length;
    i = 0;
    while (i < len) {
        c = array[i++];
        switch (c >> 4) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                // 0xxxxxxx
                out += String.fromCharCode(c);
                break;
            case 12:
            case 13:
                // 110x xxxx   10xx xxxx
                char2 = array[i++];
                out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                break;
            case 14:
                // 1110 xxxx  10xx xxxx  10xx xxxx
                char2 = array[i++];
                char3 = array[i++];
                out += String.fromCharCode(((c & 0x0F) << 12) |
                    ((char2 & 0x3F) << 6) |
                    ((char3 & 0x3F) << 0));
                break;
        }
    }
    return out;
}

this.PropertiesList = [
    {
        name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["UserDescription"];
        }
        },
    {
        name: 'driverinfos',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _typecsv;
        }
        },
    {
        name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
          },
    {
        name: 'latitude',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
          },
    {
        name: 'longitude',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
          },
    {
        name: 'apikey',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return Utf8ArrayToStr(_apa).split(',')[0]; 
        }
          },
    {
        name: 'processorid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["ProcessorID"];
        }
          },
    {
        name: 'SNID',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["SNID"];
        }
          },
    {
        name: 'parentid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return Utf8ArrayToStr(_apa).split(',')[1]; 
        }
          },
    {
        name: 'isonline',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["OnlineStatus"];
        }
        },
    {
        name: 'isalarm',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["IsAlarm"];
        }
        },
    {
        name: 'issnoozed',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["IsSnoozed"];
        }
        },    {
        name: 'PESI',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["PESI"];
        }
        },
    {

        name: 'hardwaretype',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["HardwareType"];
        }
        },
    {
        name: 'firmwareversion',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return JSON.parse(Utf8ArrayToStr(_pa))["FirmwareVersion"];
        }
        }
    ];


this.FunctionList = [
            ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
