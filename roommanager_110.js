  var convert = function stringFromArray(data) {
      var count = data.length;
      var str = "";

      for (var index = 0; index < count; index += 1)
          str += String.fromCharCode(data[index]);

      return str;
  }

  this.PropertiesList = [
      {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },{
            name: 'message',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['message']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },{
          name: 'roomimage',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['roomimage']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
      {
          name: 'location',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['location'];
          }
        },
        {
            name: 'cameraid',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['cameraid'];
            }
          },
        {
            name: 'feederlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['feederlist'];
            }
        },
      {
          name: 'latitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['latitude'];
          }
        },
      {
          name: 'longitude',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['longitude'];
          }
        },
      {
          name: 'amoniappm',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[0];
          }
        },
      {
          name: 'animalmeanweightinkg',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[1];
          }
        },
      {
          name: 'co2ppm',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[2];
          }
        },
      {
          name: 'currentlumens',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[3];
          }
        },
      {
          name: 'currenttemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[4];
          }
        },
      {
          name: 'expectedtemperatureincelcius',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[5];
          }
        },
        {
          name: 'currenttemperatureinfarenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var value = convert(_pa).split('|')[4];
              return (value * 9 / 5) + 32;
          }
          },
      {
          name: 'expectedtemperatureinfarenheit',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              var value = convert(_pa).split('|')[5];
              return (value * 9 / 5) + 32;
          }
          },
      {
          name: 'foodremainingweightinkg',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[6];
          }
        },
      {
          name: 'humiditypercentage',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[7];
          }
        },
      {
          name: 'blobnumberofanimals',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json["numberofanimals"];
          }
        },
      {
          name: 'numberofanimals',
          type: 'int',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[14];
          }
        },
      {
          name: 'staticpressureininh20',
          type: 'double',
          parser: function (_pa, _apa, _json, _typecsv) {
              return convert(_pa).split('|')[15];
          }
        },
      {
          name: 'dimmerid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['dimmerid'];
          }
        },
      {
          name: 'hvacid',
          type: 'string',
          parser: function (_pa, _apa, _json, _typecsv) {
              return _json['hvacid'];
          }
        },
        {
            name: 'seelowsid',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['seelowsid'];
            }
          },
          {
              name: 'heaterlist',
              type: 'string',
              parser: function (_pa, _apa, _json, _typecsv) {
                  return _json['heaterlist'];
              }
            },
            {
                name: 'fanslist',
                type: 'string',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return _json['fanslist'];
                }
              },
              {
                name: 'walllist',
                type: 'string',
                parser: function (_pa, _apa, _json, _typecsv) {
                    return _json['walllist'];
                }
              },
              {
                  name: 'actuatorlist',
                  type: 'string',
                  parser: function (_pa, _apa, _json, _typecsv) {
                      return _json['actuatorlist'];
                  }
                },
                {
                    name: 'openweatherhubid',
                    type: 'string',
                    parser: function (_pa, _apa, _json, _typecsv) {
                        return _json['openweatherhubid'];
                    }
                  },
                  {
                      name: 'selectedmode',
                      type: 'string',
                      parser: function (_pa, _apa, _json, _typecsv) {
                          return _json['selectedmode'];
                      }
                            }

    ];

  this.FunctionList = [
      {
          name: 'Refresh',
          method: function (_arg) {
              return "";
          }
            },
      {
          name: 'SetTemperatureInCelcius',
          method: function (_arg) {
              //Argument most be de number of days to offset start sequence
              return "";
          }
            },
      {
          name: 'SetLumens',
          method: function (_arg) {
              //Argument most be de number of days to offset start sequence
              return "";
          }
            }
    ];

  this.DefaultBlob = {
      "name": "",
      "latitude": 0,
      "longitude": 0,
      "location": "outdoor"
  };
