var temperatureParser = function (_pa) {
    var temp = _pa[30] * 256 + _pa[29];
    if (temp > 32768)
        temp = -(temp - 32768);
    return temp;
}

var ConeVolume = function (diameter, height) {
    return Math.PI * Math.pow(diameter / 2, 2) * height / 3;
}

var CylinderVolume = function (diameter, height) {
    return Math.PI * Math.pow(diameter / 2, 2) * height;
}

var totalVolumeParser = function (_pa, _json) {

    var top = Number(_json["section0"]);
    var center = Number(_json["section1"]);
    var bottom = Number(_json["section2"]);
    var diameter = Number(_json["diameter"]);


    var temptv = ConeVolume(diameter, top);
    temptv = temptv + ConeVolume(diameter, bottom);
    temptv = temptv + CylinderVolume(diameter, center);
    return temptv;
}

var volumeParser = function (_pa, _json, _apa) {
    var top = Number(_json["section0"]);
    var center = Number(_json["section1"]);
    var bottom = Number(_json["section2"]);
    var diameter = Number(_json["diameter"]);

    var max = (_pa[37] * 256 + _pa[36]) / 1000;
    var min = (_pa[35] * 256 + _pa[34]) / 1000;


    //max = max + bottom * (max / (top + center + bottom));


    if (max > top + center + bottom)
        max = top + center + bottom;



    var totVol = totalVolumeParser(_pa, _json);
    var relativediameter = diameter;
    var tempuv = 0;
    if (max < top) {
        relativediameter = diameter * (min / top);
        tempuv = totVol - ConeVolume(relativediameter, max);
        //tempuv = tempuv - ConeVolume(relativediameter, max - min);
    } else if (max < top + center) {
        tempuv = totVol - ConeVolume(diameter, top);
        tempuv = tempuv - CylinderVolume(diameter, max - top);
        //relativediameter = relativediameter * ((min - top) / center);
        //tempuv = tempuv - ConeVolume(relativediameter, max - min);
    } else {
        relativediameter = diameter * (bottom - (max - center - top)) / bottom;
        tempuv = ConeVolume(relativediameter, bottom - (max - center - top));
    }


    if (tempuv > totVol)
        tempuv = totVol;
    return tempuv;
};

var Histogram = function (values, midvalue, range) {
    var i = 0;
    var t = [];
    var count = [0, 0];
    var counter = 0;
    for (i = 0; i < values.length; i++) {
        if (values[i] < midvalue + range && values[i] > midvalue - range) {
            t.push(values[i]);
        }
        if (values[i] > 20)
            counter++;
    }



    for (i = 0; i < t.length; i++) {
        if (t[i] < midvalue) {
            count[0]++;
        } else {
            count[1]++;
        }
    }

    if (count[0] > count[1]) {
        return [midvalue - range / 2, count[0] * 100 / counter];
    } else {
        return [midvalue + range / 2, count[1] * 100 / counter];
    }

}


var RecursiveHistogram = function (values, midvalue, rangeinput, perctolerance) {
    var startvalue = midvalue;
    var range = rangeinput;
    var back = [-1, -1];

    var tk = Histogram(values, startvalue, range);

    while (tk[1] > perctolerance && range > 15) {
        back = [tk[0], tk[1]];
        tk = Histogram(values, startvalue, range);
        startvalue = tk[0];
        range = range / 2;
    }

    return back;
}

var RetrivedReadValues = function (_apa) {
    var i = 0;
    var output = [];
    for (i = 0; i < 510; i = i + 2) {
        output.push(_apa[i * 2 + 1] * 256 + _apa[i * 2]);
    }

    return output;
}


this.PropertiesList = [
    {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
        },
        {
            name: 'feederlist',
            type: 'string',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json['feederlist'];
            }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'volmas',
            type: 'double',
            parser: function (_pa, _apa, _json, _typecsv) {
                return _json["volmas"];
            }
        },
        {
            name: 'section0',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['section0'];
        }
        },
    {
        name: 'section1',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['section1'];
        }
        },
    {
        name: 'section2',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['section2'];
        }
        },
    {
        name: 'diameter',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['diameter'];
        }
        },


    {
        name: 'encrypted',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return false;
        }
        },
    {
        name: 'relaisid',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            var out = "0x";
            var i = 0;
            for (i = 0; i < 4; i++) {
                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
            }
            return out;
        }
        },
    {
        name: 'chargingstate',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[26] > 0;
        }
        },
    {
        name: 'txrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[10]);
        }
        },
    {
        name: 'rxrssi',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (-_pa[9]);
        }
        },
    {
        name: 'worstrssi',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[11];
        }
        },
    {
        name: 'nodeadress',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[12];
        }
        },
    {
        name: 'meshgroup',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[6];
        }
        },
    {
        name: 'level',
        type: 'byte',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[1];
        }
        },
    {
        name: 'batteryad',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[8] * 256 + _pa[7];
        }
        },
    {
        name: 'battvoltage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2;
        }
        },
    {
        name: 'battpercentage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var BattAD;
            BattAD = (_pa[8] * 256 + _pa[7]) / 4096 * 3.3 * 2;

            if (BattAD >= 4.2)
				return 100;
            if (BattAD > 3.95)
                return ((BattAD - 3.95) * 80 + 80);
            if (BattAD > 3.8)
                return ((BattAD - 3.80) * 133.3333 + 60);
            if (BattAD > 3.65)
                return ((BattAD - 3.65) * 266.3333 + 20);
            if (BattAD > 3.3)
                return ((BattAD - 3.3) * 57.14 + 5);

            return 0;

        }
        },
    {
        name: 'doorstate',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[33];
        }
        },
    {
        name: 'heightmaxinfos',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			var maxInfos = _pa[32] * 256 + _pa[31];
			if(maxInfos == 0)
			{
				return "Valid";
			}
			if(maxInfos == 65535)
			{
				return "Invalid: reset value"
			}
			
			var errorCode = _pa[37] * 256 + _pa[36];
			var errorHappend = maxInfos & 0x8000;
			
			if(errorCode == 15)
			{
				return "Invalid: Distance too short";
			}
			if(errorCode == 16)
			{
				return "Invalid: No echo";
			}
			if(errorHappend == 32768)
			{
				return "Invalid: an error happened";
			}
			
			return "Invalid: unknown reason"
        }
        },
	{
        name: 'heightmininfos',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
			var minInfos = _pa[28] * 256 + _pa[27];
			if(minInfos == 0)
			{
				return "Valid";
			}
			if(minInfos == 65535)
			{
				return "Invalid: reset value"
			}
			
			var errorCode = _pa[35] * 256 + _pa[34];
			var errorHappend = minInfos & 0x8000;
			
			if(errorCode == 15)
			{
				return "Invalid: Distance too short";
			}
			if(errorCode == 16)
			{
				return "Invalid: No echo";
			}
			if(errorHappend == 32768)
			{
				return "Invalid: an error happened";
			}
			
			return "Invalid: unknown reason"
        }
        },
    {
        name: 'heightmaxmm',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[37] * 256 + _pa[36];
        } //TODO: not finished
        },
	{
        name: 'heightminmm',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _pa[35] * 256 + _pa[34];
        } //TODO: not finished
        },  

    {
        name: 'heightmax',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            var top = Number(_json["section0"]);
            var center = Number(_json["section1"]);
            var bottom = Number(_json["section2"]);
            var totalheight = top + center + bottom;
            var tmp = (_pa[37] * 256 + _pa[36]) / 1000;
            if (tmp > totalheight)
                tmp == totalheight;

            return tmp;

        } //TODO: not finished
        },
    {
        name: 'heightmin',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[35] * 256 + _pa[34]) / 1000;
        } //TODO: not finished
        },
    {
        name: 'heightinmeter',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {

            var top = Number(_json["section0"]);
            var center = Number(_json["section1"]);
            var bottom = Number(_json["section2"]);
            var totalheight = top + center + bottom;
            var min = (_pa[35] * 256 + _pa[34]) / 1000;
            var max = (_pa[37] * 256 + _pa[36]) / 1000;
            if (min < 0)
                min = 0;

            if (max > totalheight)
                max = totalheight;

            if (max - min > 1)
                min = max - 1;

            var hg = max;

            if (hg > totalheight)
                hg = totalheight;
            return hg;

        } //TODO: not finished
        },
    {
        name: 'weightinkg',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var vl = volumeParser(_pa, _json, _apa) * Number(_json["volmas"]) * 1000;
            if (vl < 0) vl = 0;
            return vl;
        }
        },
        {
        name: 'weightintons',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var vl = volumeParser(_pa, _json, _apa) * Number(_json["volmas"]);
            if (vl < 0) vl = 0;
            return vl;
        }
        },
        {
        name: 'weightintonsh',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var vl = volumeParser(_pa, _json, _apa) * Number(_json["volmas"]) *1.1;
            if (vl < 0) vl = 0;
            return vl;
        }
        },
    {
        name: 'volumicmassinkg',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var vl = Number(_json["volmas"]);
            if (vl < 0) vl = 0;
            return vl;
        }
        },
        {
            name: 'alertlow',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alertlow']; }
        },
        {
            name: 'alerthigh',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['alerthigh']; }
        },
    {
        name: 'totalvolumeinm3',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return totalVolumeParser(_pa, _json);
        }
        },
    {
        name: 'volumeinm3',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var vl = volumeParser(_pa, _json, _apa);
            if (vl < 0) vl = 0;
            return vl;
        }
        },
    {
        name: 'percentage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var pr = (volumeParser(_pa, _json, _apa) / totalVolumeParser(_pa, _json)) * 100;
            if (pr < 0) pr = 0;
            if (pr > 100) pr = 100;
            return pr;
        }
        },
        {
            name: 'rcon',
            type: 'int',
            parser: function(_pa, _apa, _json, _typecsv){
                return _pa[25];
            }
        },
        {
            name: 'brownout',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
				if((_pa[25] & 0x02) == 0x02)
				{
					return (_pa[25] & 0x01) < 1;
				}
				else 
				{
					return false;
				}
            }
        },
        {
            name: 'poweronreset',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[25] & 0x02) < 1;
            }
        },
        {
            name: 'wdt',
            type: 'bool',
            parser: function(_pa, _apa, _json, _typecsv){
                return (_pa[25] & 0x08)  < 1;
            }
        }
    ];

this.FunctionList = [
    {
        name: 'RefreshWithReading',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x01]);
        }
        },
    {
        name: 'Refresh',
        method: function (_arg) {
            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02]);
        }
        },
    {
        name: 'Reset',
        method: function (_arg) {
            return btoa([0x1f, 0x28, 0x52, 0x53, 0x54]);
        }
        },
    {
        name: 'SetSettings',
        arguments: [{
            name: 'sendNode',
            type: 'bool'
        }, {
            name: 'sendGroup',
            type: 'bool'
        }, {
            name: 'sendAmpSettings',
            type: 'bool'
        }, {
            name: 'sendForceRelayTime',
            type: 'bool'
        }, {
            name: 'nodeAddress',
            type: 'byte'
        }, {
            name: 'groupAddress',
            type: 'byte'
        }, {
            name: 'forceRelayTime',
            type: 'byte'
        }, {
            name: 'useAmp',
            type: 'byte',
            default: 2
        }, {
            name: 'txPower',
            type: 'byte',
            default: 255
        }],
        method: function (_arg) {
            var split = _arg.split(',');
            var sendNode = split[0].toLowerCase() === 'true';
            var sendGroup = split[1].toLowerCase() === 'true';
            var sendAmpSettings = split[2].toLowerCase() === 'true';
            var sendForceRelayTime = split[3].toLowerCase() === 'true';
            var NodeAddress = Number(split[4]);
            var GroupAddress = Number(split[5]);
            var ForceRelayTime = Number(split[6]);
            var useAmp = Number(split[7]);
            var txPower = Number(split[8]);

            var message = []
            message.push(0x1F);
            message.push(0x23);
            message.push(0);
				message.push(0);
				message.push(0);
                if (sendNode){
                    message[2] |= 4;
					message[3] |= 4;
					message[4] |= 4;
                }

                if (sendGroup){
                    message[2] |= 2;
					message[3] |= 2;
					message[4] |= 2;
                }

                if (sendForceRelayTime){
                    message[2] |= 8;
					message[3] |= 8;
					message[4] |= 8;
                }

                if (sendAmpSettings && txPower != 255 && useAmp != 2){
                    message[2] |= 1;
					message[3] |= 1;
					message[4] |= 1;
                }

            message = message.concat([
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

            return btoa(message);
        }
        }
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor",
    "section0": 0.40,
    "section1": 4,
    "section2": 1,
    "diameter": 2,
    "volmas": 1
};
