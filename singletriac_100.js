this.PropertiesList = [
    {
            name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
        },{
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        },{
            name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },{
            name: 'relayimage0',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['relayimage0']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'relaystate',
        type: 'bool',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[0] & 1) != 0;
        }
        },
    {
        name: 'relaystateint',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[0] & 1);
        }
        },
    {
        name: 'currentpercentage',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[1]);
        }
        },
        {
            name: 'connectedobject',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['connectedobject']; }
        },
    {
        name: 'minpercentage',
        type: 'int',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['minpercentage'];
        }
    }
    ];

this.FunctionList = [
    {
        name: 'SetRelay',
        method: function (_arg) {}
        },
    {
        name: 'SetPercentage',
        method: function (_arg) {}
        }
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
