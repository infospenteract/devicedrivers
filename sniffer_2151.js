		var LatitudeDDM = function (input) {
		    var temp = input;
		    if (temp > 180000000) {
		        temp = -(4294967296 - temp);
		    }

		    var degree = Math.floor(temp / 1000000);
		    var minute = (temp - degree * 1000000) / 10000;
		    var decidegree = minute / 60;

		    return degree + decidegree;
		}

		var LongitudeDDM = function (input) {
		    var temp = input;
		    if (temp > 180000000) {
		        temp = -(4294967296 - temp);
		    }

		    var degree = Math.floor(temp / 1000000);
		    var minute = (temp - degree * 1000000) / 10000;
		    var decidegree = minute / 60;

		    return degree + decidegree;
		}




		this.PropertiesList = [
			{
				name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
				type: 'string',
				parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority']; }
			},{
				name: 'driverinfos',
				type: 'string',
				parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
			},{
				name: 'enable',
				type: 'string',
				parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
			},
		    {
		        name: 'encrypted',
		        type: 'bool',
		        parser: function (_pa, _apa, _json) {
		            return false;
		        }
        },
		    {
		        name: 'relaisid',
		        type: 'string',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            var out = "0x";
		            var i = 0;
		            for (i = 0; i < 4; i++) {
		                out += ("0" + _pa[2 + i].toString(16)).slice(-2);
		            }
		            return out;
		        }
        },
		    {
		        name: 'txrssi',
		        type: 'int',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return (-_pa[10]);
		        }
        },
		    {
		        name: 'rxrssi',
		        type: 'int',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return (-_pa[9]);
		        }
        },
		    {
		        name: 'worstrssi',
		        type: 'byte',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return _pa[11];
		        }
        },
		    {
		        name: 'nodeadress',
		        type: 'byte',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return _pa[12];
		        }
        },
		    {
		        name: 'meshgroup',
		        type: 'byte',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return _pa[6];
		        }
        },
		    {
		        name: 'level',
		        type: 'byte',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return _pa[1];
		        }
        },
		    {
		        name: 'batteryad',
		        type: 'int',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return _pa[8] * 256 + _pa[7];
		        }
        },
		    {
		        name: 'battvoltage',
		        type: 'double',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return (_pa[8] * 256 + _pa[7] ) / 4096 * 3.3 * 2;
		        }
        },
		    {
		        name: 'delay',
		        type: 'int',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return (_pa[27] * 15);
		        }
        },
		    {
		        name: 'latitude',
		        type: 'double',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return LatitudeDDM(_pa[31] * 16777216 + _pa[30] * 65536 + _pa[29] * 256 + _pa[28]);
		        }
        },
		    {
		        name: 'longitude',
		        type: 'double',
		        parser: function (_pa, _apa, _json, _typecsv) {
		            return LongitudeDDM(_pa[35] * 16777216 + _pa[34] * 65536 + _pa[33] * 256 + _pa[32]);
		        }
        }
    ];

		this.FunctionList = [
		    {
		        name: 'Refresh',
		        arguments: [],
		        method: function (_arg) {
		            return btoa([0x1e, 0x01, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x02, 0x01]);
		        }
        },
		    {
		        name: 'Reset',
		        arguments: [],
		        method: function (_arg) {
		            return btoa([0x1f, 0x28, 0x52, 0x53, 0x054]);
		        }

        },
		    {
		        name: 'SetSettings',
		        arguments: [{
		            name: 'sendNode',
		            type: 'bool'
		        }, {
		            name: 'sendGroup',
		            type: 'bool'
		        }, {
		            name: 'sendAmpSettings',
		            type: 'bool'
		        }, {
		            name: 'sendForceRelayTime',
		            type: 'bool'
		        }, {
		            name: 'nodeAddress',
		            type: 'byte'
		        }, {
		            name: 'groupAddress',
		            type: 'byte'
		        }, {
		            name: 'forceRelayTime',
		            type: 'byte'
		        }, {
		            name: 'useAmp',
		            type: 'byte',
		            default: 2
		        }, {
		            name: 'txPower',
		            type: 'byte',
		            default: 255
		        }],
		        method: function (_arg) {
		            var split = _arg.split(',');
		            var sendNode = split[0].toLowerCase() === 'true';
		            var sendGroup = split[1].toLowerCase() === 'true';
		            var sendAmpSettings = split[2].toLowerCase() === 'true';
		            var sendForceRelayTime = split[3].toLowerCase() === 'true';
		            var NodeAddress = Number(split[4]);
		            var GroupAddress = Number(split[5]);
		            var ForceRelayTime = Number(split[6]);
		            var useAmp = Number(split[7]);
		            var txPower = Number(split[8]);

		            var message = []
		            message.push(0x1F);
		            message.push(0x23);
		            if (sendNode) {
		                message = message.concat([message[2] | 4, message[3] | 4, message[4] | 4]);
		            }

		            if (sendGroup) {
		                message = message.concat([message[2] | 2, message[3] | 2, message[4] | 2]);
		            }

		            if (sendForceRelayTime) {
		                message = message.concat([message[2] | 8, message[3] | 8, message[4] | 8]);
		            }

		            if (sendAmpSettings && txPower != 255 && useAmp != 2) {
		                message = message.concat([message[2] | 1, message[3] | 1, message[4] | 1]);
		            }

		            message = message.concat([
		                (useAmp & 0x01) + (txPower & 0x0E),
		                (useAmp & 0x01) + (txPower & 0x0E),
		                (useAmp & 0x01) + (txPower & 0x0E),
                    GroupAddress,
                    GroupAddress,
                    GroupAddress,
                    NodeAddress,
                    NodeAddress,
                    NodeAddress,
                    ForceRelayTime,
                    ForceRelayTime,
                    ForceRelayTime
                ]);

		            return btoa(message);
		        }
        }
    ];

		this.DefaultBlob = {
		    "name": "",
		    "latitude": 0,
		    "longitude": 0,
		    "location": "outdoor"
		};
