this.PropertiesList = [
    {
        name: 'error',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['error']; }
        },{
            name: 'computedvalues',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['computedvalues']; }
        },{
            name: 'name',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['name']; }
        },
        {
            name: 'nodeaddress',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['nodeaddress']; }
        },
        {
            name: 'priority',
            type: 'double',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['priority'];
        }
        },
        {
            name: 'driverinfos',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _typecsv; }
        }, {
        name: 'enable',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['enable']; }
        },{
            name: 'options',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['options']; }
        },{
            name: 'room',
            type: 'string',
            parser: function(_pa, _apa, _json, _typecsv){ return _json['room']; }
        },
    {
        name: 'location',
        type: 'string',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['location'];
        }
        },
    {
        name: 'latitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['latitude'];
        }
        },
    {
        name: 'longitude',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return _json['longitude'];
        }
        },
    {
        name: 'settempincelcius',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            return (_pa[6] * 256 + _pa[7]) / 100;
        }
        },
    {
        name: 'temperatureincelcius',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var value = _pa[2] * 256 + _pa[3];
            return value / 100;
        }
        },
    {
        name: 'heatstrengthpercentage',
        type: 'double',
        parser: function (_pa, _apa, _json, _typecsv) {
            var value = _pa[8];
            return value;
        }
        }
    ];

this.FunctionList = [
    {
        name: 'SetTempInCelcius',
        method: function (_arg) {
            //Argument must be degree in celcius Must be call using Extended structure test
            return "";
        }
        }
    ];

this.DefaultBlob = {
    "name": "",
    "latitude": 0,
    "longitude": 0,
    "location": "outdoor"
};
