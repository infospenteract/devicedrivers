import sys
import numpy as np
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d
import math
from datetime import datetime, timedelta

def run_control(values):
# ###################################### Thermal Data ##################################################################
    # Control Parameters
    Core_goal = (values[9] - 32) * 5 / 9  # Target core temp for cow if F and converted to C here now 101.4 or 38.5 based on Bolus adjustment
    check_time = int(values[10] * 60)      # Time to check when dry in minutes convereted to seconds 5 mintes or 300 seconds
    soak_check_time = int(values[11] * 60)  # soaker timing convert from min to sec, Default is 20 minutes or 1200 seconds
    dry_percent = values[12] / 100  # percentage of dryness to end soaker cycle and check again currenlty aggressive soaking set to 50
    soak_temp_max = (values[16] - 32) * 5 / 9  # Soaker set to run each cycle when temp is above this temp F converted to C current 80 F or 26.67 C
    soak_temp_min = (values[34] - 32) * 5 / 9  # Soaker over-ridden to off when temp is below this temp F converted to C current 60 F or 15.56 C

    # Fan Parameters
    fan_wind_range = np.array([0.0, values[15], values[14]])
    agrimesh_fan_range = np.array([0.0, 1.0, 100.0])
    # Fan settings WCEC method
    fan_T = np.array([values[17], values [18], values[19]])  # WCEC controller fan set based on difference between core temp and goal core temp
    fan_T = fan_T * 5 / 9  # Conveting to delta C
    fan_per = np.array([0.0, values[20], 100.0])
    # Fan and soaker seatings for Classic Controller
    fan_classic_range = np.array([values[21], values[22], values[23]])
    fan_classic_range = (fan_classic_range - 32) * 5 / 9  # Converting to C
    soak_temp_range = np.array([values[24], values[26]])  # Classic Mode soaker temp range
    soak_temp_range = (soak_temp_range - 32) * 5 / 9  # Convering to C
    soak_rate_range = np.array([float(values[25]), float(values[27])])  # Classic Mode soaker frequency range, minutes

    # Setting up weather conditions functions
    samples = 3600
    Temp0 = [values[0], values[0], values[0]]
    Humid0 = [values[1], values[1], values[1]]
    Temp_rad0 = [values[2], values[2], values[2]]
    meas_wind = values[3]
    # Convert F to K
    Temp0 = [(float(x) - 32) * 5 / 9 + 273.15 for x in Temp0]
    Humid0 = [x for x in Humid0]
    Temp_rad0 = [(float(x) - 32) * 5 / 9 + 273.15 for x in Temp_rad0]
    Temp = []
    Humid = []
    Temp_rad = []
    for i in range(len(Temp0) - 1):
        Temp = Temp + np.linspace(start=Temp0[i], stop=Temp0[i + 1], num=samples).tolist()
        Humid = Humid + np.linspace(start=Humid0[i], stop=Humid0[i + 1], num=samples).tolist()
        Temp_rad = Temp_rad + np.linspace(start=Temp_rad0[i], stop=Temp_rad0[i + 1], num=samples).tolist()
    sample_times = np.arange(len(Temp))
    Temp_fun = interp1d(sample_times, Temp, bounds_error=False, fill_value="extrapolate")
    Humid_fun = interp1d(sample_times, Humid, bounds_error=False, fill_value="extrapolate")
    Rad_fun = interp1d(sample_times, Temp_rad, bounds_error=False, fill_value="extrapolate")

    # Location Annual Conditions
    annual_temp = (61 - 32) * 5 / 9 + 273.15  # Average Annual Temp for local area, converts from F to K, Default 61 F
    Tg = Temp0[0]  # Ground Temp is air temp, K
    Tn = [Tg]
    for i in range(2, 51):  # underground was divided into 50 nodes, with thickness of 0.01 m
        Tn.append(Tg + (annual_temp - Tg) / 50 * (i - 1))

    # Natural Parameters
    cp = 1006       # air specific heat, J/kg/K
    P = 101325   # air pressure, Pa
    Rd = 287   # gas constant, J/kg/K
    Pr = 0.7  # Prandtl number (Gebremedhin & Wu, 2001)
    numda = 2430  # latent heat of vaporization of water, J/g
    ka = 0.024   # air conductivity, W/m/K, (Gebremedhin, 2016)
    k_soil = 1.64  # Thermal conductivity of soil (heavy clay, Najub et al., 2020), W/m/K
    rho_soil = 1559  # soil density, kg/m3 (Najub et al., 2020)
    cp_soil = 1784  # specific heat of soil, J/kg/K (Najub et al., 2020)
    # density_g = 0.08  # ground density (Campbell & Norman, 1998)
    # alpha_soil = k_soil / (rho_soil * cp_soil)  # Thermal diffusivity of soil, m2/s

    # Input Cow Parameters
    BW = 700  # Body weight, kg, Default 700
    Myield = 40  # milk production, kg/d, Default 40
    cpb = 3472      # body core specific heat, J/kg/K (Gebremedhin, 2016)
    cps = 3472      # skin specific heat, J/kg/K (Gebremedhin, 2016)
    cpc = 1006.43   # coat specific heat, J/kg/K (Gebremedhin, 2016)
    dc = 0.0025  # coat thickness, m (Bertipaglia, 2005)
    rho_hair = 26  # hair density, hair/mm2 (Chen, 2020)
    dh = 0.0625   # hair diameter, mm (Bertipaglia, 2005)
    kf = 0.26   # fur conductivity, W/m/k, (Gebremedhin, 2016)
    TP_Milk = 3.2 * 0.951  # Milk true protein content, %
    Fat_Milk = 3.5  # Milk fat content, %
    Lactose_Milk = 4.85  # Milk lactose content, %
    # lie_per = [0.62, 0.7, 0.6, 0.68, 0.37, 0.8, 0.8, 0.4, 0.6, 0.6, 0.4, 0.48, 0.37, 0.4, 0.37, 0.26, 0, 0.2, 0.3, 0.41,
    #           0.62, 0.68, 0.6, 0.67]  # Portion of lying time at each hour 0 - 23 ### Removed and replaced with location data based on 2023 observations
    pc = 0.2  # percent the cow is in contact with the ground, used in differential equations
    pw = 0.8  # percent the cow is wet, used in differential equations
    pw = min(pw, 1 - pc)  # setting pw to not exceed 1 - pc 
   
    # Calculated Cow Parameters
    A = 0.0838 * BW**0.67   # surface area, m2 (Elting, 1926; Le Cozler, 2019)
    Ms = 1.1123*BW**0.51  # skin mass, kg (Smith & Baldwin, 1974)
    Mc = 0.0022 * A   # coat mass, kg (Berman, 2008)
    Mb = BW - Ms   # body core mass, kg
    d = 0.06 * BW ** 0.39   # characteristic diameter, m (Ehrlemark, 1988)
    Af_At = rho_hair * math.pi * dh**2 / 4   # fur area density
    kx = Af_At * kf + (1 - Af_At) * ka  # horizontal thermal conductivity, W/m/K
    lc = 1/rho_hair**0.5  # mm
    ky = ka*(lc - dh) / lc + dh*ka*kf / (dh*ka + (lc - dh)*kf)  # vertical conductivity m, W/m/k
    keff = 0.5 * (kx + ky)   # effective conductivity, W/m/K
    NEl = Myield*(9.29*Fat_Milk + 5.85*TP_Milk + 3.95*Lactose_Milk)/100  # Net energy for lactation, Mcal/d
    NEm = 0.08 * BW**0.75  # Net energy for maintenance, Mcal/d
    # energy efficiency for lactation and maintenance is 0.64 and 0.62, the rest of energy is dissipated as heat.
    HE = (NEl/0.64 * (1-0.64) + NEm/0.62 * (1-0.62)) * 4.184 * 1000000/86400
    # Cooling from drinking water. Assuming a cow drinking 100 l of 25 ºC water per day and heating all of it up to 38.5
    water_cool = 100 * 4182 * (38.5-25) / 86400
    HE = HE - water_cool  # Cow Heat production (NRC 2001)
    delta = (1.5 * np.sqrt(3) * rho_hair*10**6)**(-0.5)  # side length of hexagon corresponding to 1 hair, m
    r_o = 0.5 * (delta + delta * np.sqrt(3)/2)  # outer radius of cylindrical domain, m
    r_i = dh / 1000 / 2  # inner radius of cylindrical domain (hair radius), m
    Carea_hair = 1 / 0.075  # calibrated against the initial water content

    # Cow Initial Conditions
    M_hair_int = 0.075 * A * 0.5  # Initial water content, kg (Chen, 2020). Assuming half of the body receive the water
    dry_condition = M_hair_int * dry_percent
    Tb_int = (values[4] - 32.0) * 5 / 9 + 273.15  # Initial core temp
    Ts1_int = (values[5] - 32.0) * 5 / 9 + 273.15  # Initial top skin temp
    Ts2_int = (values[6] - 32.0) * 5 / 9 + 273.15  # Initial bot skin temp
    Tc1_int = (values[7] - 32.0) * 5 / 9 + 273.15  # Initial top coat temp
    Tc2_int = (values[8] - 32.0) * 5 / 9 + 273.15  # Initial bot coat temp
    m_h2o_hair_int = 3.7494 * 10 ** (-3) * np.exp(17.2694 * (Tc1_int - 273.15) / (Tc1_int - 273.15 + 238.3))
    z0 = [Tb_int, Ts1_int, Ts2_int, Tc1_int, Tc2_int, M_hair_int, m_h2o_hair_int, fan_wind_range[1]]
    z0 = z0 + Tn

    # ######################################################################################################################

    # ###################################### Stand Model ###################################################################


    def thermal_stand(t, z):
        Tb = z[0]  # body core temp, K
        Ts1 = z[1]  # top skin temp, K
        Ts2 = z[2]  # bottom skin temp, K
        Tc1 = z[3]  # top coat temp, K
        Tc2 = z[4]  # bottom coat temp, K

        Ta = Temp_fun(t)  # air temperature, K
        RH = Humid_fun(t)  # relative humidity, %
        T_rad = Rad_fun(t)  # roof radiant temperature, K
        u = z[7]  # fan_wind  # wind speed, m/s
        rho_air = P / (Rd * Ta)  # air density dependent on temp and pressure, kg/m3
        pcp = cp * rho_air  # volumetric heat capacity of air, J/m3/K

        # Heat conduction between body core and skin, qcond_bs, W/m2
        # rs1 = max((-0.0544 * (Ts1 - 273.15) + 2.25), 0.29)  # skin resistance, s/cm (McArthur, 1987)
        rs1 = max((-0.0925 * (Ts1 - 273.15) + 3.71), 0.34)  # Based on calibration with smaXtec Bolus data, replaced old skin resistance, s/cm (McArthur, 1987)
        qcond_bs1 = pcp * (Tb - Ts1) / rs1 / 100  # McArthur, 1987

        # rs2 = max((-0.0544 * (Ts2 - 273.15) + 2.25), 0.29)  # skin resistance, s/cm (McArthur, 1987)
        rs2 = max((-0.0925 * (Ts2 - 273.15) + 3.71), 0.34)  # Based on calibration with smaXtec Bolus data, replaced old skin resistance, s/cm (McArthur, 1987)
        qcond_bs2 = pcp * (Tb - Ts2) / rs2 / 100

        # Heat loss through respiration, qresp, W/m2
        # respiration rate, breath/s (Atkins, 2018), minimum RR = 48/60 based on real data from Chen, 2013
        RR = max((19.8 * (Tb - 273.15) - 707) / 60, 48 / 60)
        Vt = 0.0189 * (60 * RR) ** (-0.463)  # Tidal volume, m3/breath (Stevens, 1981)

        # exhaled air temperature, K (Campos Maia, 2005)
        Te = 9.47 + 1.18 * (Ta - 273.15) - 0.01278 * (Ta - 273.15) ** 2 + 273.15

        # saturation vapor pressure at Te, Pa (Murray, 1967)
        Pwsat_te = 611 * np.exp(17.27 * (Te - 273.15) / (Te - 35.86))

        # virtual temperature of exhaled air, K (Monteith & Unsworth, 2013)
        Tve = Te * (1 + 0.38 * Pwsat_te / P)
        # saturation vapor pressure at Ta, Pa
        Pwsat_ta = 611 * np.exp(17.27 * (Ta - 273.15) / (Ta - 35.86))
        # virtual temperature of inhaled air, K
        Tva = Ta * (1 + 0.38 * Pwsat_ta / P)

        # absolute humidity of exhaled air, g/m3 (Monteith & Unsworth, 2013)
        chie = 18 * Pwsat_te / (8.314 * Te)
        # absolute humidity of inhaled air, g/m3
        chia = 18 * Pwsat_ta * RH / 100 / (8.314 * Ta)

        qresp = Vt * RR * pcp * (Tve - Tva) / A + numda * Vt * RR * (chie - chia) / A  # McGovern & Bruce

        # heat evaporation through sweating, qevap, W/m2
        # saturation vapor pressure at Ts, Pa
        Pwsat_ts1 = 611 * np.exp(17.27 * (Ts1 - 273.15) / (Ts1 - 35.86))
        Pwsat_ts2 = 611 * np.exp(17.27 * (Ts2 - 273.15) / (Ts2 - 35.86))
        # absolute humidity of skin surface, g/m3
        chis1 = 18 * Pwsat_ts1 / (8.314 * Ts1)
        chis2 = 18 * Pwsat_ts2 / (8.314 * Ts2)
        # Diffusive coefficient of water vapor, m2/s (Gebremedhin & Wu, 2001)
        D = 0.22 * 10 ** (-4) * 101325 / P * (Ta / 273.15) ** 1.5
        # dynamic viscosity of air, kg/m/s (linear regreesion fitted against Ta, Li, 2021)
        mu = (0.00452 * (Ta - 273.15) + 1.734) * 10 ** (-5)
        v = mu / rho_air  # kinematic viscosity of air, m2/s (Gebremedhin & Wu, 2001)
        u = max(u, .0203)  # creates a minimum velocity for forced convection
        Re = u * d / v  # Reynolds number (Gebremedhin & Wu, 2001)
        Sc = v / D  # Schmidt number (Gebremedhin & Wu, 2001)
        hm = D / d * 0.28 * Re ** 0.6 * Sc ** 0.44  # mass transfer coefficient, m/s (Gebremedhin & Wu, 2001)
        da = 2 * v / (2 * Sc ** (2 / 3) * hm)  # laminar thickness, m (Gebremedhin & Wu, 2001)
        qevap1 = min(31.5 + 3.67 * np.exp((Ts1 - 301.05) / 2.19115),
                     numda * (chis1 - chia) / (1 / hm + (dc + da) / D))  # Gebremedhin & Wu, 2001; Silva & Maia, 2011
        qevap2 = min(31.5 + 3.67 * np.exp((Ts2 - 301.05) / 2.19115), numda * (chis2 - chia) / (1 / hm + (dc + da) / D))

        # heat convection at coat surface
        if Re <= 1000:
            Nu = (0.43 + 0.5 * Re ** 0.5) * Pr ** 0.38  # Nusselt number (Gebremedhin & Wu, 2001)
        else:
            Nu = 0.25 * Re ** 0.6 * Pr ** 0.38
        hc = Nu * ka / d  # convection coefficient, W/m2/K (Monteith & Unsworth, 2013)

        # Long wave radiation
        Tr = T_rad  # Top of cow uses roof temp
        hrad = 5.67 * 10 ** (-8) * (301.15 + Tr) * (301.15 ** 2 + Tr ** 2)
        Tr2 = Ta  # Bottom of cow uses air temp which is the same a ground temp
        hrad2 = 5.67 * 10 ** (-8) * (301.15 + Tr2) * (301.15 ** 2 + Tr2 ** 2)

        # Use boundary condition to calculate Tc_top: qcond_ctop = qconv + qlw
        Tc_top1 = (2 * keff / dc * Tc1 + hc * Ta + 0.98 * hrad * Tr) / \
                  (2 * keff / dc + 0.98 * hrad + hc)

        Tc_top2 = (2 * keff / dc * Tc2 + hc * Ta + 0.98 * hrad2 * Tr2) / \
                  (2 * keff / dc + 0.98 * hrad2 + hc)

        # heat conduction between skin and coat, qcond_sc, W/m2
        qcond_sc1 = keff * (Ts1 - Tc1) / (dc / 2)
        qcond_sc2 = keff * (Ts2 - Tc2) / (dc / 2)

        # heat conduction between middle coat and top coat (qcond_sc, W/m2)
        qcond_ctop1 = keff * (Tc1 - Tc_top1) / (dc / 2)
        qcond_ctop2 = keff * (Tc2 - Tc_top2) / (dc / 2)

        # Differential equation
        dTb = (HE - A * qresp - (1 - pc) * A * qcond_bs1 - pc * A * qcond_bs2) / (Mb * cpb)
        dTs1 = A * (qcond_bs1 - qcond_sc1 - qevap1) / (Ms * cps)
        dTs2 = A * (qcond_bs1 - qcond_sc2 - qevap2) / (Ms * cps)
        dTc1 = A * (qcond_sc1 - qcond_ctop1) / (Mc * cpc)
        dTc2 = A * (qcond_sc2 - qcond_ctop2) / (Mc * cpc)

        dM_hair = 0
        dm_h20 = 0
        d_wind = 0
        dTg = list(np.repeat(0, 50))
        dzdt = [dTb, dTs1, dTs2, dTc1, dTc2, dM_hair, dm_h20, d_wind] + dTg
        return dzdt
    # ######################################################################################################################

    # ################################## Soaker Model ###################################################################


    def thermal_soaker(t, z):
        Tb = z[0]
        Ts1 = z[1]
        Ts2 = z[2]
        Tc1 = z[3]
        Tc2 = z[4]
        M_hair = z[5]
        m_h2o_hair = z[6]

        Ta = Temp_fun(t)  # air temperature, K
        RH = Humid_fun(t)  # relative humidity, %
        T_rad = Rad_fun(t)  # roof radiant temperature, K

        u = z[7]
        rho_air = P / (Rd * Ta)
        pcp = cp * rho_air

        # Heat conduction between body core and skin (qcond_bs, W/m2)
        # rs1 = max((-0.0544 * (Ts1 - 273.15) + 2.25), 0.29)  # skin resistance, s/cm (McArthur, 1987)
        rs1 = max((-0.0925 * (Ts1 - 273.15) + 3.71), 0.34)  # Based on calibration with smaXtec Bolus data, replaced old skin resistance, s/cm (McArthur, 1987)
        qcond_bs1 = pcp * (Tb - Ts1) / rs1 / 100

        # rs2 = max((-0.0544 * (Ts2 - 273.15) + 2.25), 0.29)  # skin resistance, s/cm (McArthur, 1987)
        rs2 = max((-0.0925 * (Ts2 - 273.15) + 3.71), 0.34)  # Based on calibration with smaXtec Bolus data, replaced old skin resistance, s/cm (McArthur, 1987)
        qcond_bs2 = pcp * (Tb - Ts2) / rs2 / 100

        # Heat loss through respiration (qresp, W/m2)
        RR = max((19.8 * (Tb - 273.15) - 707) / 60, 48 / 60)
        Vt = 0.0189 * (60 * RR) ** (-0.463)  # Tidal volume, m3/breath

        # exhaled air temperature, K
        Te = 9.47 + 1.18 * (Ta - 273.15) - 0.01278 * (Ta - 273.15) ** 2 + 273.15
        # saturation vapor pressure at Te, Pa
        Pwsat_te = 611 * np.exp(17.27 * (Te - 273.15) / (Te - 35.86))
        # virtual temperature of exhaled air
        Tve = Te * (1 + 0.38 * Pwsat_te / P)
        Pwsat_ta = 611 * np.exp(17.27 * (Ta - 273.15) / (Ta - 35.86))
        Tva = Ta * (1 + 0.38 * Pwsat_ta / P)

        # absolute humidity of exhaled air, g/m3
        chie = 18 * Pwsat_te / (8.314 * Te)
        # absolute humidity of inhaled air, g/m3
        chia = 18 * Pwsat_ta * RH / 100 / (8.314 * Ta)

        qresp = Vt * RR * pcp * (Tve - Tva) / A + numda * Vt * RR * (chie - chia) / A

        # heat evaporation through sweating (qevap, W/m2)
        # saturation vapor pressure at Ts, Pa
        Pwsat_ts1 = 611 * np.exp(17.27 * (Ts1 - 273.15) / (Ts1 - 35.86))
        Pwsat_ts2 = 611 * np.exp(17.27 * (Ts2 - 273.15) / (Ts2 - 35.86))
        # absolute humidity of skin surface, g/m3
        chis1 = 18 * Pwsat_ts1 / (8.314 * Ts1)
        chis2 = 18 * Pwsat_ts2 / (8.314 * Ts2)
        # Diffusive coefficient of water vapor, m2/s
        D = 0.22 * 10 ** (-4) * 101325 / P * (Ta / 273.15) ** 1.5
        # dynamic viscosity of air, kg/m/s
        mu = (0.00452 * (Ta - 273.15) + 1.734) * 10 ** (-5)
        v = mu / (P / (Rd * Ta))  # kinematic viscosity of air, m2/s
        u = max(u, .0203)  # creates a minimum velocity for forced convection
        Re = u * d / v  # Reynolds number
        Sc = v / D  # Schmidt number
        hm = D / d * 0.28 * Re ** 0.6 * Sc ** 0.44  # mass transfer coefficient, m/s
        da = 2 * v / (2 * Sc ** (2 / 3) * hm)  # laminar thickness, m
        qevap1 = min(31.5 + 3.67 * np.exp((Ts1 - 301.05) / 2.19115), numda * (chis1 - chia) / (1 / hm + (dc + da) / D))
        qevap2 = min(31.5 + 3.67 * np.exp((Ts2 - 301.05) / 2.19115), numda * (chis2 - chia) / (1 / hm + (dc + da) / D))

        # heat convection at coat surface (qconv, W/m2)
        if Re <= 1000:
            Nu = (0.43 + 0.5 * Re ** 0.5) * Pr ** 0.38
        else:
            Nu = 0.25 * Re ** 0.6 * Pr ** 0.38
        hc = Nu * ka / d

        # Long wave radiation
        Tr = T_rad  # Top of cow uses roof temp
        hrad = 5.67 * 10 ** (-8) * (301.15 + Tr) * (301.15 ** 2 + Tr ** 2)
        Tr2 = Ta  # Bottom of cow uses air temp which is same as ground temp
        hrad2 = 5.67 * 10 ** (-8) * (301.15 + Tr2) * (301.15 ** 2 + Tr2 ** 2)

        # Use boundry condition to calculate Tc_top: qcond_ctop = qconv + qlw
        Tc_top1 = (2 * keff / dc * Tc1 + hc * Ta + 0.98 * hrad * Tr) / \
                  (2 * keff / dc + 0.98 * hrad + hc)

        Tc_top2 = (2 * keff / dc * Tc2 + hc * Ta + 0.98 * hrad2 * Tr2) / \
                  (2 * keff / dc + 0.98 * hrad2 + hc)

        # heat conduction between skin and coat (qcond_sc, W/m2)
        qcond_sc1 = keff * (Ts1 - Tc1) / (dc / 2)  # change keff to k of fur
        qcond_sc2 = keff * (Ts2 - Tc2) / (dc / 2)

        # heat conduction between middle coat and top coat (qcond_sc, W/m2)
        qcond_ctop1 = keff * (Tc1 - Tc_top1) / (dc / 2)
        qcond_ctop2 = keff * (Tc2 - Tc_top2) / (dc / 2)

        # All equations from saturation vapor pressure at temperature T, Eq. 3 in Chen et al. (2020)
        m_h20_sat = 3.7494 * 10 ** (-3) * np.exp(17.2694 * (Tc1 - 273.15) / (Tc1 - 273.15 + 238.3))
        # water vapor mass flux, kg/s/m2 (Chen, 2020)
        n_evap = rho_hair * 10 ** 6 * 2 * math.pi * dc * rho_air * D / np.log(r_o / r_i) * (m_h20_sat - m_h2o_hair)
        # evaporation correction factor, dimensionless (Chen, 2020)
        beta_hair = Carea_hair * (M_hair / (0.5 * A))
        # convective mass transfer resistance (R_conv), m2/s/kg (Chen, 2020)
        Sh = 0.3 + 0.62 * Re ** 0.5 * Sc ** (1 / 3) / (1 + (0.4 / Sc) ** (2 / 3)) ** 0.25 * (1 + (Re / 282000)
                                                                                             ** (5 / 8)) ** (4 / 5)
        h_MT = Sh * D / d
        R_conv = 1 / (rho_air * h_MT)

        # water fraction: m_air = water / (dry air + water)
        m_air = 0.622 * (RH / 100) * Pwsat_ta / P
        # evaporation rate through soaker, W/m2 (Chen, 2020)
        qevap_soaker = n_evap * beta_hair * numda * 1000

        # mass output, kg/s/m2 (Chen, 2020)
        m_out = (m_h2o_hair - m_air) / R_conv

        # Differential equation
        dTb = (HE - A * qresp - (1 - pc) * A * qcond_bs1 - pc * A * qcond_bs2) / (Mb * cpb)
        dTs1 = A * (qcond_bs1 - qcond_sc1 - ((1 - pc) - pw) / (1 - pc) * qevap1) / (Ms * cps)
        dTs2 = A * (qcond_bs1 - qcond_sc2 - qevap2) / (Ms * cps)
        dTc1 = A * (qcond_sc1 - qcond_ctop1 - pw / (1 - pc) * qevap_soaker) / (Mc * cpc)
        dTc2 = A * (qcond_sc2 - qcond_ctop2) / (Mc * cpc)

        dM_hair = -n_evap * beta_hair * A * 0.5
        dm_h20 = (n_evap * beta_hair - m_out) / (rho_air * dc)
        d_wind = 0
        dTg = list(np.repeat(0, 50))
        dzdt = [dTb, dTs1, dTs2, dTc1, dTc2, dM_hair, dm_h20, d_wind] + dTg
        return dzdt


    def drying(t, y): return y[5] - dry_condition


    drying.terminal = True
    drying.direction = 0
    # ######################################################################################################################

    # ############################################## Lie Model #############################################################


    def thermal_lie(t, z):
        Tb = z[0]
        Ts1 = z[1]
        Ts2 = z[2]
        Tc1 = z[3]

        Ta = Temp_fun(t)  # air temperature, K
        RH = Humid_fun(t)  # relative humidity, %
        T_rad = Rad_fun(t)  # roof radiant temperature, K

        u = z[7]
        rho_air = P / (Rd * Ta)
        pcp = cp * rho_air

        # Heat conduction between body core and skin (qcond_bs, W/m2)
        # rs1 = max((-0.0544 * (Ts1 - 273.15) + 2.25), 0.29)  # skin resistance, s/cm (McArthur, 1987)
        rs1 = max((-0.0925 * (Ts1 - 273.15) + 3.71), 0.34)  # Based on calibration with smaXtec Bolus data, replaced old skin resistance, s/cm (McArthur, 1987)
        qcond_bs1 = pcp * (Tb - Ts1) / rs1 / 100

        # rs2 = max((-0.0544 * (Ts2 - 273.15) + 2.25), 0.29)  # skin resistance, s/cm (McArthur, 1987)
        rs2 = max((-0.0925 * (Ts2 - 273.15) + 3.71), 0.34)  # Based on calibration with smaXtec Bolus data, replaced old skin resistance, s/cm (McArthur, 1987)
        qcond_bs2 = pcp * (Tb - Ts2) / rs2 / 100

        # Heat loss through respiration (qresp, W/m2)
        RR = max((19.8 * (Tb - 273.15) - 707) / 60, 48 / 60)
        Vt = 0.0189 * (60 * RR) ** (-0.463)  # Tidal volume, m3/breath

        # exhaled air temperature, K
        Te = 9.47 + 1.18 * (Ta - 273.15) - 0.01278 * (Ta - 273.15) ** 2 + 273.15
        # saturation vapor pressure at Te, Pa
        Pwsat_te = 611 * np.exp(17.27 * (Te - 273.15) / (Te - 35.86))
        # virtual temperature of exhaled air
        Tve = Te * (1 + 0.38 * Pwsat_te / P)
        Pwsat_ta = 611 * np.exp(17.27 * (Ta - 273.15) / (Ta - 35.86))
        Tva = Ta * (1 + 0.38 * Pwsat_ta / P)

        # absolute humidity of exhaled air, g/m3
        chie = 18 * Pwsat_te / (8.314 * Te)
        # absolute humidity of inhaled air, g/m3
        chia = 18 * Pwsat_ta * RH / 100 / (8.314 * Ta)

        qresp = Vt * RR * pcp * (Tve - Tva) / A + numda * Vt * RR * (chie - chia) / A

        # heat evaporation through sweating (qevap, W/m2)
        # saturation vapor pressure at Ts, Pa
        Pwsat_ts1 = 611 * np.exp(17.27 * (Ts1 - 273.15) / (Ts1 - 35.86))
        # absolute humidity of skin surface, g/m3
        chis1 = 18 * Pwsat_ts1 / (8.314 * Ts1)
        # Diffusive coefficient of water vapor, m2/s
        D = 0.22 * 10 ** (-4) * 101325 / P * (Ta / 273.15) ** 1.5
        # dynamic viscosity of air, kg/m/s
        mu = (0.00452 * (Ta - 273.15) + 1.734) * 10 ** (-5)
        v = mu / rho_air  # kinematic viscosity of air, m2/s
        u = max(u, .0203)  # creates a minimum velocity for forced convection
        Re = u * d / v  # Reynolds number
        Sc = v / D  # Schmidt number
        hm = D / d * 0.28 * Re ** 0.6 * Sc ** 0.44  # mass transfer coefficient, m/s
        da = 2 * v / (2 * Sc ** (2 / 3) * hm)  # laminar thickness, m

        qevap1 = min(31.5 + 3.67 * np.exp((Ts1 - 301.05) / 2.19115), numda * (chis1 - chia) / (1 / hm + (dc + da) / D))

        # heat convection at coat surface (qconv, W/m2)
        if Re <= 1000:
            Nu = (0.43 + 0.5 * Re ** 0.5) * Pr ** 0.38
        else:
            Nu = 0.25 * Re ** 0.6 * Pr ** 0.38
        hc = Nu * ka / d

        # long wave radiation (qlw, W/m2)
        Tr = T_rad  # radiant temperature, K
        hrad = 5.67 * 10 ** (-8) * (301.15 + Tr) * (301.15 ** 2 + Tr ** 2)

        # Use boundry condition to calculate Tc_top: qcond_ctop = qconv + qlw
        Tc_top1 = (2 * keff / dc * Tc1 + hc * Ta + 0.98 * hrad * Tr) / \
                  (2 * keff / dc + 0.98 * hrad + hc)

        # heat conduction between skin and coat (qcond_sc, W/m2)
        qcond_sc1 = keff * (Ts1 - Tc1) / (dc / 2)

        # heat conduction between middle coat and top coat (qcond_sc, W/m2)
        qcond_ctop1 = keff * (Tc1 - Tc_top1) / (dc / 2)

        # Conduction between skin and ground (qcond_sg, W/m2)
        rsg = 0.1  # conduction resistance between the skin and the ground surface, m2.K/W (Radon, 2014)
        qcond_sg = (Ts2 - z[8]) / rsg  #
        dTg = []
        dTg.append((qcond_sg - k_soil * (z[8] - z[9]) / (0.01 / 2)) / (0.01 * rho_soil * cp_soil))  # node 0.01 m thick

        for j in range(9, 57):
            dTg.append((k_soil * (z[j - 1] - z[j]) - k_soil * (z[j] - z[j + 1])) / 0.01 / (0.01 * rho_soil * cp_soil))
        dTg.append((k_soil * (z[56] - z[57]) / 0.01 - k_soil *
                    (z[57] - annual_temp) / (0.01 / 2)) / (0.01 * rho_soil * cp_soil))
            
        # Differential equation
        dTb = (HE - A * qresp - (1 - pc) * A * qcond_bs1 - pc * A * qcond_bs2) / (Mb * cpb)
        dTs1 = A * (qcond_bs1 - qcond_sc1 - qevap1) / (Ms * cps)
        dTs2 = A * (qcond_bs2 - qcond_sg) / (Ms * cps)
        dTc1 = A * (qcond_sc1 - qcond_ctop1) / (Mc * cpc)
        dTc2 = A * (qcond_bs2 - qcond_sg) / (Ms * cps)
        dM_hair = 0
        dm_h20 = 0
        d_wind = 0
        dzdt = [dTb, dTs1, dTs2, dTc1, dTc2, dM_hair, dm_h20, d_wind] + dTg
        return dzdt
    # ######################################################################################################################
    
    # ############################################## Wet Routine ###########################################################
    def run_wet(z0, fan_wind, time_total, method):
        z0[7] = meas_wind  # uses measured wind (a safety minimum Re is in the three solution methods)
        # Find check_dry_time based on method
        if method:                   
            # Determine Drying Time
            t_end = t_start + time_total - 1
            t_eval = list(range(t_start, t_end + 1))
            solution1 = solve_ivp(thermal_soaker, (t_start, t_end), z0, events=drying, method='Radau', t_eval=t_eval)
            z1 = solution1.y
            dry_time = t_start + z1.shape[1]
        else: dry_time = time_total
            
        wet_per = 1.0 - lie_per
        t_next = int(np.floor(dry_time*wet_per))
        t_end = t_start + t_next - 1
        t_eval = list(range(t_start, t_end + 1))
        # For wet_per time
        solution1 = solve_ivp(thermal_soaker, (t_start, t_end), z0, events=drying, method='Radau', t_eval=t_eval)
        z1 = solution1.y         
        z0 = z1[:, z1.shape[1] - 1]
        
        windA = z0[7]
        cycleA = 'Soaker'
        timeA = t_start + z1.shape[1]
        body_coreA = z1[0, z1.shape[1] - 1] - 273.15
        top_skinA = z1[1, z1.shape[1] - 1] - 273.15
        bot_skinA = z1[2, z1.shape[1] - 1] - 273.15
        top_coatA = z1[3, z1.shape[1] - 1] - 273.15
        bot_coatA = z1[4, z1.shape[1] - 1] - 273.15
        
        # Now Lie
        # Bottom coat temperature equal to bottom skin when lying
        z0[4] = z0[2]
        # Reset ground temperature
        z0[7] = fan_wind
        z0[8] = Tg
        for m in range(9, 58):
            z0[m] = (z0[8] + (annual_temp - z0[8]) / 50 * (m - 8))
        # solve ODE using lying model
        t_end = t_start + dry_time - t_next - 1
        t_eval = list(range(t_start, t_end + 1))
        solution1 = solve_ivp(thermal_lie, (t_start, t_end), z0, method='Radau', t_eval=t_eval)
        z1 = solution1.y
        z0 = z1[:, z1.shape[1] - 1]
                
        windB = z0[7]
        cycleB = 'Lie'
        timeB = t_start + z1.shape[1]
        body_coreB = z1[0, z1.shape[1] - 1] - 273.15
        top_skinB = z1[1, z1.shape[1] - 1] - 273.15
        bot_skinB = z1[2, z1.shape[1] - 1] - 273.15
        top_coatB = z1[3, z1.shape[1] - 1] - 273.15
        bot_coatB = z1[4, z1.shape[1] - 1] - 273.15
        t_total = dry_time
        return [windA, cycleA, timeA, body_coreA, top_skinA, bot_skinA, top_coatA, bot_coatA, windB, cycleB, timeB, body_coreB, top_skinB, bot_skinB, top_coatB, bot_coatB, t_total]
    # ######################################################################################################################

    # ############################################## Dry Routine ###########################################################
    def run_dry(z0, fan_wind, time_total, method):
        t_next = int(np.floor(check_time*lie_per))
        if t_next != 0:
            # Bottom coat temperature equal to bottom skin when lying
            z0[4] = z0[2]
            # Reset ground temperature
            z0[7] = fan_wind
            z0[8] = Tg
            for m in range(9, 58):
                z0[m] = (z0[8] + (annual_temp - z0[8]) / 50 * (m - 8))
            # solve ODE using lying model
            t_end = t_start + t_next - 1
            t_eval = list(range(t_start, t_end + 1))

            solution1 = solve_ivp(thermal_lie, (t_start, t_end), z0, method='Radau', t_eval=t_eval)
            z1 = solution1.y
            z0 = z1[:, z1.shape[1] - 1]

            windA = z0[7]
            cycleA = 'Lie'
            timeA = t_start + z1.shape[1]
            body_coreA = z1[0, z1.shape[1] - 1] - 273.15
            top_skinA = z1[1, z1.shape[1] - 1] - 273.15
            bot_skinA = z1[2, z1.shape[1] - 1] - 273.15
            top_coatA = z1[3, z1.shape[1] - 1] - 273.15
            bot_coatA = z1[4, z1.shape[1] - 1] - 273.15

        # Then Stand
        z0[7] = meas_wind  # uses measured wind (a safety minimum Re is in the three solution methods)
        t_end = t_start + check_time - t_next - 1
        t_eval = list(range(t_start, t_end + 1))

        solution1 = solve_ivp(thermal_stand, (t_start, t_end), z0, method='Radau', t_eval=t_eval)
        z1 = solution1.y
        z0 = z1[:, z1.shape[1] - 1]
        
        windB = z0[7]
        cycleB = 'Stand'
        timeB = t_start + z1.shape[1]
        body_coreB = z1[0, z1.shape[1] - 1] - 273.15
        top_skinB = z1[1, z1.shape[1] - 1] - 273.15
        bot_skinB = z1[2, z1.shape[1] - 1] - 273.15
        top_coatB = z1[3, z1.shape[1] - 1] - 273.15
        bot_coatB = z1[4, z1.shape[1] - 1] - 273.15
        t_total = check_time
        return [windA, cycleA, timeA, body_coreA, top_skinA, bot_skinA, top_coatA, bot_coatA, windB, cycleB, timeB, body_coreB, top_skinB, bot_skinB, top_coatB, bot_coatB, t_total]
    # ######################################################################################################################
      
    # Determine lie_per based on Location Prediciton Based on Summer 2023 Observations (See Blue Sky and Johann data in Cow location and Conditions)
    lie_per = (Temp_fun(0) - 273.15) * -0.0216 + 1.0921
    lie_per = min(lie_per, .99)
    lie_per = max(lie_per, 0.01)
    t_start = 0
    
    #  Determine the Control Method: WCEC, Classic Control, Off Window
    now = datetime.now()
    weekday = now.weekday()
    day_check = any([weekday == values[28], values [28] == 7])
    off_start = now.replace(hour=values[29], minute=values[30], second=0, microsecond=0)
    time_change = timedelta(minutes=values[31]) 
    off_end = off_start + time_change
    if all([day_check, now >= off_start, now <= off_end]):  # The current time is in the window
        method = 3    
        if all([values[32] == 0, values[33] == 0]):  # Both are off
            fan_wind = fan_wind_range[1]
            fan_percent = int(fan_per[0])
            agri_set = int(agrimesh_fan_range[0])
            outcome = run_dry(z0, fan_wind, check_time, 0)
            tag = 1
        elif values[32] == 0:   # Fan is off only
            fan_wind = fan_wind_range[1]
            fan_percent = int(fan_per[0])
            agri_set = int(agrimesh_fan_range[0])
            if Temp_fun(0) - 273.15 < soak_temp_range[0]:
                soak_time = 0  # place holder is not used
                outcome = run_dry(z0, fan_wind, check_time, 0)
                tag = 2          
            elif Temp_fun(0) - 273.15 < soak_temp_range[1]:
                ## Run wet with soak_time
                soak_time = int(round(np.interp(Temp_fun(0) - 273.15, soak_temp_range, soak_rate_range) * 60,0)) # linear of soak time in seconds
                outcome = run_wet(z0, fan_wind, soak_time, 0)
                tag = 3
            else:
                ## Run wet with soak_time
                soak_time = int(soak_rate_range[1] * 60)  # fastest soak rate        
                outcome = run_wet(z0, fan_wind, soak_time, 0)
                tag = 4
        elif values[33] == 0:   # Soaker is off only
            # Set fan wind based on air temperature
            if  Temp_fun(0) - 273.15 < fan_classic_range[0]:
                fan_wind = fan_wind_range[1]
                fan_percent = int(fan_per[0])
                agri_set = int(agrimesh_fan_range[0])
            elif Temp_fun(0) - 273.15 < fan_classic_range[1]:
                fan_wind = fan_wind_range[1]
                fan_percent = int(fan_per[1])
                agri_set = int(agrimesh_fan_range[1])
            elif Temp_fun(0) - 273.15 < fan_classic_range[2]:
                fan_wind = np.interp(Temp_fun(0) - 273.15, fan_classic_range, fan_wind_range)
                fan_percent = int(np.interp(Temp_fun(0) - 273.15, fan_classic_range, fan_per))
                agri_set = int(np.interp(Temp_fun(0) - 273.15, fan_classic_range, agrimesh_fan_range))
            else:
                fan_wind = fan_wind_range[2]
                fan_percent = int(fan_per[2])
                agri_set = int(agrimesh_fan_range[2])
            outcome = run_dry(z0, fan_wind, check_time, 0)
            tag = 5
        elif values[13]:  # Run the WCEC Controller 
            method = values[13]
            # Fan Setting is based on difference between core temp and desired core temp; independent of soaking cycles
            T_core_diff = z0[0] - 273.15 - Core_goal
            if T_core_diff < fan_T[0]:
                fan_wind = fan_wind_range[1]
                fan_percent = int(fan_per[0])
                agri_set = int(agrimesh_fan_range[0])
            elif T_core_diff < fan_T[1]:
                fan_wind = fan_wind_range[1]
                fan_percent = int(fan_per[1])
                agri_set = int(agrimesh_fan_range[1])
            elif T_core_diff < fan_T[2]:
                fan_wind = np.interp(T_core_diff, fan_T, fan_wind_range)
                fan_percent = int(np.interp(T_core_diff, fan_T, fan_per))
                agri_set = int(np.interp(T_core_diff, fan_T, agrimesh_fan_range))
            else:
                fan_wind = fan_wind_range[2]
                fan_percent = int(fan_per[2])
                agri_set = int(agrimesh_fan_range[2])

            if any([z0[0] - 273.15 > Core_goal, Temp_fun(t_start) > soak_temp_max + 273.15]):  # Cow needs soaker 
                outcome = run_wet(z0, fan_wind, soak_check_time, method)
                tag = 6
            else:
                outcome = run_dry(z0, fan_wind, check_time, method)
                tag = 7

        else:  # Run the Classic Controller
            method = values[13]
            # Set fan wind based on air temperature
            if  Temp_fun(0) - 273.15 < fan_classic_range[0]:
                fan_wind = fan_wind_range[1]
                fan_percent = int(fan_per[0])
                agri_set = int(agrimesh_fan_range[0])
            elif Temp_fun(0) - 273.15 < fan_classic_range[1]:
                fan_wind = fan_wind_range[1]
                fan_percent = int(fan_per[1])
                agri_set = int(agrimesh_fan_range[1])
            elif Temp_fun(0) - 273.15 < fan_classic_range[2]:
                fan_wind = np.interp(Temp_fun(0) - 273.15, fan_classic_range, fan_wind_range)
                fan_percent = int(np.interp(Temp_fun(0) - 273.15, fan_classic_range, fan_per))
                agri_set = int(np.interp(Temp_fun(0) - 273.15, fan_classic_range, agrimesh_fan_range))
            else:
                fan_wind = fan_wind_range[2]
                fan_percent = int(fan_per[2])
                agri_set = int(agrimesh_fan_range[2])
            
            # Determine if soaker should run and calculate soaker time in seconds
            if Temp_fun(0) - 273.15 < soak_temp_range[0]:
                soak_time = 0  # place holder is not used
                outcome = run_dry(z0, fan_wind, check_time, method)
                tag = 8         
            elif Temp_fun(0) - 273.15 < soak_temp_range[1]:
                ## Run wet with soak_time
                soak_time = int(round(np.interp(Temp_fun(0) - 273.15, soak_temp_range, soak_rate_range) * 60,0)) # linear of soak time in seconds
                outcome = run_wet(z0, fan_wind, soak_time, method)
                tag = 9
            else:
                ## Run wet with soak_time
                soak_time = int(soak_rate_range[1] * 60)  # fastest soak rate        
                outcome = run_wet(z0, fan_wind, soak_time, method)
                tag = 10
    
    elif values[13]:  # Run the WCEC Controller 
        method = values[13]
        # Fan Setting is based on difference between core temp and desired core temp; independent of soaking cycles
        T_core_diff = z0[0] - 273.15 - Core_goal
        if T_core_diff < fan_T[0]:
            fan_wind = fan_wind_range[1]
            fan_percent = int(fan_per[0])
            agri_set = int(agrimesh_fan_range[0])
        elif T_core_diff < fan_T[1]:
            fan_wind = fan_wind_range[1]
            fan_percent = int(fan_per[1])
            agri_set = int(agrimesh_fan_range[1])
        elif T_core_diff < fan_T[2]:
            fan_wind = np.interp(T_core_diff, fan_T, fan_wind_range)
            fan_percent = int(np.interp(T_core_diff, fan_T, fan_per))
            agri_set = int(np.interp(T_core_diff, fan_T, agrimesh_fan_range))
        else:
            fan_wind = fan_wind_range[2]
            fan_percent = int(fan_per[2])
            agri_set = int(agrimesh_fan_range[2])

        if any([all([z0[0] - 273.15 > Core_goal, Temp_fun(t_start) > soak_temp_min + 273.15]), Temp_fun(t_start) > soak_temp_max + 273.15]):  # Cow needs soaker 
            outcome = run_wet(z0, fan_wind, soak_check_time, method)
            tag = 11
        else:
            outcome = run_dry(z0, fan_wind, check_time, method)
            tag = 12

    else:  # Run the Classic Controller
        method = values[13]
        # Set fan wind based on air temperature
        if  Temp_fun(0) - 273.15 < fan_classic_range[0]:
            fan_wind = fan_wind_range[1]
            fan_percent = int(fan_per[0])
            agri_set = int(agrimesh_fan_range[0])
        elif Temp_fun(0) - 273.15 < fan_classic_range[1]:
            fan_wind = fan_wind_range[1]
            fan_percent = int(fan_per[1])
            agri_set = int(agrimesh_fan_range[1])
        elif Temp_fun(0) - 273.15 < fan_classic_range[2]:
            fan_wind = np.interp(Temp_fun(0) - 273.15, fan_classic_range, fan_wind_range)
            fan_percent = int(np.interp(Temp_fun(0) - 273.15, fan_classic_range, fan_per))
            agri_set = int(np.interp(Temp_fun(0) - 273.15, fan_classic_range, agrimesh_fan_range))
        else:
            fan_wind = fan_wind_range[2]
            fan_percent = int(fan_per[2])
            agri_set = int(agrimesh_fan_range[2])
        
        # Determine if soaker should run and calculate soaker time in seconds
        if Temp_fun(0) - 273.15 < soak_temp_range[0]:
            soak_time = 0  # place holder is not used
            outcome = run_dry(z0, fan_wind, check_time, method)
            tag = 13         
        elif Temp_fun(0) - 273.15 < soak_temp_range[1]:
            ## Run wet with soak_time
            soak_time = int(round(np.interp(Temp_fun(0) - 273.15, soak_temp_range, soak_rate_range) * 60,0)) # linear of soak time in seconds
            outcome = run_wet(z0, fan_wind, soak_time, method)
            tag = 14
        else:
            ## Run wet with soak_time
            soak_time = int(soak_rate_range[1] * 60)  # fastest soak rate        
            outcome = run_wet(z0, fan_wind, soak_time, method)
            tag = 15
             
    settings = [fan_percent, agri_set, method, lie_per]
    outcome = outcome + settings
    return outcome

val0 = [float(sys.argv[1]),float(sys.argv[2]),float(sys.argv[3]),float(sys.argv[4]),float(sys.argv[5]),float(sys.argv[6]),float(sys.argv[7]),float(sys.argv[8]),float(sys.argv[9])]

# conditions = [85, 67.0, 70.0, 1.5]  # measured conditions, Temp F, humidity, Roof Temp F, wind speed m/s
# cow_temps = [103.7, 84.2, 90.4, 76.1, 86.1]  # initial cow temps that gets updated every iteration
# val0 = conditions + cow_temps

settings = [103.6, 5.0, 20.0, 50.0, 1, 2.5, 1.0, 80.0,  -0.09, -0.05, 0.05, 35.0]  # general settings that can be changed in the portal
classic_settings = [72.0, 75.0, 85.0, 80, 9, 90, 4] # general settings that can be changed in the portal
off_window_settings = [7, 11, 15, 70, 1, 0, 60]  # general settings that can be changed in the portal

val = val0 + settings + classic_settings + off_window_settings
print (val) # full input array should be printed to API

results = run_control(val) # full results array, should be printed to API
new_cow_temps = np.round(np.array(results[11:16])* 9 / 5 + 32,4) # Cow temperatures to be updated to replace the previous cow temps converted back to F
fan_set = results[18]  # setting for the fan in the range
if results[1] == 'Soaker':
    run_soaker = 1  # run the soaker if this value is 1 (run the soaker for 1 minute)
else:
    run_soaker = 0  # don't runt he soaker if this value is 0
check_time = results[16] # check again (run this control program) in this many seconds

print (results)  # the full results array that should be added to the API
print (new_cow_temps, fan_set, run_soaker, check_time) # The settings printed in neatly order
